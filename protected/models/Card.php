<?php

/**
 * Class Card
 */
class Card extends CardBase {

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Card the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @param int $userId
	 * @param bool $create if true and card not exists, run Card::createEmpty
	 * @return Card
	 */
	public function getUserCard($userId = null, $create = false) {
		if (!$userId) {
			$userId = Yii::app()->user->id;
		}
		$criteria = new CDbCriteria();
		$criteria->addCondition('user_id=:user_id');
		$criteria->params = array(':user_id' => $userId);
		$result = $this->find($criteria);

		if (!$result) {
			$this->createEmpty($userId);
			$result = $this->find($criteria);
		}

		return $result;
	}

	/**
	 * Create empty record
	 * @param int $userId
	 * @return bool
	 */
	public function createEmpty($userId = null) {
		if (!$userId) {
			$userId = Yii::app()->user->id;
		}
		$model = new Card();
		$model->user_id = $userId;
		return $model->save(false);
	}

}