<?php


class Article extends ArticleBase
{

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Article the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave() {
		if ($this->isNewRecord) {
			$this->translit = TString::str2url($this->title);
		}

		return parent::beforeSave();
	}

	public function dataProvider() {
		$criteria = new CDbCriteria();
		$criteria->order = 'create_at DESC';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Yii::app()->params->itemAt('articlesPerPage'),
				'pageVar' => 'page',
			),
		));
	}

	/**
	 * @return News[]
	 */
	public function findLast() {
		$criteria = new CDbCriteria();
		$criteria->order = 'create_at DESC';
		$criteria->limit = Yii::app()->params->itemAt('lastArticlesCount');
		return $this->findAll($criteria);
	}


}