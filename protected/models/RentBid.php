<?php
/**
 * Class RentBid
 */
class RentBid extends RentBidBase
{

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RentBid the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		return parent::beforeSave();
	}

	/**
	 * @param int $userId
	 * @param int $rentId
	 * @return string
	 */
	public function countUserBid($userId, $rentId) {
		$criteria = new CDbCriteria();
		$criteria->addCondition('user_id=:user_id');
		$criteria->addCondition('rent_id=:rent_id');
		$criteria->params = array(
			':user_id' => $userId,
			':rent_id' => $rentId,
		);

		return $this->count($criteria);
	}
}