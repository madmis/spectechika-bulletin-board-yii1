<?php

class Buses extends BusesBase implements TPhotoHandlerInterface {
	const PHOTO_MINI_PREFIX = 'mini-';
	const PHOTO_MINI_WIDTH = 50;
	const PHOTO_MINI_HEIGHT = 50;
	const PHOTO_SMALL_PREFIX = 'small-';
	const PHOTO_SMALL_WIDTH = 100;
	const PHOTO_SMALL_HEIGHT = 100;
	const PHOTO_MEDIUM_PREFIX = 'medium-';
	const PHOTO_MEDIUM_WIDTH = 250;
	const PHOTO_MEDIUM_HEIGHT = 220;

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Buses the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function myBusesList($userId) {
		$items = $this->findAllByAttributes(array(
			'user_id' => $userId
		));

		return CHtml::listData($items, 'id', function ($item) {
			/** @var Buses $item */
			return $item->category->name;
		});
	}

	/**
	 * @return $this|bool
	 */
	public function uploadPhoto() {
		$photo = CUploadedFile::getInstance($this, 'photo');
		if (!$photo) {
			return false;
		}

		$PhotoHandler = new TPhotoHandler($this, $photo);
		$this->photo = $PhotoHandler->load();
		return $this;
	}

	/**
	 * @param string $prefix
	 *            Buses::PHOTO_SMALL_PREFIX, PHOTO_MEDIUM_PREFIX, empty prefix - original photo
	 * @return string
	 */
	public function getPhotoUrl($prefix = '') {
		$PhotoHandler = new TPhotoHandler($this);
		$url = $PhotoHandler->getPhotoUrl($this->photo, $prefix);

		if (!$url) {
			$url = Yii::app()->getAssetManager()->getPublishedUrl(
				Yii::getPathOfAlias('application.assets')
			);
			$url .= "/img/no-photo/{$prefix}no-photo.png";
		}

		return $url;
	}

	public function getDropDownList($categoryId) {
		$items = $this->findAllByAttributes(array(
			'user_id' => Yii::app()->user->id,
			'category_id' => $categoryId,
		));

		$result = array(
			'data' => array(),
			'options' => array(),
		);

		foreach ($items as $item) {
			/** @var Buses $item */
			$result['data'][$item->id] = $item->category->name;
			$result['options'][$item->id] = array(
				'data-img' => $item->getPhotoUrl($this->getPhotoMiniPrefix()),
				'data-price' => $item->price . ' грн./час'
			);
		}

		return $result;
	}


	public function getBasePhotoUrl() {
		return '/uploads/photo/buses/';
	}

	public function getBasePhotoAlias() {
		return 'webroot.uploads.photo.buses';
	}

	public function getPhotoSmallWidth() {
		return self::PHOTO_SMALL_WIDTH;
	}

	public function getPhotoSmallHeight() {
		return self::PHOTO_SMALL_HEIGHT;
	}

	public function getPhotoSmallPrefix() {
		return self::PHOTO_SMALL_PREFIX;
	}

	public function getPhotoMediumWidth() {
		return self::PHOTO_MEDIUM_WIDTH;
	}

	public function getPhotoMediumHeight() {
		return self::PHOTO_MEDIUM_HEIGHT;
	}

	public function getPhotoMediumPrefix() {
		return self::PHOTO_MEDIUM_PREFIX;
	}

	public function getPhotoMiniWidth() {
		return self::PHOTO_MINI_WIDTH;
	}

	public function getPhotoMiniHeight() {
		return self::PHOTO_MINI_HEIGHT;
	}

	public function getPhotoMiniPrefix() {
		return self::PHOTO_MINI_PREFIX;
	}
}