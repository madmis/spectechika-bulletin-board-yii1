<?php

/**
 * Class UserService
 */
class UserService extends UserServiceBase {
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserService the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Найти пользователя по сервису
	 * @param EAuthServiceBase $service
	 * @return UserService or null
	 */
	public function getUserByService(EAuthServiceBase $service) {
		$criteria = new CDbCriteria;
		$criteria->condition = 'service=:service AND identity=:identity';
		$criteria->params = array('service' => $service->getServiceName(), 'identity' => $service->getId());
		return $this->find($criteria);
	}

	/**
	 * Возвращает массив сервисов, в которых, если они подключены у пользователя,
	 * присутствует ключ user со значением true ($service['vkontakte']['user'] = true)
	 * @param $userId
	 * @return array
	 */
	public function getUserServices($userId = null) {
		$userServices = $this->getByUserId($userId);
		$services = Yii::app()->eauth->services;
		/** @var $value UserService */
		foreach ($userServices as $value) {
			if ($value->service != 'email') {
				$services[$value->service]['user'] = true;
			}
		}

		return $services;
	}

	/**
	 * Выбирает всех
	 * @param $userId if null, take current authorized user
	 * @return array
	 */
	public function getByUserId($userId = null) {
		if ($userId == null) {
			$userId = Yii::app()->user->id;
		}
		$criteria = new CDbCriteria;
		$criteria->condition = 'user_id=:user_id';
		$criteria->params = array('user_id' => (int)$userId);
		return $this->findAll($criteria);
	}

	/**
	 * Check, is user has connected social network
	 * @param int $userId
	 * @param array $exclude exclude from check. Default array('email')
	 * @return bool
	 */
	public function isUserSocial($userId = null, $exclude = array('email')) {
		if ($userId == null) {
			$userId = Yii::app()->user->id;
		}

		$criteria = new CDbCriteria;
		$criteria->compare('user_id', (int)$userId);
		if ($exclude) {
			$criteria->addNotInCondition('service', $exclude);
		}

		return$this->exists($criteria);
	}

	/**
	 * @param int $identity
	 * @param string $service
	 * @return UserService
	 */
	public function getByIdentity($identity, $service = null) {
		$criteria = new CDbCriteria;
		$criteria->condition = 'identity=:identity';
		if ($service != null) {
			$criteria->condition .= ' AND service=:service';
		}
		$criteria->params = array('identity' => $identity, 'service' => $service);
		return $this->find($criteria);
	}

	/**
	 * Удалить запись по имени сервиса и id пользователя
	 * @param $service
	 * @param int $userId. Если id не указан, берется текущий авторизованный пользователь
	 * @return int
	 */
	public function removeUserService($service, $userId = null) {
		if ($userId == null) {
			$userId = Yii::app()->user->id;
		}

		$criteria = new CDbCriteria;
		$criteria->condition = 'service=:service AND user_id=:user_id';
		$criteria->params = array('service' => $service, 'user_id' => (int)$userId);

		return $this->deleteAll($criteria);
	}
}