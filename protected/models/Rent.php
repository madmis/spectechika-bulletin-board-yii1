<?php

class Rent extends RentBase {

	const STATUS_ACTIVE = 0;
	const STATUS_CLOSED_PERFORMER = 1;
	const STATUS_CLOSED = 2;

	protected $_statusDescription = array(
		0 => 'Активно',
		1 => 'Закрыто, выбран исполнитель',
		2 => 'Закрыто',
	);

	/**
	 * @return Rent[]
	 */
	public function mainBlock() {
		$criteria = new CDbCriteria();
		$criteria->limit = Yii::app()->params->itemAt('mainBlockCount');
		$criteria->order = 'create_at DESC';

		return $this->findAll($criteria);
	}

	/**
	 * @return Rent[]
	 */
	public function mainTable() {
		$criteria = new CDbCriteria();
		$criteria->limit = Yii::app()->params->itemAt('mainTableCount');
		$criteria->offset = Yii::app()->params->itemAt('mainBlockCount');
		$criteria->order = 'create_at DESC';

		return $this->findAll($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rent the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @param int|null $status
	 * @return string
	 * @throws InvalidArgumentException
	 */
	public function getStatusDescription($status = null) {
		if ($status === null) {
			$status = $this->status;
		}

		if (!isset($this->_statusDescription[$status])) {
			throw new \InvalidArgumentException("Invalid status value: {$status}");
		}

		return $this->_statusDescription[$status];
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function getDataProvider() {
		return $this->baseDataProvider();
	}

	/**
	 * @param Category $category
	 * @return CActiveDataProvider
	 */
	public function byCategoryProvider(Category $category) {
		return $this->baseDataProvider($category);
	}

	/**
	 * @param Region $region
	 * @return CActiveDataProvider
	 */
	public function byRegionProvider(Region $region) {
		return $this->baseDataProvider(null, $region);
	}

	/**
	 * @param City $city
	 * @return CActiveDataProvider
	 */
	public function byCityProvider(City $city) {
		return $this->baseDataProvider(null, null, $city);
	}

	/**
	 * @param Category $Category
	 * @param Region $Region
	 * @param City $City
	 * @return CActiveDataProvider
	 */
	public function baseDataProvider(Category $Category = null, Region $Region = null, City $City = null) {
		$criteria = new CDbCriteria();
		$params = new CMap();
		if ($Category) {
			$criteria->addCondition('category_id = :category_id');
			$params->add(':category_id', $Category->id);
		}

		if ($Region) {
			$criteria->addCondition('region_id = :region_id');
			$params->add(':region_id', $Region->id);
		}

		if ($City) {
			$criteria->addCondition('city_id = :city_id');
			$params->add(':city_id', $City->id);
		}

		$criteria->params = $params->toArray();

		$sort = new CSort;
		$sort->defaultOrder = 't.create_at DESC';
		$sort->attributes = array(
			'create_at' => array(
				'asc' => 't.create_at',
				'desc' => 't.create_at DESC',
				'label' => 'create_at'
			),
			'max_price' => array(
				'asc' => 't.max_price',
				'desc' => 't.max_price DESC',
				'label' => 'max_price'
			),
		);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Yii::app()->params->itemAt('rentPerPage'),
				'pageVar' => 'page',
			),
			'sort' => $sort,
		));
	}

	/**
	 * Event onNewRent
	 * @param CModelEvent $event
	 */
	public function onNewRent(CModelEvent $event)
	{
		$this->raiseEvent('onNewRent', $event);
	}

	public function beforeSave() {
		if (!$this->city_id) {
			$this->city_id = null;
		}

		$notifier = new TAdminNotifier();
		$this->onNewRent = array($notifier, 'newRent');

		return parent::beforeSave();
	}

	protected function afterSave()
	{
		if ($this->isNewRecord) {
			$event = new CModelEvent($this);
			$this->onNewRent($event);
			if (!$event->isValid) {
				$mess = 'Error to send email from Rent. Params: ' . print_r($this->attributes, true);
				Yii::log($mess, CLogger::LEVEL_WARNING, 'system.mail.Rent');
			}
		}
		return parent::afterSave();
	}


}