<?php

/**
 * Class Photo
 */
class Photo extends PhotoBase {
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Photo the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Upload photo and save new record
	 * @return bool
	 */
	public function uploadPhoto() {
		$photo = CUploadedFile::getInstanceByName('photo');
		if (!$photo) {
			return false;
		}

		$PhotoHandler = new TPhotoHandler($photo);
		$this->photo = $PhotoHandler->load();
		$Card = Card::model()->getUserCard(null, true);
		$this->card_id = $Card->id;
		$this->user_id = Yii::app()->user->id;

		return $this->save();
	}

	/**
	 * @param int $userId
	 * @return int
	 */
	public function countByUser($userId = null) {
		if (!$userId) {
			$userId = Yii::app()->user->id;
		}

		$criteria = new CDbCriteria();
		$criteria->addInCondition('user_id', array($userId));
		return (int)$this->count($criteria);
	}

	/**
	 * @param string $prefix
	 * 			TPhotoHandler::PHOTO_SMALL_PREFIX, PHOTO_MEDIUM_PREFIX, empty prefix - original photo
	 * @return string
	 */
	public function getPhotoUrl($prefix = '') {
		$PhotoHandler = new TPhotoHandler();
		return $PhotoHandler->getPhotoUrl($this->photo, $prefix);
	}
}