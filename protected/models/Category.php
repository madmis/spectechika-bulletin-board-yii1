<?php

class Category extends CategoryBase {

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function groupedIndexedList() {
		$criteria = new CDbCriteria();
		$criteria->order = 'name ASC';
		$items = $this->findAll($criteria);
		$list = new CMap();

		$data = CHtml::listData($items, 'id', 'name', 'parent_id');
		$parent = $data[0];
		unset($data[0]);
		$result = array();
		foreach ($data as $key => $value) {
			if (isset($parent[$key])) {
				$result[$parent[$key]] = $value;
			}
		}
		ksort($result);
		return $result;
	}

	/**
	 * @return CMap {parent: Category[], child: {parent_id: Category[]} }
	 */
	public function parentAndChild() {
		$criteria = new CDbCriteria();
		$criteria->order = 'name ASC';
		$criteria->condition = "parent_id = 0";
		$parentItems = $this->findAll($criteria);
		$criteria->condition = "parent_id != 0";
		$items = $this->findAll($criteria);
		$child = array();
		foreach ($items as $item) {
			/** @var $item Category */
			if (!isset($child[$item->parent_id])) {
				$child[$item->parent_id] = new CList();
			}

			$child[$item->parent_id]->add($item);
		}

		$result = new CMap();
		$result->add('parent', $parentItems);
		$result->add('child', $child);
		return $result;
	}
}