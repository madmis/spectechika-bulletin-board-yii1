<?php

/**
 * Class User
 */
class User extends UserBase
{

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeValidate()
	{
		if ($this->isNewRecord) {
			$this->create_at = Yii::app()->format->formatDbDateTime('now');
			$this->last_visit = Yii::app()->format->formatDbDateTime('now');
		}
		return parent::beforeValidate();
	}

	public function beforeSave()
	{
		if (!empty($this->password)) {
			if (in_array($this->scenario, array('insert', 'register'))) {
				$this->password = $this->hashPassword($this->password);
			} else if ($this->scenario == 'update') {
				/** @var $user User */
				$user = User::model()->findByPk($this->id);
				$hash = $this->hashPassword($this->password);
				if ($this->password !== $user->password && $hash !== $user->password) {
					$this->password = $this->hashPassword($this->password);
				} else {
					$this->password = $user->password;
				}
			}
		}

		$this->email = empty($this->email) ? null : $this->email;
		$this->region_id = empty($this->region_id) ? null : $this->region_id;
		$this->city_id = empty($this->city_id) ? null : $this->city_id;

		$notifier = new TAdminNotifier();
		// навешиваем обработчик на событие, в afterSave будем проверять,
		// если добавлена новая запись, админу будет отправлено уведомление
		$this->onNewUser = array($notifier, 'newUser');

		return parent::beforeSave();
	}

	/**
	 * Hash user password
	 * @param $password
	 * @return string
	 */
	public function hashPassword($password)
	{
		$password = trim($password);
		return hash('sha256', Yii::app()->params->itemAt('salt') . $password);
	}

	/**
	 * Validate password
	 * @param $password
	 * @return bool
	 */
	public function validatePassword($password)
	{
		return $this->hashPassword($password) === $this->password;
	}

	public function validateCity($attribute, $params)
	{
		if (!empty($this->$attribute) && !is_numeric($this->$attribute)) {
			City::model()->findByAttributes(array('name' => $this->$attribute));
			$c = new CDbCriteria();
			$c->addCondition('name = :name');
			$c->params = array(':name' => $this->$attribute);

			if (!City::model()->exists($c)) {
				$this->addError($attribute, 'Город указан неверно! Выберите город из списка.');
			}
		}
	}

	/**
	 * Event onNewUser
	 * @param CModelEvent $event
	 */
	public function onNewUser(CModelEvent $event)
	{
		$this->raiseEvent('onNewUser', $event);
	}

	protected function afterSave()
	{
		if ($this->isNewRecord) {
			$event = new CModelEvent($this);
			$this->onNewUser($event);
			if (!$event->isValid) {
				$mess = 'Error to send email from User. Params: ' . print_r($this->attributes, true);
				Yii::log($mess, CLogger::LEVEL_WARNING, 'system.mail.User');
			}
		}
		return parent::afterSave();
	}

}