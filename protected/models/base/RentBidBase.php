<?php

/**
 * This is the model class for table "{{rent_bid}}".
 *
 * The followings are the available columns in table '{{rent_bid}}':
 * @property string $id
 * @property string $rent_id
 * @property integer $user_id
 * @property string $buse_id
 * @property string $price
 * @property string $description
 * @property string $create_at
 *
 * The followings are the available model relations:
 * @property Rent $rent
 * @property Buses $buse
 * @property User $user
 */
class RentBidBase extends CActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{rent_bid}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rent_id, user_id, buse_id, price, description', 'required'),
			array('user_id', 'numerical', 'integerOnly' => true),
			array('rent_id, buse_id, price', 'length', 'max' => 11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, rent_id, user_id, buse_id, price, description, create_at', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		return array(
			'rent' => array(self::BELONGS_TO, 'Rent', 'rent_id'),
			'buse' => array(self::BELONGS_TO, 'Buses', 'buse_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'rent_id' => 'Объявление',
			'user_id' => 'Пользователь',
			'buse_id' => 'Спецтехника',
			'price' => 'Цена в час',
			'description' => 'Описание',
			'create_at' => 'Добавлено',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('rent_id', $this->rent_id, true);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('buse_id', $this->buse_id, true);
		$criteria->compare('price', $this->price, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('create_at', $this->create_at, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
