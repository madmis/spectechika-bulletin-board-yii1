<?php

/**
 * This is the model class for table "region".
 *
 * The followings are the available columns in table 'region':
 * @property integer $id
 * @property string $name
 * @property string $name_r
 * @property string $name_d
 * @property string $name_v
 * @property string $name_t
 * @property string $name_p
 * @property string $translit
 *
 * The followings are the available model relations:
 * @property City[] $cities
 * @property Rent[] $rents
 * @property ToRent[] $toRents
 * @property User[] $users
 */
class RegionBase extends CActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'region';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, translit', 'required'),
			array('name, name_r, name_d, name_v, name_t, name_p', 'length', 'max'=>255),
			array('translit', 'length', 'max' => 50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, translit, name_r, name_d, name_v, name_t, name_p', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cities' => array(self::HAS_MANY, 'City', 'region_id'),
			'rents' => array(self::HAS_MANY, 'Rent', 'region_id'),
			'toRents' => array(self::HAS_MANY, 'ToRent', 'region_id'),
			'users' => array(self::HAS_MANY, 'User', 'regoin_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'name_r' => 'Name R',
			'name_d' => 'Name D',
			'name_v' => 'Name V',
			'name_t' => 'Name T',
			'name_p' => 'Name P',
			'translit' => 'Translit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('name_r',$this->name_r,true);
		$criteria->compare('name_d',$this->name_d,true);
		$criteria->compare('name_v',$this->name_v,true);
		$criteria->compare('name_t',$this->name_t,true);
		$criteria->compare('name_p',$this->name_p,true);
		$criteria->compare('translit', $this->translit, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
