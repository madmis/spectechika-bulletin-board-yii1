<?php

/**
 * This is the model class for table "{{to_rent}}".
 *
 * The followings are the available columns in table '{{to_rent}}':
 * @property string $id
 * @property integer $user_id
 * @property integer $category_id
 * @property integer $region_id
 * @property integer $city_id
 * @property integer $price
 * @property string $features
 * @property string $description
 * @property string $fio
 * @property string $company_name
 * @property string $phone
 * @property string $photo
 * @property string $create_at
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property City $city
 * @property Region $region
 * @property User $user
 */
class ToRentBase extends CActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{to_rent}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, region_id, price, features, description, fio, phone', 'required'),
			array('user_id, price', 'numerical', 'integerOnly' => true),
			array('category_id, region_id', 'length', 'max' => 11),
			array('fio, company_name', 'length', 'max' => 255),
			array('phone', 'length', 'max' => 19),
			array('city_id', 'safe'),
			array(
				'id, user_id, category_id, region_id, city_id, price,
				features, description, fio, company_name, phone,
				photo, create_at', 'safe', 'on' => 'search'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'category_id' => 'Категория',
			'region_id' => 'Регион',
			'city_id' => 'Город',
			'price' => 'Цена в час',
			'features' => 'Дополнительные характеристики',
			'description' => 'Описание',
			'fio' => 'ФИО',
			'company_name' => 'Название компании',
			'phone' => 'Телефон',
			'photo' => 'Фотография',
			'create_at' => 'Добавлено',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('region_id', $this->region_id);
		$criteria->compare('city_id', $this->city_id);
		$criteria->compare('price', $this->price);
		$criteria->compare('features', $this->features, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('fio', $this->fio, true);
		$criteria->compare('company_name', $this->company_name, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('photo', $this->photo, true);
		$criteria->compare('create_at', $this->create_at, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
