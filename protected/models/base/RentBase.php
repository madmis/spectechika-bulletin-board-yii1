<?php

/**
 * This is the model class for table "{{rent}}".
 *
 * The followings are the available columns in table '{{rent}}':
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property integer $category_id
 * @property integer $region_id
 * @property integer $city_id
 * @property integer $max_price
 * @property string $description
 * @property string $fio
 * @property string $company_name
 * @property string $phone
 * @property integer $status
 * @property integer $performer_id
 * @property string $create_at
 *
 * The followings are the available model relations:
 * @property City $city
 * @property Category $category
 * @property Region $region
 * @property User $user
 * @property RentBid[] $rentBs
 */
class RentBase extends CActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{rent}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, category_id, region_id, description, fio, company_name, phone, max_price', 'required'),
			array('user_id, max_price, status, performer_id', 'numerical', 'integerOnly' => true),
			array('category_id, region_id, city_id', 'length', 'max' => 11),
			array('title, fio, company_name', 'length', 'max' => 255),
			array('phone', 'length', 'max' => 19),
			array(
				'id, user_id, title, category_id, region_id, city_id, max_price,
				description, fio, company_name, phone, status, performer_id, create_at, status', 'safe', 'on' => 'search'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'rentBs' => array(self::HAS_MANY, 'RentBid', 'rent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'title' => 'Название',
			'category_id' => 'Категория',
			'region_id' => 'Регион',
			'city_id' => 'Город',
			'max_price' => 'Макс. цена в час',
			'description' => 'Описание',
			'fio' => 'ФИО',
			'company_name' => 'Название компании',
			'phone' => 'Телефон',
			'status' => 'Статус',
			'performer_id' => 'Исполнитель',
			'create_at' => 'Добавлено',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('region_id', $this->region_id);
		$criteria->compare('city_id', $this->city_id);
		$criteria->compare('max_price', $this->max_price);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('fio', $this->fio, true);
		$criteria->compare('company_name', $this->company_name, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('performer_id',$this->performer_id);
		$criteria->compare('create_at', $this->create_at, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
