<?php

/**
 * This is the model class for table "{{article}}".
 *
 * The followings are the available columns in table '{{article}}':
 * @property string $id
 * @property integer $user_id
 * @property string $title
 * @property string $translit
 * @property string $snippet
 * @property string $text
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $create_at
 *
 * The followings are the available model relations:
 * @property User $user
 */
class ArticleBase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{article}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, title, snippet, text', 'required'),
			array('user_id', 'numerical', 'integerOnly' => true),
			array('title, translit', 'length', 'max' => 255),
			array('meta_keywords, meta_description', 'length', 'max' => 200),
			array('title', 'unique', 'attributeName' => 'title'),
			array('create_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, title, translit, snippet, text, meta_keywords, meta_description, create_at', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'title' => 'Название',
			'translit' => 'Translit',
			'snippet' => 'Сниппет',
			'text' => 'Текст',
			'meta_keywords' => 'Meta Keywords',
			'meta_description' => 'Meta Description',
			'create_at' => 'Добавлено',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('translit', $this->translit, true);
		$criteria->compare('snippet', $this->snippet, true);
		$criteria->compare('text', $this->text, true);
		$criteria->compare('meta_keywords', $this->meta_keywords, true);
		$criteria->compare('meta_description', $this->meta_description, true);
		$criteria->compare('create_at', $this->create_at, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
