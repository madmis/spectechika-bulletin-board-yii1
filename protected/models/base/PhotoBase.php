<?php

/**
 * This is the model class for table "photo".
 *
 * The followings are the available columns in table 'photo':
 * @property string $id
 * @property string $photo
 * @property string $card_id
 * @property integer $user_id
 * @property string $create_at
 *
 * The followings are the available model relations:
 * @property Card $card
 * @property User $user
 */
class PhotoBase extends TActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{photo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('photo, card_id, user_id', 'required'),
			array('photo', 'length', 'max' => 255),
			array('card_id, user_id', 'length', 'max' => 11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, photo, card_id, user_id, create_at', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'card' => array(self::BELONGS_TO, 'Card', 'card_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'photo' => Yii::t('main', 'Файл'),
			'card_id' => Yii::t('main', 'Id карточки'),
			'user_id' => 'User',
			'create_at' => Yii::t('main', 'Дата создания'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('photo', $this->path, true);
		$criteria->compare('card_id', $this->card_id, true);
		$criteria->compare('user_id', $this->user_id, true);
		$criteria->compare('create_at', $this->create_at, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
