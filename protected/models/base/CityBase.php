<?php

/**
 * This is the model class for table "{{city}}".
 *
 * The followings are the available columns in table '{{city}}':
 * @property integer $id
 * @property string $region_id
 * @property string $name
 * @property string $name_r
 * @property string $name_d
 * @property string $name_v
 * @property string $name_t
 * @property string $name_p
 *
 * The followings are the available model relations:
 * @property Region $region
 * @property Rent[] $rents
 * @property ToRent[] $toRents
 * @property User[] $users
 */
class CityBase extends CActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{city}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('region_id, name', 'required'),
			array('region_id', 'length', 'max' => 11),
			array('name, name_r, name_d, name_v, name_t, name_p', 'length', 'max' => 255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, region_id, name, name_r, name_d, name_v, name_t, name_p', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
			'rents' => array(self::HAS_MANY, 'Rent', 'city_id'),
			'toRents' => array(self::HAS_MANY, 'ToRent', 'city_id'),
			'users' => array(self::HAS_MANY, 'User', 'city_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'region_id' => 'Region',
			'name' => 'Name',
			'name_r' => 'Name R',
			'name_d' => 'Name D',
			'name_v' => 'Name V',
			'name_t' => 'Name T',
			'name_p' => 'Name P',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('region_id', $this->region_id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('name_r', $this->name_r, true);
		$criteria->compare('name_d', $this->name_d, true);
		$criteria->compare('name_v', $this->name_v, true);
		$criteria->compare('name_t', $this->name_t, true);
		$criteria->compare('name_p', $this->name_p, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
