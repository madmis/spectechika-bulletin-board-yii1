<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $company_name
 * @property string $address
 * @property string $region_id
 * @property string $city_id
 * @property string $description
 * @property string $email
 * @property string $password
 * @property string $avatar
 * @property string $phone
 * @property string $create_at
 * @property string $last_visit
 *
 * The followings are the available model relations:
 * @property AuthItem[] $authItems
 * @property Buses[] $buses
 * @property Card[] $cards
 * @property Photo[] $photos
 * @property City $city
 * @property Region $region
 * @property UserService[] $userServices
 */
class UserBase extends CActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, company_name', 'length', 'max' => 200),
			array('email, password', 'required', 'on' => 'register, login'),
			array('region_id', 'length', 'max' => 11),
			array('email', 'length', 'max' => 128),
			array('password', 'length', 'max' => 64),
			array('phone', 'length', 'max' => 19),
			array('phone', 'TPhoneValidator'),
			array('city_id', 'validateCity'),
			array('avatar', 'length', 'max' => 255),
			array('email', 'email'),
			array('email', 'unique', 'on' => 'register, update'),
			array('username', 'in', 'range'=>Yii::app()->authManager->admins, 'not' => true, 'message' => 'Данное имя запрещено использовать'),

			array('last_visit, address, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array(
				'id, username, company_name, address, region_id,
				city_id, description, email, password, avatar, phone,
				create_at, last_visit', 'safe', 'on' => 'search'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'authItems' => array(self::MANY_MANY, 'AuthItem', 'auth_assignment(userid, itemname)'),
			'buses' => array(self::HAS_MANY, 'Buses', 'user_id'),
			'cards' => array(self::HAS_MANY, 'Card', 'user_id'),
			'photos' => array(self::HAS_MANY, 'Photo', 'user_id'),
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
			'userServices' => array(self::HAS_MANY, 'UserService', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'username' => 'ФИО',
			'company_name' => 'Название компании',
			'address' => 'Адрес',
			'region_id' => 'Регион',
			'city_id' => 'Город',
			'description' => 'Описание деятельности',
			'email' => 'Email',
			'password' => 'Пароль',
			'avatar' => 'Аватар',
			'phone' => 'Телефон',
			'create_at' => 'Дата создания',
			'last_visit' => 'Последний визит',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('username', $this->username, true);
		$criteria->compare('company_name', $this->company_name, true);
		$criteria->compare('address', $this->address, true);
		$criteria->compare('region_id', $this->region_id, true);
		$criteria->compare('city_id', $this->city_id, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('avatar', $this->avatar, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('create_at', $this->create_at, true);
		$criteria->compare('last_visit', $this->last_visit, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
