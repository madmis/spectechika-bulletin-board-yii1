<?php

/**
 * This is the model class for table "{{category}}".
 *
 * The followings are the available columns in table '{{category}}':
 * @property string $id
 * @property string $parent_id
 * @property string $name
 * @property string $name_r
 * @property string $name_d
 * @property string $name_v
 * @property string $name_t
 * @property string $name_p
 * @property string $translit
 * @property string $description
 * @property string $title
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * The followings are the available model relations:
 * @property Buses[] $buses
 * @property ToRent[] $toRents
 * @property Rent[] $rents
 */
class CategoryBase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description', 'required'),
			array('parent_id', 'length', 'max'=>11),
			array('name, translit, title', 'length', 'max'=>100),
			array('name_r, name_d, name_v, name_t, name_p', 'length', 'max'=>255),
			array('meta_keywords, meta_description', 'length', 'max'=>200),
			// The following rule is used by search().
			array(
				'id, parent_id, name, translit, description, title, meta_keywords,
				name_r, name_d, name_v, name_t, name_p, meta_description',
				'safe', 'on'=>'search'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'buses' => array(self::HAS_MANY, 'Buses', 'category_id'),
			'toRents' => array(self::HAS_MANY, 'ToRent', 'category_id'),
			'rents' => array(self::HAS_MANY, 'Rent', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Parent',
			'name' => 'Name',
			'name_r' => 'Name R',
			'name_d' => 'Name D',
			'name_v' => 'Name V',
			'name_t' => 'Name T',
			'name_p' => 'Name P',
			'translit' => 'Translit',
			'description' => 'Description',
			'title' => 'Title',
			'meta_keywords' => 'Meta Keywords',
			'meta_description' => 'Meta Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('parent_id',$this->parent_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('name_r',$this->name_r,true);
		$criteria->compare('name_d',$this->name_d,true);
		$criteria->compare('name_v',$this->name_v,true);
		$criteria->compare('name_t',$this->name_t,true);
		$criteria->compare('name_p',$this->name_p,true);
		$criteria->compare('translit',$this->translit,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('meta_description',$this->meta_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
