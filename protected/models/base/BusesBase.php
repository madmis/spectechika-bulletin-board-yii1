<?php

/**
 * This is the model class for table "{{buses}}".
 *
 * The followings are the available columns in table '{{buses}}':
 * @property string $id
 * @property integer $user_id
 * @property string $photo
 * @property string $category_id
 * @property integer $price
 * @property string $features
 * @property string $description
 * @property string $create_at
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property User $user
 */
class BusesBase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{buses}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, photo, category_id, price, features, description', 'required'),
			array('user_id, price', 'numerical', 'integerOnly'=>true),
			array('category_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, photo, category_id, price, features, description, create_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'photo' => 'Фотография',
			'category_id' => 'Категория',
			'price' => 'Цена в час',
			'features' => 'Дополнительные характеристики',
			'description' => 'Дополнительное описание',
			'create_at' => 'Дата создания',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('category_id',$this->category_id,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('features',$this->features,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('create_at',$this->create_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
