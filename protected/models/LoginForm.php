<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel {
	public $email;
	public $password;
	public $rememberMe;

	private $__identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			// username and password are required
			array('email, password', 'required'),
			array('email', 'email'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'email' => Yii::t('main', 'Email'),
			'password' => Yii::t('main', 'Пароль'),
			'rememberMe' => Yii::t('main', 'Оставаться в системе'),
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute, $params) {
		if (!$this->hasErrors()) {
			$this->__identity = new TUserIdentity($this->email, $this->password);
			if (!$this->__identity->authenticate()) {
				$this->addError('password', Yii::t('main', 'Неверное имя пользователя или пароль'));
			}
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login() {
		if ($this->__identity === null) {
			$this->__identity = new TUserIdentity($this->email, $this->password);
			$this->__identity->authenticate();
		}
		if ($this->__identity->errorCode === TUserIdentity::ERROR_NONE) {
			$duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
			Yii::app()->user->login($this->__identity, $duration);
			return true;
		} else {
			return false;
		}
	}
}
