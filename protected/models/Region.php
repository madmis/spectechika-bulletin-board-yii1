<?php

class Region extends RegionBase {

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Region the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function getList() {
		$criteria = new CDbCriteria();
		$criteria->order = 'name ASC';
		$items = $this->findAll($criteria);
		return CHtml::listData($items, 'id', 'name', 'parent_id');
	}

}