<?php

class City extends CityBase {

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return City the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function getCitiesArray() {
		$criteria = new CDbCriteria();
		$criteria->order = 'name ASC';
		$items = City::model()->findAll($criteria);
		return CHtml::listData($items, 'id', 'name');
	}

	public function getIdByName($name) {
		$criteria = new CDbCriteria();
		$criteria->addCondition('name = :name');
		$criteria->params = array(':name' => $name);
		/** @var City $item */
		$item = $this->find($criteria);
		if ($item) {
			return $item->id;
		}

		return null;
	}
}