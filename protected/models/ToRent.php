<?php

class ToRent extends ToRentBase implements TPhotoHandlerInterface {

	const PHOTO_MINI_PREFIX = 'mini-';
	const PHOTO_MINI_WIDTH = 50;
	const PHOTO_MINI_HEIGHT = 50;
	const PHOTO_SMALL_PREFIX = 'small-';
	const PHOTO_SMALL_WIDTH = 100;
	const PHOTO_SMALL_HEIGHT = 100;
	const PHOTO_MEDIUM_PREFIX = 'medium-';
	const PHOTO_MEDIUM_WIDTH = 250;
	const PHOTO_MEDIUM_HEIGHT = 220;

	/**
	 * @return ToRent[]
	 */
	public function mainBlock() {
		$criteria = new CDbCriteria();
		$criteria->limit = Yii::app()->params->itemAt('mainBlockCount');
		$criteria->order = 'create_at DESC';

		return $this->findAll($criteria);
	}

	/**
	 * @return ToRent[]
	 */
	public function mainTable() {
		$criteria = new CDbCriteria();
		$criteria->limit = Yii::app()->params->itemAt('mainTableCount');
		$criteria->offset = Yii::app()->params->itemAt('mainBlockCount');
		$criteria->order = 'create_at DESC';

		return $this->findAll($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ToRent the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function getDataProvider() {
		return $this->baseDataProvider();
	}

	/**
	 * @param Category $category
	 * @return CActiveDataProvider
	 */
	public function byCategoryProvider(Category $category) {
		return $this->baseDataProvider($category);
	}

	/**
	 * @param Region $region
	 * @return CActiveDataProvider
	 */
	public function byRegionProvider(Region $region) {
		return $this->baseDataProvider(null, $region);
	}

	/**
	 * @param City $city
	 * @return CActiveDataProvider
	 */
	public function byCityProvider(City $city) {
		return $this->baseDataProvider(null, null, $city);
	}

	/**
	 * @param Category $Category
	 * @param Region $Region
	 * @param City $City
	 * @return CActiveDataProvider
	 */
	public function baseDataProvider(Category $Category = null, Region $Region = null, City $City = null) {
		$criteria = new CDbCriteria();
		$params = new CMap();
		if ($Category) {
			$criteria->addCondition('category_id = :category_id');
			$params->add(':category_id', $Category->id);
		}

		if ($Region) {
			$criteria->addCondition('region_id = :region_id');
			$params->add(':region_id', $Region->id);
		}

		if ($City) {
			$criteria->addCondition('city_id = :city_id');
			$params->add(':city_id', $City->id);
		}

		$criteria->params = $params->toArray();

		$sort = new CSort;
		$sort->defaultOrder = 't.create_at DESC';
		$sort->attributes = array(
			'create_at' => array(
				'asc' => 't.create_at',
				'desc' => 't.create_at DESC',
				'label' => 'create_at'
			),
			'price' => array(
				'asc' => 't.price',
				'desc' => 't.price DESC',
				'label' => 'price'
			),
		);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Yii::app()->params->itemAt('toRentPerPage'),
				'pageVar' => 'page',
			),
			'sort' => $sort,
		));
	}

	/**
	 * @param string $prefix
	 *            ToRent::PHOTO_SMALL_PREFIX, PHOTO_MEDIUM_PREFIX, empty prefix - original photo
	 * @return string
	 */
	public function getPhotoUrl($prefix = '') {
		$PhotoHandler = new TPhotoHandler($this);
		$url = $PhotoHandler->getPhotoUrl($this->photo, $prefix);

		if (!$url) {
			$url = Yii::app()->getAssetManager()->getPublishedUrl(
				Yii::getPathOfAlias('application.assets')
			);
			$url .= "/img/no-photo/{$prefix}no-photo.png";
		}

		return $url;
	}

	/**
	 * @return $this|bool
	 */
	public function uploadPhoto() {
		$photo = CUploadedFile::getInstance($this, 'photo');
		if (!$photo) {
			return false;
		}

		$PhotoHandler = new TPhotoHandler($this, $photo);
		$this->photo = $PhotoHandler->load();
		return $this;
	}

	public function uploadBusesPhoto(Buses $Buses) {
		if (!empty($Buses) && $Buses->photo) {
			$PhotoHandler = new TPhotoHandler($Buses);
			$path = $PhotoHandler->getPhotoPath($Buses->photo);
			if ($path && file_exists($path)) {
				$name = pathinfo($path, PATHINFO_BASENAME);
				$file = new CUploadedFile($name, $path, null, 0, 0);
				$PhotoHandler = new TPhotoHandler($this, $file);
				$this->photo = $PhotoHandler->load(false);
			}
		}
		return $this;
	}

	public function getBasePhotoUrl() {
		return '/uploads/photo/torent/';
	}

	public function getBasePhotoAlias() {
		return 'webroot.uploads.photo.torent';
	}

	/**
	 * Event onNewToRent
	 * @param CModelEvent $event
	 */
	public function onNewToRent(CModelEvent $event)
	{
		$this->raiseEvent('onNewToRent', $event);
	}

	public function beforeSave() {
		if (!$this->city_id) {
			$this->city_id = null;
		}

		$notifier = new TAdminNotifier();
		$this->onNewToRent = array($notifier, 'newToRent');

		return parent::beforeSave();
	}

	protected function afterSave()
	{
		if ($this->isNewRecord) {
			$event = new CModelEvent($this);
			$this->onNewToRent($event);
			if (!$event->isValid) {
				$mess = 'Error to send email from ToRent. Params: ' . print_r($this->attributes, true);
				Yii::log($mess, CLogger::LEVEL_WARNING, 'system.mail.ToRent');
			}
		}
		return parent::afterSave();
	}


	public function getPhotoSmallWidth() {
		return self::PHOTO_SMALL_WIDTH;
	}

	public function getPhotoSmallHeight() {
		return self::PHOTO_SMALL_HEIGHT;
	}

	public function getPhotoSmallPrefix() {
		return self::PHOTO_SMALL_PREFIX;
	}

	public function getPhotoMediumWidth() {
		return self::PHOTO_MEDIUM_WIDTH;
	}

	public function getPhotoMediumHeight() {
		return self::PHOTO_MEDIUM_HEIGHT;
	}

	public function getPhotoMediumPrefix() {
		return self::PHOTO_MEDIUM_PREFIX;
	}

	public function getPhotoMiniWidth() {
		return self::PHOTO_MINI_WIDTH;
	}

	public function getPhotoMiniHeight() {
		return self::PHOTO_MINI_HEIGHT;
	}

	public function getPhotoMiniPrefix() {
		return self::PHOTO_MINI_PREFIX;
	}
}