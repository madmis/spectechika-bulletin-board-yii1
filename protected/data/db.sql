-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.32-0ubuntu0.13.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-09-17 22:19:13
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table spec.auth_assignment
DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `itemname` varchar(64) NOT NULL COMMENT 'Role, task or operation name',
  `userid` int(11) NOT NULL COMMENT 'User id',
  `bizrule` text COMMENT 'Biznes rule fo role',
  `data` text COMMENT 'Additional data',
  PRIMARY KEY (`itemname`,`userid`),
  KEY `auth_assignment_user_fk_idx` (`userid`),
  KEY `auth_assignment_item_fk_idx` (`itemname`),
  CONSTRAINT `auth_assignment_item_fk` FOREIGN KEY (`itemname`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_assignment_user_fk` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Relations between roles and users';

-- Data exporting was unselected.


-- Dumping structure for table spec.auth_item
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL COMMENT 'Item name',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - operation, 1 - task, 2 - role',
  `description` text COMMENT 'Item description',
  `bizrule` text COMMENT 'Item biznes rule',
  `data` text COMMENT 'Additional data',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='roles, tasks and operations';

-- Data exporting was unselected.


-- Dumping structure for table spec.auth_item_child
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL COMMENT 'Parent item',
  `child` varchar(64) NOT NULL COMMENT 'Child item',
  PRIMARY KEY (`parent`,`child`),
  KEY `auth_item_child_parent_fk_idx` (`parent`),
  KEY `auth_item_child_child_fk_idx` (`child`),
  CONSTRAINT `auth_item_child_child_fk` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_parent_fk` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the hierarchy of roles, tasks and operations';

-- Data exporting was unselected.


-- Dumping structure for table spec.region
DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Кто? Что?',
  `name_r` varchar(255) NOT NULL DEFAULT '' COMMENT 'родительный Кого? Чего?',
  `name_d` varchar(255) NOT NULL DEFAULT '' COMMENT 'дательный Кому? Чему?',
  `name_v` varchar(255) NOT NULL DEFAULT '' COMMENT 'винительный Кого? Что?',
  `name_t` varchar(255) NOT NULL DEFAULT '' COMMENT 'творительный Кем? Чем?',
  `name_p` varchar(255) NOT NULL DEFAULT '' COMMENT 'предложный О ком? О чём?; В ком? В чём? Где ?',
  `translit` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_idx` (`translit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table spec.spec_article
DROP TABLE IF EXISTS `spec_article`;
CREATE TABLE IF NOT EXISTS `spec_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `translit` varchar(255) NOT NULL,
  `snippet` text NOT NULL,
  `text` text NOT NULL,
  `meta_keywords` varchar(200) NOT NULL,
  `meta_description` varchar(200) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `create_at_idx` (`create_at`),
  KEY `article_user_id` (`user_id`),
  CONSTRAINT `article_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table spec.spec_buses
DROP TABLE IF EXISTS `spec_buses`;
CREATE TABLE IF NOT EXISTS `spec_buses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `price` int(11) unsigned NOT NULL COMMENT 'цена в час',
  `features` text NOT NULL COMMENT 'дополнительные характеристики',
  `description` text NOT NULL COMMENT 'дополнительное описание',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_idx` (`user_id`),
  KEY `buses_category_fk` (`category_id`),
  CONSTRAINT `buses_category_fk` FOREIGN KEY (`category_id`) REFERENCES `spec_category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `buses_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Спецтехника';

-- Data exporting was unselected.


-- Dumping structure for table spec.spec_category
DROP TABLE IF EXISTS `spec_category`;
CREATE TABLE IF NOT EXISTS `spec_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT 'Кто? Что?',
  `name_r` varchar(255) NOT NULL DEFAULT '' COMMENT 'родительный Кого? Чего?',
  `name_d` varchar(255) NOT NULL DEFAULT '' COMMENT 'дательный Кому? Чему?',
  `name_v` varchar(255) NOT NULL DEFAULT '' COMMENT 'винительный Кого? Что?',
  `name_t` varchar(255) NOT NULL DEFAULT '' COMMENT 'творительный Кем? Чем?',
  `name_p` varchar(255) NOT NULL DEFAULT '' COMMENT 'предложный О ком? О чём?; В ком? В чём? Где ?',
  `translit` varchar(100) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `meta_keywords` varchar(200) NOT NULL DEFAULT '',
  `meta_description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_translit` (`translit`),
  UNIQUE KEY `unique_name` (`name`),
  KEY `translit_idx` (`translit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table spec.spec_city
DROP TABLE IF EXISTS `spec_city`;
CREATE TABLE IF NOT EXISTS `spec_city` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Кто? Что?',
  `name_r` varchar(255) NOT NULL DEFAULT '' COMMENT 'родительный Кого? Чего?',
  `name_d` varchar(255) NOT NULL DEFAULT '' COMMENT 'дательный Кому? Чему?',
  `name_v` varchar(255) NOT NULL DEFAULT '' COMMENT 'винительный Кого? Что?',
  `name_t` varchar(255) NOT NULL DEFAULT '' COMMENT 'творительный Кем? Чем?',
  `name_p` varchar(255) NOT NULL DEFAULT '' COMMENT 'предложный О ком? О чём?; В ком? В чём? Где ?',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`region_id`,`name`),
  CONSTRAINT `region_fk` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table spec.spec_news
DROP TABLE IF EXISTS `spec_news`;
CREATE TABLE IF NOT EXISTS `spec_news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `translit` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `tags` varchar(255) NOT NULL,
  `meta_keywords` varchar(200) NOT NULL,
  `meta_description` varchar(200) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `create_at_idx` (`create_at`),
  KEY `news_user_id` (`user_id`),
  CONSTRAINT `news_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table spec.spec_rent
DROP TABLE IF EXISTS `spec_rent`;
CREATE TABLE IF NOT EXISTS `spec_rent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `region_id` int(11) unsigned NOT NULL,
  `city_id` int(11) unsigned DEFAULT NULL,
  `max_price` int(11) unsigned NOT NULL COMMENT 'Максимальная цена в час',
  `description` text NOT NULL,
  `fio` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `phone` char(19) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Статус объявления: 0 - активное; 1 - закрыто и выбран исполнитель; 2 - закрыто без выбора исполнителя;',
  `performer_id` int(11) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rent_user_id_fk` (`user_id`),
  KEY `rent_category_id_fk` (`category_id`),
  KEY `rent_region_id_fk` (`region_id`),
  KEY `rent_city_id_fk` (`city_id`),
  KEY `rent_performer_id` (`performer_id`),
  CONSTRAINT `rent_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `spec_category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `rent_city_id_fk` FOREIGN KEY (`city_id`) REFERENCES `spec_city` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `rent_performer_id` FOREIGN KEY (`performer_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `rent_region_id_fk` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `rent_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table spec.spec_rent_bid
DROP TABLE IF EXISTS `spec_rent_bid`;
CREATE TABLE IF NOT EXISTS `spec_rent_bid` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rent_id` int(11) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `buse_id` int(11) unsigned NOT NULL,
  `price` int(11) unsigned NOT NULL COMMENT 'цена в час',
  `description` text NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rent_bid_user_id_fk` (`user_id`),
  KEY `rent_bid_buse_id` (`buse_id`),
  KEY `rent_bid_rent_id` (`rent_id`),
  CONSTRAINT `rent_bid_buse_id` FOREIGN KEY (`buse_id`) REFERENCES `spec_buses` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `rent_bid_rent_id` FOREIGN KEY (`rent_id`) REFERENCES `spec_rent` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `rent_bid_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Заявки к объявлению';

-- Data exporting was unselected.


-- Dumping structure for table spec.spec_to_rent
DROP TABLE IF EXISTS `spec_to_rent`;
CREATE TABLE IF NOT EXISTS `spec_to_rent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `region_id` int(11) unsigned NOT NULL,
  `city_id` int(11) unsigned DEFAULT NULL,
  `price` int(11) NOT NULL COMMENT 'цена в час',
  `features` text NOT NULL COMMENT 'дополнительные характеристики',
  `description` text NOT NULL COMMENT 'дополнительное описание',
  `fio` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `phone` char(19) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `category_id_fk` (`category_id`),
  KEY `region_id_fk` (`region_id`),
  KEY `city_id_fk` (`city_id`),
  CONSTRAINT `category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `spec_category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `city_id_fk` FOREIGN KEY (`city_id`) REFERENCES `spec_city` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `region_id_fk` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table spec.tbl_migration
DROP TABLE IF EXISTS `tbl_migration`;
CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table spec.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL DEFAULT '',
  `company_name` varchar(200) NOT NULL DEFAULT '',
  `address` text NOT NULL,
  `region_id` int(11) unsigned DEFAULT NULL,
  `city_id` int(11) unsigned DEFAULT NULL,
  `description` text NOT NULL COMMENT 'Описание деятельности',
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(64) NOT NULL DEFAULT '',
  `avatar` varchar(255) NOT NULL DEFAULT '',
  `phone` char(19) NOT NULL DEFAULT '',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_visit` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `user_city_id_fk` (`city_id`),
  KEY `user_region_id_fk` (`region_id`),
  CONSTRAINT `user_city_id_fk` FOREIGN KEY (`city_id`) REFERENCES `spec_city` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `user_region_id_fk` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table spec.user_service
DROP TABLE IF EXISTS `user_service`;
CREATE TABLE IF NOT EXISTS `user_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'ID пользователя',
  `service` text NOT NULL COMMENT 'идентификатор сервиса',
  `identity` text NOT NULL COMMENT 'идентификатор пользователя в сервисе',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_service_unique` (`user_id`,`service`(20),`identity`(255)),
  KEY `userid` (`user_id`),
  CONSTRAINT `user_service_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица соответствий пользователей и сервисов';

-- Data exporting was unselected.
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
