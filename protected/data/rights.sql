-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.32-0ubuntu0.13.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-09-01 21:46:45
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table spec.auth_item
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL COMMENT 'Item name',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - operation, 1 - task, 2 - role',
  `description` text COMMENT 'Item description',
  `bizrule` text COMMENT 'Item biznes rule',
  `data` text COMMENT 'Additional data',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='roles, tasks and operations';

-- Dumping data for table spec.auth_item: ~57 rows (approximately)
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
	('Admin', 2, 'Amins', '', ''),
	('Article.*', 1, 'Article.*', NULL, NULL),
	('Article.create', 0, 'Article.create', NULL, NULL),
	('Article.index', 0, 'Article.index', NULL, NULL),
	('Article.view', 0, 'Article.view', NULL, NULL),
	('Authenticate.*', 1, 'Authenticate.*', NULL, NULL),
	('Authenticate.login', 0, 'Authenticate.login', NULL, NULL),
	('Authenticate.logout', 0, 'Authenticate.logout', NULL, NULL),
	('Authenticated', 2, 'Authenticated users', '', ''),
	('City.*', 1, 'City.*', NULL, NULL),
	('City.clientCities', 0, 'City.clientCities', NULL, NULL),
	('City.clientCitiesOld', 0, 'City.clientCitiesOld', NULL, NULL),
	('City.getRegion', 0, 'City.getRegion', NULL, NULL),
	('Guest', 2, 'Guest', '', ''),
	('MyBuses.*', 1, 'MyBuses.*', NULL, '(NULL)'),
	('MyBuses.admin', 0, 'MyBuses.admin', NULL, NULL),
	('MyBuses.Authorized', 1, 'MyBuses.Authorized', NULL, NULL),
	('MyBuses.create', 0, 'MyBuses.create', NULL, NULL),
	('MyBuses.delete', 0, 'MyBuses.delete', NULL, NULL),
	('MyBuses.index', 0, 'MyBuses.index', NULL, NULL),
	('MyBuses.update', 0, 'MyBuses.update', NULL, NULL),
	('MyBuses.view', 0, 'MyBuses.view', NULL, NULL),
	('News.*', 1, 'News.*', NULL, NULL),
	('News.create', 0, 'News.create', NULL, NULL),
	('News.index', 0, 'News.index', NULL, NULL),
	('News.view', 0, 'News.view', NULL, NULL),
	('Profile.*', 1, 'Profile.*', NULL, '(NULL)'),
	('Profile.addService', 0, 'Profile.addService', NULL, NULL),
	('Profile.Authorized', 1, 'Profile.Authorized', NULL, NULL),
	('Profile.removeService', 0, 'Profile.removeService', NULL, NULL),
	('Profile.saveProfile', 0, 'Profile.saveProfile', NULL, NULL),
	('Profile.settings', 0, 'Profile.settings', NULL, NULL),
	('Profile.social', 0, 'Profile.social', NULL, NULL),
	('Profile.updateService', 0, 'Profile.updateService', NULL, NULL),
	('Rent.*', 1, 'Rent.*', NULL, NULL),
	('Rent.add', 0, 'Rent.add', NULL, NULL),
	('Rent.Authorized', 1, 'Rent.Authorized', NULL, NULL),
	('Rent.category', 0, 'Rent.category', NULL, NULL),
	('Rent.city', 0, 'Rent.city', NULL, NULL),
	('Rent.index', 0, 'Rent.index', NULL, NULL),
	('Rent.region', 0, 'Rent.region', NULL, NULL),
	('Rent.search', 0, 'Rent.search', NULL, NULL),
	('Rent.setStatus', 0, 'Rent.setStatus', NULL, NULL),
	('Rent.view', 0, 'Rent.view', NULL, NULL),
	('RentBid.*', 1, 'RentBid.*', NULL, NULL),
	('RentBid.add', 0, 'RentBid.add', NULL, NULL),
	('Site.*', 1, 'Site.*', NULL, NULL),
	('Site.contact', 0, 'Site.contact', NULL, NULL),
	('Site.error', 0, 'Site.error', NULL, NULL),
	('Site.index', 0, 'Site.index', NULL, NULL),
	('ToRent.*', 1, 'ToRent.*', NULL, NULL),
	('ToRent.add', 0, 'ToRent.add', NULL, NULL),
	('ToRent.category', 0, 'ToRent.category', NULL, NULL),
	('ToRent.city', 0, 'ToRent.city', NULL, NULL),
	('ToRent.region', 0, 'ToRent.region', NULL, NULL),
	('ToRent.search', 0, 'ToRent.search', NULL, NULL),
	('ToRent.view', 0, 'ToRent.view', NULL, NULL);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;


-- Dumping structure for table spec.auth_item_child
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL COMMENT 'Parent item',
  `child` varchar(64) NOT NULL COMMENT 'Child item',
  PRIMARY KEY (`parent`,`child`),
  KEY `auth_item_child_parent_fk_idx` (`parent`),
  KEY `auth_item_child_child_fk_idx` (`child`),
  CONSTRAINT `auth_item_child_child_fk` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_parent_fk` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the hierarchy of roles, tasks and operations';

-- Dumping data for table spec.auth_item_child: ~65 rows (approximately)
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
	('Admin', 'Article.*'),
	('Admin', 'Authenticate.*'),
	('Admin', 'City.*'),
	('Admin', 'MyBuses.*'),
	('Admin', 'News.*'),
	('Admin', 'Profile.*'),
	('Admin', 'Rent.*'),
	('Admin', 'RentBid.*'),
	('Admin', 'Site.*'),
	('Admin', 'ToRent.*'),
	('Article.*', 'Article.create'),
	('Article.*', 'Article.index'),
	('Article.*', 'Article.view'),
	('Authenticate.*', 'Authenticate.login'),
	('Authenticate.*', 'Authenticate.logout'),
	('Authenticated', 'MyBuses.Authorized'),
	('Authenticated', 'Profile.Authorized'),
	('Authenticated', 'Rent.Authorized'),
	('City.*', 'City.clientCities'),
	('City.*', 'City.clientCitiesOld'),
	('City.*', 'City.getRegion'),
	('MyBuses.*', 'MyBuses.admin'),
	('MyBuses.*', 'MyBuses.create'),
	('MyBuses.*', 'MyBuses.delete'),
	('MyBuses.*', 'MyBuses.index'),
	('MyBuses.*', 'MyBuses.update'),
	('MyBuses.*', 'MyBuses.view'),
	('MyBuses.Authorized', 'MyBuses.create'),
	('MyBuses.Authorized', 'MyBuses.index'),
	('MyBuses.Authorized', 'MyBuses.view'),
	('News.*', 'News.create'),
	('News.*', 'News.index'),
	('News.*', 'News.view'),
	('Profile.*', 'Profile.addService'),
	('Profile.*', 'Profile.removeService'),
	('Profile.*', 'Profile.saveProfile'),
	('Profile.*', 'Profile.settings'),
	('Profile.*', 'Profile.social'),
	('Profile.*', 'Profile.updateService'),
	('Profile.Authorized', 'Profile.addService'),
	('Profile.Authorized', 'Profile.removeService'),
	('Profile.Authorized', 'Profile.saveProfile'),
	('Profile.Authorized', 'Profile.settings'),
	('Profile.Authorized', 'Profile.social'),
	('Profile.Authorized', 'Profile.updateService'),
	('Rent.*', 'Rent.add'),
	('Rent.*', 'Rent.category'),
	('Rent.*', 'Rent.city'),
	('Rent.*', 'Rent.index'),
	('Rent.*', 'Rent.region'),
	('Rent.*', 'Rent.search'),
	('Rent.*', 'Rent.setStatus'),
	('Rent.*', 'Rent.view'),
	('Rent.Authorized', 'Rent.add'),
	('Rent.Authorized', 'Rent.setStatus'),
	('RentBid.*', 'RentBid.add'),
	('Site.*', 'Site.contact'),
	('Site.*', 'Site.error'),
	('Site.*', 'Site.index'),
	('ToRent.*', 'ToRent.add'),
	('ToRent.*', 'ToRent.category'),
	('ToRent.*', 'ToRent.city'),
	('ToRent.*', 'ToRent.region'),
	('ToRent.*', 'ToRent.search'),
	('ToRent.*', 'ToRent.view');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
