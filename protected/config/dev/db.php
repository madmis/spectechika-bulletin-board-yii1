<?php

return array(
	'connectionString' => 'mysql:host=127.0.0.1;dbname=spec',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => 'root',
	'charset' => 'utf8',
	'tablePrefix'=>'spec_',
	// включаем профайлер
	'enableProfiling' => true,
	// показываем значения параметров
	'enableParamLogging' => true,
	'schemaCachingDuration' => 3600,
);