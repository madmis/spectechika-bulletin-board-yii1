<?php
return array( // You can change the providers and their classes.
	'vkontakte' => array(
		// register your app here: https://vk.com/editapp?act=create&site=1
		'class' => 'VKontakteOAuth',
		'client_id' => '',
		'client_secret' => '',
	),
	'facebook' => array(
		// register your app here: https://developers.facebook.com/apps/
		'class' => 'FacebookOAuth',
		'client_id' => '',
		'client_secret' => '',
	),
	'twitter' => array(
		// register your app here: https://dev.twitter.com/apps/new
		//'class' => 'TwitterOAuthService',
		'class' => 'TwitterOAuth',
		'key' => '',
		'secret' => '',
	),
	'google' => array(
		//'class' => 'GoogleOpenIDService',
		'class' => 'GoogleOpenID',
	),
	//				'yandex' => array(
	//					'class' => 'YandexOpenIDService',
	//				),
	//				'google_oauth' => array(
	//					// register your app here: https://code.google.com/apis/console/
	//					'class' => 'GoogleOAuthService',
	//					'client_id' => '...',
	//					'client_secret' => '...',
	//					'title' => 'Google (OAuth)',
	//				),
	//				'yandex_oauth' => array(
	//					// register your app here: https://oauth.yandex.ru/client/my
	//					'class' => 'YandexOAuthService',
	//					'client_id' => '...',
	//					'client_secret' => '...',
	//					'title' => 'Yandex (OAuth)',
	//				),
	//				'linkedin' => array(
	//					// register your app here: https://www.linkedin.com/secure/developer
	//					'class' => 'LinkedinOAuthService',
	//					'key' => '...',
	//					'secret' => '...',
	//				),
	//				'github' => array(
	//					// register your app here: https://github.com/settings/applications
	//					'class' => 'GitHubOAuthService',
	//					'client_id' => '...',
	//					'client_secret' => '...',
	//				),
	//				'live' => array(
	//					// register your app here: https://manage.dev.live.com/Applications/Index
	//					'class' => 'LiveOAuthService',
	//					'client_id' => '...',
	//					'client_secret' => '...',
	//				),
	//				'mailru' => array(
	//					// register your app here: http://api.mail.ru/sites/my/add
	//					'class' => 'MailruOAuthService',
	//					'client_id' => '...',
	//					'client_secret' => '...',
	//				),
	//				'moikrug' => array(
	//					// register your app here: https://oauth.yandex.ru/client/my
	//					'class' => 'MoikrugOAuthService',
	//					'client_id' => '...',
	//					'client_secret' => '...',
	//				),
	//				'odnoklassniki' => array(
	//					// register your app here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
	//					'class' => 'OdnoklassnikiOAuthService',
	//					'client_id' => '...',
	//					'client_public' => '...',
	//					'client_secret' => '...',
	//					'title' => 'Odnokl.',
	//				),
);
