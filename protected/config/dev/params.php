<?php
return array(
	'adminEmail' => '',
	'salt' => '_rev2b4kf56&^',
	'contactEmail' => '',
	'phoneMask' => '+380 (99) 999 99 99',
	'currency' => 'UAH',
	'slogan' => 'аренда спецтехники в Украине',
	'toRentPerPage' => 5,
	'rentPerPage' => 5,
	'newsPerPage' => 5,
	'lastNewsCount' => 5,
	'mainBlockCount' => 4,
	'mainTableCount' => 5,
	'articlesPerPage' => 5,
	'lastArticlesCount' => 5,
	'sitemapItemsCount' => 100,
);
