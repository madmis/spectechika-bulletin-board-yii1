<?php
return array(
	'class' => 'CLogRouter',
	'routes' => array(
		array(
			'class' => 'CFileLogRoute',
			'levels' => 'error, warning, trace',
			'enabled' => true,
			// логируем sql запросы в файл
			// 'levels' => 'trace',
			'categories'=>'system.db.*',
			'LogFile' => 'db.trace',
		),
		array(
			// направляем результаты профайлинга в ProfileLogRoute (отображается
			// внизу страницы)
			'class' => 'CProfileLogRoute',
			'levels' => 'profile',
			'enabled' => false,
		),
		// uncomment the following to show log messages on web pages
		array(
			'class' => 'CWebLogRoute',
			'categories' => 'system.db.*',
			'levels' => 'error, warning, trace, profile, info',
			'showInFireBug' => false,
			'enabled' => false,
		),
	),
);