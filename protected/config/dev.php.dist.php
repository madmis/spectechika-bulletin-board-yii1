<?php

return CMap::mergeArray(
// наследуемся от main.php
	require(dirname(__FILE__) . '/main.php'),
	array(
		// preloading 'log' component
		'preload' => array('log', 'debug'),
		'modules' => array(
			// uncomment the following to enable the Gii tool
			'gii' => array(
				'class' => 'system.gii.GiiModule',
				'password' => '1',
				// If removed, Gii defaults to localhost only. Edit carefully to taste.
				'ipFilters' => array(),
			),
		),
		// application components
		'components' => array(
			// uncomment the following to use a MySQL database
			'db' => require(dirname(__FILE__) . '/dev/db.php'),
			'eauth' => array(
				'class' => 'ext.eauth.EAuth',
				'popup' => true, // Use the popup window instead of redirecting.
				'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache'.
				'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
				'services' => require(dirname(__FILE__) . '/dev/services.php'),
			),
			'log' => require(dirname(__FILE__) . '/dev/log.php'),
			'mailer' => require(dirname(__FILE__) . '/dev/mailer.php'),
		),
		// application-level parameters that can be accessed
		// using Yii::app()->params->itemAt(paramName)
		'params' => require(dirname(__FILE__) . '/dev/params.php'),
	)
);