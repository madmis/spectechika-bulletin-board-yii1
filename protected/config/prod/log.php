<?php
return array(
	'class' => 'CLogRouter',
	'routes'=>array(
		array(
			'class'=>'CFileLogRoute',
			'levels'=>'trace, info, error, warning',
		),
		array(
			'class'=>'CEmailLogRoute',
			'levels'=>'error, warning',
			'emails'=>'',
		),
	),
);
