<?php
return array(
	'vkontakte' => array(
		// register your app here: https://vk.com/editapp?act=create&site=1
		'class' => 'VKontakteOAuth',
		'client_id' => '',
		'client_secret' => '',
	),
	'facebook' => array(
		// register your app here: https://developers.facebook.com/apps/
		'class' => 'FacebookOAuth',
		'client_id' => '',
		'client_secret' => '',
	),
	'twitter' => array(
		// register your app here: https://dev.twitter.com/apps/new
		'class' => 'TwitterOAuth',
		'key' => '',
		'secret' => '',
	),
	'google' => array(
		'class' => 'GoogleOpenID',
	),
);
