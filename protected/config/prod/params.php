<?php
return array(
	'adminEmail' => '',
	'salt' => '_rev2b4kf56&^',
	'contactEmail' => '',
	'phoneMask' => '+380 (99) 999 99 99',
	'currency' => 'UAH',
	'slogan' => 'аренда спецтехники в Украине',
	'toRentPerPage' => 20,
	'rentPerPage' => 20,
	'newsPerPage' => 20,
	'lastNewsCount' => 5,
	'mainBlockCount' => 4,
	'mainTableCount' => 5,
	'articlesPerPage' => 20,
	'lastArticlesCount' => 5,
	'sitemapItemsCount' => 100,
);
