<?php

return array(
	'connectionString' => 'mysql:host=127.0.0.1;dbname=',
	'emulatePrepare' => true,
	'username' => '',
	'password' => '',
	'charset' => 'utf8',
	'tablePrefix'=>'spec_',
	// включаем профайлер
	'enableProfiling' => false,
	// показываем значения параметров
	'enableParamLogging' => false,
	'schemaCachingDuration' => 3600,
);
