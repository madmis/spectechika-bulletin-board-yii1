<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');
return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'sourceLanguage' => 'en_US',
	'language' => 'ru',
	'charset' => 'utf-8',
	'name' => 'СпецАвто',
	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.models.base.*',
		'application.components.*',
		'application.components.services.*',
		'application.components.validators.*',
		'application.components.notify.*',
		'application.interfaces.*',
		'ext.eoauth.*',
		'ext.eoauth.lib.*',
		'ext.lightopenid.*',
		'ext.eauth.*',
		'ext.eauth.services.*',
		'ext.eauth.custom_services.*',
		'application.modules.auth.*',
		'application.modules.auth.components.*',
		'application.utils.*',
		'application.helpers.*',
		'application.components.ImageHandler.*',
	),
	'modules' => array(
		'auth' => array(
			'strictMode' => true, // when enabled authorization items cannot be assigned children of the same type.
			'userClass' => 'User', // the name of the user model class.
			'userIdColumn' => 'id', // the name of the user id column.
			'userNameColumn' => 'username', // the name of the user name column.
			'appLayout' => 'webroot.themes.classic.views.layouts.authMain', // the layout used by the module.
			'viewDir' => null, // the path to view files to use with this module.
		),
	),
	// application components
	'components' => array(
		'cache' => array(
			'class' => 'system.caching.CFileCache',
		),
		'debug' => array(
			'class' => 'ext.yii2-debug.Yii2Debug',
			'enabled' => YII_DEBUG,
			'panels' => array(),
		),
		'user' => array(
			'class' => 'TWebUser',
			// enable cookie-based authentication
			'allowAutoLogin' => true,
			'loginUrl' => array('/authenticate/login'),
		),
		'authManager' => array(
			'class' => 'CDbAuthManager',
			'connectionID' => 'db',
			'itemTable' => 'auth_item',
			'itemChildTable' => 'auth_item_child',
			'assignmentTable' => 'auth_assignment',
			'behaviors' => array(
				'auth' => array(
					'class' => 'AuthBehavior',
					// users with full access
					'admins' => array('administrator', 'admin', 'Administrator', 'Admin'),
				),
			),
		),
		'urlManager' => require(dirname(__FILE__) . '/route.php'),
		'loid' => array(
			'class' => 'ext.lightopenid.loid',
		),
		'errorHandler' => array(
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.Bootstrap',
		),
		'mailMessage' => array(
			'class' => 'ext.mailMessage.MailMessage',
			'viewPath' => 'webroot.themes.classic.views.mail',
			'htmlLayout' => 'webroot.themes.classic.views.mail.html.layout',
			'textLayout' => 'webroot.themes.classic.views.mail.text.layout',
		),
		'format' => array(
			'class' => 'TFormatter',
			'dateFormat' => 'd.m.Y',
			'timeFormat' => 'H:i:s',
			'datetimeFormat' => 'd.m.Y H:i',
			'numberFormat' => array('decimals' => null, 'decimalSeparator' => '.', 'thousandSeparator' => ','),
//			'booleanFormat' => array('No','Yes'),
			'clientDateFormat' => 'dd.mm.yyyy',
			'clientTimeFormat' => 'hh:ii',
			'clientDateTimeFormat' => 'dd.mm.yyyy hh:ii',
		),
		'curl' => array(
			'class' => 'application.components.curl.Curl',
		),
	),
	'theme' => 'classic',
);