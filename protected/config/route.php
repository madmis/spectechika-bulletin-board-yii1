<?php
return array(
	'urlFormat' => 'path',
	'showScriptName' => false,
	'rules' => array(
		/**
		 * Sitemap
		 */
		array(
			'sitemap/index',
			'pattern'=>'sitemap.xml',
			'urlSuffix'=>''
		),
		/** Articles * */
		array(
			'article/create',
			'pattern' => 'article/create',
		),
		array(
			'article/index',
			'pattern' => 'article/page/<page:\d+>',
			'verb' => 'GET'
		),
		array(
			'article/view',
			'pattern' => 'article/<translit:([\w\-]+)>',
			'verb' => 'GET'
		),
		array(
			'article/index',
			'pattern' => 'article',
			'verb' => 'GET'
		),
		/** NEWS * */
		array(
			'news/create',
			'pattern' => 'news/create',
		),
		array(
			'news/index',
			'pattern' => 'news/page/<page:\d+>',
			'verb' => 'GET'
		),
		array(
			'news/view',
			'pattern' => 'news/<translit:([\w\-]+)>',
			'verb' => 'GET'
		),
		array(
			'news/index',
			'pattern' => 'news',
			'verb' => 'GET'
		),
		/** RENT */
		'rent/<id:\d+>/setStatus' => 'rent/setStatus',
		/** rent main page */
		array(
			'rent/index',
			'pattern' => 'rent/page/<page:\d+>/<sort:(create_at|create_at.desc|max_price|max_price.desc)>',
			'verb' => 'GET'
		),
		array(
			'rent/index',
			'pattern' => 'rent/page/<page:\d+>',
			'verb' => 'GET'
		),
		array(
			'rent/index',
			'pattern' => 'rent/<sort:(create_at|create_at.desc|max_price|max_price.desc)>',
			'verb' => 'GET'
		),
		array(
			'rent/index',
			'pattern' => 'rent',
			'verb' => 'GET'
		),
		/** to rent by category */
		array(
			'rent/category',
			'pattern' => 'rent/category/<category:([\w\-]+)>/page/<page:\d+>/<sort:(create_at|create_at.desc|max_price|max_price.desc)>',
			'verb' => 'GET'
		),
		array(
			'rent/category',
			'pattern' => 'rent/category/<category:([\w\-]+)>/page/<page:\d+>',
			'verb' => 'GET'
		),
		array(
			'rent/category',
			'pattern' => 'rent/category/<category:([\w\-]+)>/<sort:(create_at|create_at.desc|max_price|max_price.desc)>',
			'verb' => 'GET'
		),
		array(
			'rent/category',
			'pattern' => 'rent/category/<category:([\w\-]+)>',
			'verb' => 'GET'
		),
		/** to rent by region */
		array(
			'rent/region',
			'pattern' => 'rent/region/<region:([\w\-]+)>/page/<page:\d+>/<sort:(create_at|create_at.desc|max_price|max_price.desc)>',
			'verb' => 'GET'
		),
		array(
			'rent/region',
			'pattern' => 'rent/region/<region:([\w\-]+)>/page/<page:\d+>',
			'verb' => 'GET'
		),
		array(
			'rent/region',
			'pattern' => 'rent/region/<region:([\w\-]+)>/<sort:(create_at|create_at.desc|max_price|max_price.desc)>',
			'verb' => 'GET'
		),
		array(
			'rent/region',
			'pattern' => 'rent/region/<region:([\w\-]+)>',
			'verb' => 'GET'
		),
		/** to rent by city */
		array(
			'rent/city',
			'pattern' => 'rent/city/<city:\d+>/page/<page:\d+>/<sort:(create_at|create_at.desc|max_price|max_price.desc)>',
			'verb' => 'GET'
		),
		array(
			'rent/city',
			'pattern' => 'rent/city/<city:\d+>/page/<page:\d+>',
			'verb' => 'GET'
		),
		array(
			'rent/city',
			'pattern' => 'rent/city/<city:\d+>/<sort:(create_at|create_at.desc|max_price|max_price.desc)>',
			'verb' => 'GET'
		),
		array(
			'rent/city',
			'pattern' => 'rent/city/<city:\d+>',
			'verb' => 'GET'
		),

		/** TO RENT */
		/** to rent main page */
		array(
			'toRent/index',
			'pattern' => 'toRent/page/<page:\d+>/<sort:(create_at|create_at.desc|price|price.desc)>',
			'verb' => 'GET'
		),
		array(
			'toRent/index',
			'pattern' => 'toRent/page/<page:\d+>',
			'verb' => 'GET'
		),
		array(
			'toRent/index',
			'pattern' => 'toRent/<sort:(create_at|create_at.desc|price|price.desc)>',
			'verb' => 'GET'
		),
		array(
			'toRent/index',
			'pattern' => 'toRent',
			'verb' => 'GET'
		),
		/** to rent by category */
		array(
			'toRent/category',
			'pattern' => 'toRent/category/<category:([\w\-]+)>/page/<page:\d+>/<sort:(create_at|create_at.desc|price|price.desc)>',
			'verb' => 'GET'
		),
		array(
			'toRent/category',
			'pattern' => 'toRent/category/<category:([\w\-]+)>/page/<page:\d+>',
			'verb' => 'GET'
		),
		array(
			'toRent/category',
			'pattern' => 'toRent/category/<category:([\w\-]+)>/<sort:(create_at|create_at.desc|price|price.desc)>',
			'verb' => 'GET'
		),
		array(
			'toRent/category',
			'pattern' => 'toRent/category/<category:([\w\-]+)>',
			'verb' => 'GET'
		),
		/** to rent by region */
		array(
			'toRent/region',
			'pattern' => 'toRent/region/<region:([\w\-]+)>/page/<page:\d+>/<sort:(create_at|create_at.desc|price|price.desc)>',
			'verb' => 'GET'
		),
		array(
			'toRent/region',
			'pattern' => 'toRent/region/<region:([\w\-]+)>/page/<page:\d+>',
			'verb' => 'GET'
		),
		array(
			'toRent/region',
			'pattern' => 'toRent/region/<region:([\w\-]+)>/<sort:(create_at|create_at.desc|price|price.desc)>',
			'verb' => 'GET'
		),
		array(
			'toRent/region',
			'pattern' => 'toRent/region/<region:([\w\-]+)>',
			'verb' => 'GET'
		),
		/** to rent by city */
		array(
			'toRent/city',
			'pattern' => 'toRent/city/<city:\d+>/page/<page:\d+>/<sort:(create_at|create_at.desc|price|price.desc)>',
			'verb' => 'GET'
		),
		array(
			'toRent/city',
			'pattern' => 'toRent/city/<city:\d+>/page/<page:\d+>',
			'verb' => 'GET'
		),
		array(
			'toRent/city',
			'pattern' => 'toRent/city/<city:\d+>/<sort:(create_at|create_at.desc|price|price.desc)>',
			'verb' => 'GET'
		),
		array(
			'toRent/city',
			'pattern' => 'toRent/city/<city:\d+>',
			'verb' => 'GET'
		),


		'city/clientCitiesOld/<query>' => 'city/clientCitiesOld',
		'contact' => 'site/contact',
		'<view(about|faq)>' => 'site/page',
		'<controller:\w+>/<id:\d+>' => '<controller>/view',
		'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
		'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
	),
);