<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'sourceLanguage' => 'en_US',
	'language' => 'ru',
	'charset' => 'utf-8',
	'name' => 'СпецАвто',

	// preloading 'log' component
	'preload' => array('log'),
	'import' => array(
		'application.models.*',
		'application.models.base.*',
		'application.components.*',
		'application.components.services.*',
		'application.utils.*',
		'application.helpers.*',
	),
	// application components
	'components' => array(
		'request' => array(
			'hostInfo' => 'http://spec-auto.com.ua/',
			'baseUrl' => '/',
			'scriptUrl' => 'index.php',
		),
		// uncomment the following to use a MySQL database
		'db' => require(dirname(__FILE__) . '/prod/db.php'),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
		'urlManager' => array(
			'urlFormat' => 'path',
			'showScriptName' => false,
		),
		'curl' => array(
			'class' => 'application.components.curl.Curl',
		),
		'format' => array(
			'class' => 'TFormatter',
			'dateFormat' => 'd.m.Y',
			'timeFormat' => 'H:i:s',
			'datetimeFormat' => 'd.m.Y H:i',
			'numberFormat' => array('decimals' => null, 'decimalSeparator' => '.', 'thousandSeparator' => ','),
			'clientDateFormat' => 'dd.mm.yyyy',
			'clientTimeFormat' => 'hh:ii',
			'clientDateTimeFormat' => 'dd.mm.yyyy hh:ii',
		),
	),
);