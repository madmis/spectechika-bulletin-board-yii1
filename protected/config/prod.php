<?php

return CMap::mergeArray(
// наследуемся от main.php
	require(dirname(__FILE__) . '/main.php'),
	array(
		// preloading 'log' component
		'preload' => array('log'),
		// application components
		'components' => array(
			// uncomment the following to use a MySQL database
			'db' => require(dirname(__FILE__) . '/prod/db.php'),
			'eauth' => array(
				'class' => 'ext.eauth.EAuth',
				'popup' => true, // Use the popup window instead of redirecting.
				'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache'.
				'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
				'services' => require(dirname(__FILE__) . '/prod/services.php'),
			),
			'log' => require(dirname(__FILE__) . '/prod/log.php'),
			'mailer' => require(dirname(__FILE__) . '/prod/mailer.php'),
		),
		// application-level parameters that can be accessed
		// using Yii::app()->params->itemAt(paramName)
		'params' => require(dirname(__FILE__) . '/prod/params.php'),
	)
);