<?php
/**
 * String helper
 */

class TString
{

	/**
	 * Transliterate a given string.
	 * @param string $string The string you want to transliterate.
	 * @return string A string representing the transliterated version of the input string.
	 */
	public static function rus2translit($string)
	{
		$converter = array(
			'а' => 'a', 'б' => 'b', 'в' => 'v',
			'г' => 'g', 'д' => 'd', 'е' => 'e',
			'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
			'и' => 'i', 'й' => 'y', 'к' => 'k',
			'л' => 'l', 'м' => 'm', 'н' => 'n',
			'о' => 'o', 'п' => 'p', 'р' => 'r',
			'с' => 's', 'т' => 't', 'у' => 'u',
			'ф' => 'f', 'х' => 'h', 'ц' => 'c',
			'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
			'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
			'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
			'і' => 'i', 'ї' => 'i', 'є' => 'e',
			'А' => 'A', 'Б' => 'B', 'В' => 'V',
			'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
			'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
			'И' => 'I', 'Й' => 'Y', 'К' => 'K',
			'Л' => 'L', 'М' => 'M', 'Н' => 'N',
			'О' => 'O', 'П' => 'P', 'Р' => 'R',
			'С' => 'S', 'Т' => 'T', 'У' => 'U',
			'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
			'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
			'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
			'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',

			'І' => 'I', 'Ї' => 'I', 'Є' => 'E',

			'«' => '"', '»' => '"', '—' => '-'
		);

		return strtr($string, $converter);
	}

	/**
	 * Transliterate string for url
	 * @param $string
	 * @return string
	 */
	public static function str2url($string)
	{
		$string = mb_strtolower(self::rus2translit($string));
		$string = preg_replace(array('~[\']+~', '~[^-\w]+~', '~(:?-){2,}~'), array('', '-', '-'), $string);
		$string = trim($string, '-');
		return $string;
	}

	/**
	 * trims text to a space then adds ellipses if desired
	 * @param string $input text to trim
	 * @param int $length in characters to trim to
	 * @param bool $ellipses if ellipses (...) are to be added
	 * @param bool $strip_html if html tags are to be stripped
	 * @return string
	 */
	public static function cuteText($input, $length, $ellipses = true, $strip_html = true)
	{
		$charset = Yii::app()->charset;
		//strip tags, if desired
		if ($strip_html) {
			$input = strip_tags($input);
		}

		//no need to trim, already shorter than trim length
		if (mb_strlen($input, $charset) <= $length) {
			return $input;
		}

		//find last space within length
		$last_space = mb_strpos($input, ' ', $length, $charset);
		$trimmed_text = mb_substr($input, 0, $last_space, $charset);

		//add ellipses (...)
		if ($ellipses) {
			$trimmed_text .= '...';
		}

		return $trimmed_text;
	}
}