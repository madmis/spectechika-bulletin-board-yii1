<?php

/**
 * Class TUrl
 * Url helper
 */
class TUrl {
    /**
     * Clean url as site URL
     * Remove schema part and "www" part from the url
     * Remove all path information. Keep only domain information.
     * Example:
     * input url: http://www.example.com/path?param#hash
     * result: example.com
     *
     * @param string $url
     * @return string|null Cleaned URL or null if not domain in url
     */
    public static function extractDomain($url) {
        $pattern = '#^(?:\w+?://)?(?:www\.)?([^/]+).*$#i';
        preg_match($pattern, trim($url), $matches);
        if (isset($matches[1])) {
            return $matches[1];
        }

        return null;
    }
}