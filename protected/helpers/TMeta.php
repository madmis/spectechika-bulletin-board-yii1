<?php

/**
 * Class TMeta
 * Helper to create meta info
 */
class TMeta {

	public static function toRentTitle(ToRent $model) {
		$result = "{$model->fio}, аренда {$model->category->name_r} в ";
		if (!empty($model->city->name)) {
			$result .= $model->city->name_p;
		} else {
			$result .= $model->region->name_p;
		}

		return $result;
	}

	public static function toRentDescription(ToRent $model) {
		$result = "{$model->fio}, аренда {$model->category->name_r} в ";
		if (!empty($model->city->name)) {
			$result .= "{$model->city->name_p} ({$model->region->name}), ";
		} else {
			$result .= "{$model->region->name_p}, ";
		}

		$result .= "по цене {$model->price} грн. в час";

		return $result;
	}

	public static function rentDescription(Rent $model) {
		$result = "{$model->fio}, {$model->title} в ";
		if (!empty($model->city->name)) {
			$result .= "{$model->city->name_p} ({$model->region->name}), ";
		} else {
			$result .= "{$model->region->name_p}, ";
		}

		$result .= "максимальная цена {$model->max_price} грн. в час";

		return $result;
	}

	public static function rentKeywords(Rent $model) {
		$keywords = new CList();
		$base = "возьму в аренду {$model->category->name_v}";
		$keywords->add($base);
		$keywords->add("{$base} {$model->region->name}");

		if (!empty($model->city)) {
			$keywords->add("{$base} {$model->city->name}");
		}

		$keywords->add("{$base} в {$model->region->name_p}");
		if (!empty($model->city)) {
			$keywords->add("{$base} в {$model->city->name_p}");
		}

		$keywords = implode(', ', $keywords->toArray());
		return mb_strtolower($keywords, Yii::app()->charset);
	}

	public static function toRentKeywords(ToRent $model) {
		$keywords = new CList();
		$base = "аренда {$model->category->name_r}";
		$keywords->add($base);
		$keywords->add("{$base} {$model->region->name}");

		if (!empty($model->city)) {
			$keywords->add("{$base} {$model->city->name}");
		}

		$keywords->add("{$base} в {$model->region->name_p}");
		if (!empty($model->city)) {
			$keywords->add("{$base} в {$model->city->name_p}");
		}

		$keywords = implode(', ', $keywords->toArray());
		return mb_strtolower($keywords, Yii::app()->charset);
	}

	public static function toRentRegionTitle(Region $model) {
		return "Аренда спецтехники в {$model->name_p}";
	}

	public static function toRentRegionDescription(Region $model) {
		$result = "Аренда спецтехники в {$model->name_p}";
		$result .= "Аренда самосвала, экскаватора, погрузчика и другой техники  в {$model->name_p}.";
		return $result;
	}

	public static function toRentRegionKeywords(Region $model) {
		$keywords = array(
			"спецтехника $model->name",
			"аренда спецтехники $model->name",
			"аренда спецтехники в $model->name_p",
		);
		$keywords = implode(', ', $keywords);

		return mb_strtolower($keywords, Yii::app()->charset);
	}

	public static function toRentCityTitle(City $model) {
		return "Аренда спецтехники в {$model->name_p}";
	}

	public static function toRentCityDescription(City $model) {
		$result = "Аренда спецтехники в {$model->name_p}";
		$result .= "Аренда самосвала, экскаватора, погрузчика и другой техники  в {$model->name_p}.";
		return $result;
	}

	public static function toRentCityKeywords(City $model) {
		$keywords = array(
			"спецтехника $model->name",
			"аренда спецтехники $model->name",
			"аренда спецтехники в $model->name_p",
		);
		$keywords = implode(', ', $keywords);

		return mb_strtolower($keywords, Yii::app()->charset);
	}


	/**
	 * Prepare text. Replace special variables in text (%APP_NAME%)
	 * @param string $string
	 * @param array $params
	 * @return string
	 */
	public static function prepare($string, array $params = array()) {
		return strtr($string, $params);
	}
}