<?php
/**
 * Swift Mailer wrapper class.
 *
 * @author Sergii 'b3atb0x' hello@webkadabra.com
 */
class SwiftMailer extends CComponent {

	/**
	 * @var string smtp|sendmail|mail
	 */
	public $mailer = 'sendmail'; //
	/**
	 * @var string SMTP outgoing mail server host. Example: smtp.mail.ru
	 */
	public $host;
	/**
	 * @var int Outgoing SMTP server port Example: 587
	 */
	public $port = 25;
	/**
	 * @var string SMTP Relay account username
	 */
	public $username;
	/**
	 * @var string SMTP Relay account password
	 */
	public $password;
	/**
	 * @var string ssl|tls (SMTP security)
	 */
	public $security;
	/**
	 * Email address messages are going to be sent "from"
	 * email | email => name
	 * @param string|array
	 */
	public $from;
	/**
	 * @var string sendmail command
	 */
	public $sendmailCommand = '/usr/bin/sendmail -t';

	/**
	 * @var Swift_Message
	 */
	private $__message;

	const BODY_TYPE_HTML = 'text/html';
	const BODY_TYPE_TEXT = 'text/plain';

	public function init() {
		if (!class_exists('Swift', false)) {
			$this->_registerAutoloader();
			// include the SwiftMailer Dependencies
			require_once dirname(__FILE__) . '/lib/dependency_maps/cache_deps.php';
			require_once dirname(__FILE__) . '/lib/dependency_maps/mime_deps.php';
			require_once dirname(__FILE__) . '/lib/dependency_maps/message_deps.php';
			require_once dirname(__FILE__) . '/lib/dependency_maps/transport_deps.php';
			//Load in global library preferences
			require_once dirname(__FILE__) . '/lib/preferences.php';
		}
		$this->__message = Swift_Message::newInstance();
	}

	protected function _registerAutoloader() {
		Yii::import('ext.swiftMailer.lib.classes.*');
		require_once(dirname(__FILE__) . '/lib/classes/Swift.php');
		Swift::registerAutoLoad();
		// Register SwiftMailer's autoloader before Yii for correct class loading.
		$autoLoad = array('Swift', 'autoload');
		spl_autoload_unregister($autoLoad);
		Yii::registerAutoloader($autoLoad);
	}

	/**
	 * Set the subject of this message.
	 * @param string $subject
	 * @return SwiftMailer
	 */
	public function setSubject($subject) {
		$this->__message->setSubject($subject);
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSubject() {
		return $this->__message->getSubject();
	}

	/**
	 * Set the from address of this message.
	 * You may pass an array of addresses if this message is from multiple people.
	 * If $name is passed and the first parameter is a string, this name will be
	 * associated with the address.
	 * 'some@address.tld', array('some@address.tld' => 'The Name'),
	 * @param mixed $addresses
	 * @param string $name
	 * @return SwiftMailer
	 */
	public function setFrom($addresses, $name = null) {
		$this->__message->setFrom($addresses, $name);
		return $this;
	}

	/**
	 * Set the to addresses of this message.
	 * If multiple recipients will receive the message and array should be used.
	 * If $name is passed and the first parameter is a string, this name will be
	 * associated with the address.
	 * 'some@address.tld', array('some@address.tld' => 'The Name'),
	 * @param mixed $addresses
	 * @param string $name
	 * @return SwiftMailer
	 */
	public function setTo($addresses, $name = null) {
		$this->__message->setTo($addresses, $name);
		return $this;
	}

	/**
	 * Set the reply-to address of this message.
	 * You may pass an array of addresses if replies will go to multiple people.
	 * If $name is passed and the first parameter is a string, this name will be
	 * associated with the address.
	 * @param string $addresses
	 * @param string $name optional
	 * @return SwiftMailer
	 */
	public function setReplyTo($addresses, $name = null) {
		$this->__message->setReplyTo($addresses, $name);
		return $this;
	}

	/**
	 * Set the body of this entity
	 * @param $body
	 * @param string $contentType
	 * @param string $charset
	 * @return $this
	 */
	public function setBody($body, $contentType = self::BODY_TYPE_HTML, $charset = null) {
		$this->__message->setBody($body, $contentType, $charset);
		return $this;
	}

	/**
	 * Add a MimePart to this Message.
	 *
	 * @param string $body
	 * @param string $contentType
	 * @param string $charset
	 * @return $this
	 */
	public function addPart($body, $contentType = self::BODY_TYPE_TEXT, $charset = null) {
		$this->__message->addPart($body, $contentType, $charset);
		return $this;
	}

	/**
	 * @param string $pathToFile
	 * @return $this
	 */
	public function attach($pathToFile) {
		$this->__message->attach(Swift_Attachment::fromPath($pathToFile));
		return $this;
	}

	/**
	 * Add a To: address to this message.
	 * If $name is passed this name will be associated with the address.
	 * @param string $address
	 * @param string $name
	 * @return $this
	 */
	public function addTo($address, $name = null) {
		$this->__message->addTo($address, $name);
		return $this;
	}

	/**
	 * Add a From: address to this message.
	 * If $name is passed this name will be associated with the address.
	 * @param string $address
	 * @param string $name
	 * @return $this
	 */
	public function addFrom($address, $name = null) {
		$this->__message->addFrom($address, $name);
		return $this;
	}

	/**
	 * Set the Cc addresses of this message.
	 * If $name is passed and the first parameter is a string, this name will be
	 * associated with the address.
	 * @param mixed $addresses
	 * @param string $name
	 * @return $this
	 */
	public function setCc($addresses, $name = null) {
		$this->__message->setCc($addresses, $name);
		return $this;
	}

	/**
	 * Set the Bcc addresses of this message.
	 * If $name is passed and the first parameter is a string, this name will be
	 * associated with the address.
	 * @param mixed $addresses
	 * @param string $name
	 * @return $this
	 */
	public function setBcc($addresses, $name = null) {
		$this->__message->setBcc($addresses, $name);
		return $this;
	}

	/**
	 * Set the return-path (the bounce address) of this message.
	 * @param string $address
	 * @return $this
	 */
	public function setReturnPath($address) {
		$this->__message->setReturnPath($address);
		return $this;
	}

	/**
	 * Ask for a delivery receipt from the recipient to be sent to $addresses
	 * @param array $addresses
	 * @return $this
	 */
	public function setReadReceiptTo($addresses) {
		$this->__message->setReadReceiptTo($addresses);
		return $this;
	}

	/**
	 * Set the maximum line length of lines in this body.
	 * Though not enforced by the library, lines should not exceed 1000 chars.
	 * @param int $length
	 * @return $this
	 */
	public function setMaxLineLength($length) {
		$this->__message->setMaxLineLength($length);
		return $this;
	}

	/**
	 * Set the priority of this message.
	 * The value is an integer where 1 is the highest priority and 5 is the lowest.
	 * 1: 'Highest', 2: 'High', 3: 'Normal', 4: 'Low', 5: 'Lowest'
	 * @param int $priority
	 * @return $this
	 */
	public function setPriority($priority) {
		$this->__message->setPriority($priority);
		return $this;
	}

	/**
	 * @return boolean whether email has been sent or not
	 */
	public function send() {
		if (!$this->__message->getFrom()) {
			$this->__message->setFrom($this->from);
		}

		$transport = $this->_loadTransport();
		//Create the Mailer using your created Transport
		$mailer = Swift_Mailer::newInstance($transport);
		return $mailer->send($this->__message);
	}

	/**
	 * @return Swift_MailTransport|Swift_SendmailTransport|Swift_SmtpTransport
	 */
	protected function _loadTransport() {
		if ($this->mailer == 'smtp') {
			$transport = Swift_SmtpTransport::newInstance($this->host, $this->port, $this->security);
			$transport->setUsername($this->username);
			$transport->setPassword($this->password);
		} elseif ($this->mailer == 'sendmail') {
			$transport = Swift_SendmailTransport::newInstance($this->sendmailCommand);
		} else /*($this->mailer == 'mail')*/ {
			$transport = Swift_MailTransport::newInstance();
		}

		return $transport;
	}
}
