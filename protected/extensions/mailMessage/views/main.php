<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

	<style>
		html { font-size: 100%; }
		img { height: auto; max-width: 100%; vertical-align: middle; border: 0; }
		body { margin: 0; font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #333333; background-color: #ffffff; }
		a { color: #0088cc; text-decoration: none; }
		a:hover { color: #005580; text-decoration: underline; }
		small { font-size: 85%; }
		strong { font-weight: bold; }
		em { font-style: italic; }
		cite { font-style: normal; }
		p { margin: 0; padding: 0; }
		.navbar { color: white; }
		.navbar .navbar-inner { border: 0; padding-right: 0; padding-left: 0; width: 100%; min-height: 40px; }
		.navbar .navbar-inner.top { background-color: #f7782d; border-color: #d4d4d4; }
		.navbar .navbar-inner.bottom { background-color: #1b1b1b; border-color: #252525; }
		.container-fluid { padding-right: 20px; padding-left: 20px; *zoom: 1; }
		.container-fluid:before, .container-fluid:after { display: table; line-height: 0; content: ""; }
		.container-fluid:after { clear: both; }
		.navbar .brand { display: block; float: left; padding: 10px 20px 10px; margin-left: -20px; font-size: 20px; font-weight: 200; color: #fff; }
		.navbar .brand:hover { text-decoration: none; }
		.container { width: 944px; margin: 0 auto; }
		.content { min-height: 200px; padding: 20px; }
	</style>
</head>
<body>
	<div class="container">
		<div class="navbar">
			<div class="navbar-inner top">
				<div class="container-fluid">
					<a class="brand" href="<?php echo Yii::app()->urlManager->baseUrl ?>/"><?php echo CHtml::encode(Yii::app()->name); ?></a>
				</div>
			</div>
		</div>
		<div class="content small"><?php echo $content ?></div>
		<div class="navbar">
			<div class="navbar-inner bottom">
			</div>
		</div>
	</div>
</body>
</html>