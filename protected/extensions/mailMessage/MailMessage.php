<?php

class MailMessage extends CApplicationComponent {

	/**
	 * Path to messages view
	 * @var string
	 */
	public $viewPath = 'ext.mailMessage.views';
	/**
	 * Path to main layout mailMessage view
	 * @var string
	 */
	public $htmlLayout = 'ext.mailMessage.views.html.layout';
	public $textLayout = 'ext.mailMessage.views.text.layout';

	public function init() {
//		$view = Yii::app()->controller->renderPartial($this->layout, array('content'=>''), TRUE);
//		$basePath = Yii::getPathOfAlias($this->layout);
	}

	/**
	 * Get mail message
	 * @param string $view view name
	 * @param array $params
	 * @param bool $isHtml html or text
	 * @return string
	 */
	public function getMessage($view, array $params = array(), $isHtml = true) {
		$view = $isHtml ? $this->_getHtmlView($view) : $this->_getTextView($view);
		$layout = $isHtml ? $this->htmlLayout : $this->textLayout;
		$title = isset($params['title']) ? $params['title'] : CHtml::encode(Yii::app()->name);
		$content = Yii::app()->controller->renderPartial($view, $params, true);

		return Yii::app()->controller->renderPartial($layout, array(
			'content'=>$content,
			'title' => $title,
		), true);
	}

	/**
	 * @param string $viewName
	 * @return string
	 */
	protected function _getHtmlView($viewName) {
		return $this->viewPath . '.html.' . $viewName;
	}

	/**
	 * @param string $viewName
	 * @return string
	 */
	protected function _getTextView($viewName) {
		return $this->viewPath . '.text.' . $viewName;
	}
}
