<?php

interface TPhotoHandlerInterface {

	public function getBasePhotoUrl();

	public function getBasePhotoAlias();

	public function getPhotoSmallWidth();

	public function getPhotoSmallHeight();

	public function getPhotoMiniWidth();

	public function getPhotoMiniHeight();

	public function getPhotoMediumWidth();

	public function getPhotoMediumHeight();

	public function getPhotoSmallPrefix();

	public function getPhotoMiniPrefix();

	public function getPhotoMediumPrefix();
}