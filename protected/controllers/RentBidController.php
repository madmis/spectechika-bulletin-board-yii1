<?php

class RentBidController extends TController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			array('auth.filters.AuthFilter'),
		);
	}

	public function actionAdd()
	{
		$model = new RentBid();
		$this->performAjaxValidation($model);

		if (Yii::app()->request->getPost('RentBid')) {
			if (Yii::app()->user->isGuest) {
				throw new CHttpException(401, 'Authentication required');
			}

			$post = Yii::app()->request->getPost('RentBid');
			$model->attributes = $post;
			$model->user_id = Yii::app()->user->getId();

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Заявка успешно добавлена!');
				$this->redirect(Yii::app()->request->getUrlReferrer());
			} else {
				throw new CHttpException(500, 'Error to save bid');
			}
		}

		$this->redirect('/');
	}

	protected function performAjaxValidation($model)
	{
		if (Yii::app()->request->getPost('ajax') === 'rent-bid-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}