<?php
/**
 * Class SitemapController
 */
class SitemapController extends TController
{

	const ALWAYS = 'always';
	const HOURLY = 'hourly';
	const DAILY = 'daily';
	const WEEKLY = 'weekly';
	const MONTHLY = 'monthly';
	const YEARLY = 'yearly';
	const NEVER = 'never';

	public function actionIndex()
	{
		if (!$xml = Yii::app()->cache->get('sitemap')) {
			$items = new CList();
			$items->add($this->createAbsoluteUrl('/about'));
			$items->add($this->createAbsoluteUrl('/faq'));
			$items->add($this->createAbsoluteUrl('/toRent'));
			$items->add($this->createAbsoluteUrl('/rent'));
			$items->add($this->createAbsoluteUrl('/news'));
			$items->add($this->createAbsoluteUrl('/article'));

			$items = $this->__addNews($items);
			$items = $this->__addArticles($items);

			$items = $this->__addRentCategory($items);
			$items = $this->__addRentRegion($items);
			$items = $this->__addRent($items);

			$items = $this->__addToRentCategory($items);
			$items = $this->__addToRentRegion($items);
			$items = $this->__addToRent($items);

			$xml = $this->renderPartial('index', array('items' => $items), true);
			$xml = serialize($xml);
			// 2 hours cache
			Yii::app()->cache->set('sitemap', $xml, 3600 * 2);
		}

		header('Content-type: text/xml');
		echo unserialize($xml);
		Yii::app()->end();
	}

	private function __addNews(CList $items)
	{
		/** @var News[] $rows */
		$rows = News::model()->findAll();
		foreach ($rows as $row) {
			$items->add(
				Yii::app()->createAbsoluteUrl("/news/{$row->translit}")
			);
		}
		return $items;
	}

	private function __addArticles(CList $items)
	{
		/** @var Article[] $rows */
		$rows = Article::model()->findAll();
		foreach ($rows as $row) {
			$items->add(
				Yii::app()->createAbsoluteUrl("/article/{$row->translit}")
			);
		}
		return $items;
	}

	private function __addRentCategory(CList $items)
	{
		/** @var Category[] $rows */
		$rows = Category::model()->findAll();
		foreach ($rows as $row) {
			$items->add(
				Yii::app()->createAbsoluteUrl("/rent/category/{$row->translit}")
			);
		}
		return $items;
	}

	private function __addRentRegion(CList $items)
	{
		/** @var Region[] $rows */
		$rows = Region::model()->findAll();
		foreach ($rows as $row) {
			$items->add(
				Yii::app()->createAbsoluteUrl("/rent/region/{$row->translit}")
			);
		}
		return $items;
	}

	private function __addRent(CList $items)
	{
		$criteria = new CDbCriteria();
		$criteria->limit = Yii::app()->params->itemAt('sitemapItemsCount');
		$criteria->order = 'create_at DESC';
		/** @var Rent[] $rows */
		$rows = Rent::model()->findAll($criteria);
		foreach ($rows as $row) {
			$items->add(
				Yii::app()->createAbsoluteUrl("/rent/{$row->id}")
			);
		}
		return $items;
	}

	private function __addToRentCategory(CList $items)
	{
		/** @var Category[] $rows */
		$rows = Category::model()->findAll();
		foreach ($rows as $row) {
			$items->add(
				Yii::app()->createAbsoluteUrl("/toRent/category/{$row->translit}")
			);
		}
		return $items;
	}

	private function __addToRentRegion(CList $items)
	{
		/** @var Region[] $rows */
		$rows = Region::model()->findAll();
		foreach ($rows as $row) {
			$items->add(
				Yii::app()->createAbsoluteUrl("/toRent/region/{$row->translit}")
			);
		}
		return $items;
	}

	private function __addToRent(CList $items)
	{
		$criteria = new CDbCriteria();
		$criteria->limit = Yii::app()->params->itemAt('sitemapItemsCount');
		$criteria->order = 'create_at DESC';
		/** @var ToRent[] $rows */
		$rows = ToRent::model()->findAll($criteria);
		foreach ($rows as $row) {
			$items->add(
				Yii::app()->createAbsoluteUrl("/toRent/{$row->id}")
			);
		}
		return $items;
	}


}