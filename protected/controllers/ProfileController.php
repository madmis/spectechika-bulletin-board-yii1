<?php

/**
 * Class ProfileController
 */
class ProfileController extends TController {

	public $defaultAction = 'social';

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			array('auth.filters.AuthFilter'),
		);
	}

	public function actionSocial() {
		$model = User::model()->findByPk(Yii::app()->user->id);
		$param = array(
			'userServices' => UserService::model()->getUserServices($model->id),
			'model' => $model,
		);
		$this->render('social-tab', $param);
	}

	public function actionSettings() {
		$c = new CDbCriteria();
		$c->with = array('city');
		$model = User::model()->findByPk(Yii::app()->user->id, $c);
		$param = array(
			'model' => $model,
		);
		$this->render('settings-tab', $param);

	}

	/**
	 * Добавить соеть к профилю
	 */
	public function actionAddService() {
		$service = Yii::app()->request->getQuery('service');
		if (isset($service)) {
			$authIdentity = Yii::app()->eauth->getIdentity($service);
			if ($authIdentity->authenticate()) {
				if (!UserService::model()->getByIdentity($authIdentity->getId(), $authIdentity->getServiceName())) {
					$userService = new UserService();
					$userService->user_id = Yii::app()->user->id;
					$userService->service = $authIdentity->getServiceName();
					$userService->identity = $authIdentity->getId();
					if (!$userService->save()) {
						Yii::app()->user->setFlash('error', Yii::t('main', 'Не удалось добавить сервис в БД!'));
					} else {
						Yii::app()->user->setFlash('success', Yii::t('main', 'Сеть добавлена!'));
					}
				} else {
					Yii::app()->user->setFlash('error', Yii::t('main', 'Сервис с такми данными уже привязан к другому профилю!'));
				}
			} else {
				Yii::app()->user->setFlash('error', Yii::t('main', 'Не удалось пройти авторизацию в сервисе!'));
			}
			Yii::app()->request->redirect(Yii::app()->createUrl('profile/settings'));
		}
	}

	/**
	 * Удалить сеть из профиля
	 */
	public function actionRemoveService() {
		$service = Yii::app()->request->getQuery('service');
		if (isset($service)) {
			UserService::model()->removeUserService($service);
		}
		Yii::app()->request->redirect(Yii::app()->createUrl('profile/settings'));
	}

	/**
	 * Обновить данные профиля данными из сервиса
	 */
	public function actionUpdateService() {
		$service = Yii::app()->request->getQuery('service');
		if (isset($service)) {
			$authIdentity = Yii::app()->eauth->getIdentity($service);
			if ($authIdentity->authenticate()) {
				$authIdentity->fetchAttributes();
				if ($authIdentity->getAttribute('name') != NULL) {
					$user = User::model()->findByPk(Yii::app()->user->id);
					$user->username = $authIdentity->getAttribute('name');
					Yii::app()->user->setState('name', $authIdentity->getAttribute('name'));
					if ($user->save()) {
						Yii::app()->user->setFlash('success', Yii::t('main', 'Данные профиля обновлены!'));
					} else {
						Yii::app()->user->setFlash('error', Yii::t('main', 'Не удалось обновить данные профиля!'));
					}
				}
			} else {
				Yii::app()->user->setFlash('error', Yii::t('main', 'Не удалось пройти авторизацию в сервисе!'));
			}
		}
		Yii::app()->request->redirect(Yii::app()->createUrl('profile/settings'));
	}

	public function actionSaveProfile() {
		/** @var User $model */
		$model = User::model()->findByPk(Yii::app()->user->id);
		$model->scenario = 'update';
		$this->_performAjaxValidation($model);
		$user = Yii::app()->request->getPost('User');

		if ($user) {
			$model->attributes = $user;

			if (!empty($model->city_id)) {
				$model->city_id = City::model()->getIdByName($model->city_id);
			}

			if($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('main', 'Данные профиля изменены!'));
			} else {
				if ($model->hasErrors()) {
					Yii::app()->user->setFlash('error', array_shift($model->errors));
				}
			}
		}

		$this->redirect(Yii::app()->createUrl('profile/settings'));
	}

	protected function _performAjaxValidation($model) {
		$ajax = Yii::app()->request->getPost('ajax');
		if($ajax === 'profile-settings-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}