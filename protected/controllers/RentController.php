<?php
/**
 * Class RentController
 */
class RentController extends TController {

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			array('auth.filters.AuthFilter + add, setStatus'),
		);
	}

	public function actionIndex() {
		$provider = Rent::model()->getDataProvider();

		$this->render('index', array(
			'provider' => $provider,
		));
	}

	/**
	 * @param string $category
	 * @throws CHttpException
	 */
	public function actionCategory($category) {
		$Category = Category::model()
			->findByAttributes(array('translit' => $category));
		if ($Category === null) {
			throw new CHttpException(404, Yii::t('error', 'The requested page does not exist.'));
		}
		$this->searchCategory = $Category;
		$provider = Rent::model()->byCategoryProvider($Category);

		$this->render('category', array(
			'provider' => $provider,
			'category' => $Category,
		));
	}

	/**
	 * @param string $region
	 * @throws CHttpException
	 */
	public function actionRegion($region) {
		$Region = Region::model()
			->findByAttributes(array('translit' => $region));
		if ($Region === null) {
			throw new CHttpException(404, Yii::t('error', 'The requested page does not exist.'));
		}
		$this->searchRegion = $Region;
		$provider = Rent::model()->byRegionProvider($Region);

		$this->render('region', array(
			'provider' => $provider,
			'region' => $Region,
		));
	}

	/**
	 * @param int $city
	 * @throws CHttpException
	 */
	public function actionCity($city) {
		$City = City::model()->findByPk($city);
		if ($City === null) {
			throw new CHttpException(404, Yii::t('error', 'The requested page does not exist.'));
		}
		$provider = Rent::model()->byCityProvider($City);

		$this->render('city', array(
			'provider' => $provider,
			'city' => $City,
		));
	}

	/**
	 * @throws CHttpException
	 */
	public function actionSearch() {
		$Category = $Region = null;
		$category = (int)Yii::app()->request->getQuery('category');
		$region = (int)Yii::app()->request->getQuery('region');

		if ($category) {
			$Category = Category::model()->findByPk($category);
			if ($Category === null) {
				throw new CHttpException(404, Yii::t('error', 'The requested page does not exist.'));
			}
			$this->searchCategory = $Category;
		}

		if ($region) {
			$Region = Region::model()->findByPk($region);
			if ($Region === null) {
				throw new CHttpException(404, Yii::t('error', 'The requested page does not exist.'));
			}
			$this->searchRegion = $Region;
		}

		$provider = Rent::model()->baseDataProvider($Category, $Region);

		$this->render('search', array(
			'provider' => $provider,
			'category' => $Category,
			'region' => $Region,
		));

	}

	/**
	 * @param int $id
	 */
	public function actionView($id) {
		$model = Rent::model()->findByPk($id);

		$this->render('view', array(
			'model' => $model,
		));
	}

	public function actionSetStatus($id) {
		if (Yii::app()->user->isGuest) {
			throw new CHttpException(401, 'Authentication required');
		}

		/** @var Rent $Rent */
		$Rent = Rent::model()->findByPk($id);
		if (!$Rent) {
			throw new CHttpException(404, 'Page not found');
		}

		if (Yii::app()->user->id !== $Rent->user_id) {
			throw new CHttpException(404, 'Page not found');
		}

		$status = (int)Yii::app()->request->getQuery('status');
		if (!in_array($status, array(0, 1, 2))) {
			throw new CHttpException(404, 'Invalid status');
		}

		$Rent->status = $status;

		if ($status === 1) {
			$performer = (int)Yii::app()->request->getQuery('performer');
			/** @var User $User */
			$User = User::model()->findByPk($performer);

			if (!$User) {
				throw new CHttpException(400, 'Bad request');
			}

			$Rent->performer_id = $User->id;
		}

		if (!$Rent->save()) {
			throw new CHttpException(500, 'Не удалось изменить статус');
		}

		$this->redirect(Yii::app()->request->getUrlReferrer());
	}

	public function actionAdd() {
		$User = Yii::app()->user;
		$model = new Rent();

		if (Yii::app()->request->getPost('Rent')) {
			$post = Yii::app()->request->getPost('Rent');
			if (!empty($post['city_id'])) {
				$post['city_id'] = City::model()->getIdByName($post['city_id']);
				if (!$post['city_id']) {
					$model->addError('city_id', 'Город указан неверно! Выберите город из списка.');
				}
			}

			$model->attributes = $post;

			if (!$model->hasErrors()) {
				$model->user_id = Yii::app()->user->id;

				if (!empty($model->city_id)) {
					if (!City::model()->getIdByName($model->city_id)) {
						$model->addError('city_id', 'Город указан неверно! Выберите город из списка.');
					}
				}

				if ($model->save()) {
					$this->redirect(array('index'));
				}
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}
}