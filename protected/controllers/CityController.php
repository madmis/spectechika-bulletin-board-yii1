<?php

class CityController extends TController {

	public function actionClientCitiesOld($query) {
		if (!empty($query)) {
			$parent = (int) Yii::app()->request->getQuery('parent');

			$criteria = new CDbCriteria();
			if ($parent) {
				$criteria->addCondition('region_id = :region_id');
				$criteria->params = array(':region_id' => $parent);
			}

			$criteria->addSearchCondition('name', $query);
			$criteria->limit = 20;
			$criteria->order = 'name ASC';
			$items = City::model()->findAll($criteria);
			$result = new CList();
			foreach ($items as $item) {
				/** @var $item City */
				$result->add(array(
					'value' => $item->name
				));
			}
			$result = $result->toArray();
		} else {
			$result = array();
		}

		echo CJSON::encode($result);
		Yii::app()->end();
	}

	public function actionClientCities() {
		$query = Yii::app()->request->getQuery('q');
		$parent = (int) Yii::app()->request->getQuery('p');

		if (!empty($query)) {
			$criteria = new CDbCriteria();
			if ($parent) {
				$criteria->addCondition('region_id = :region_id');
				$criteria->params = array(':region_id' => $parent);
			}
			$criteria->addSearchCondition('name', $query);
			$criteria->limit = 20;

			$items = City::model()->findAll($criteria);

			$result = array();
			foreach ($items as $item) {
				/** @var $item City */
				$result[] = array(
					'id' => $item->id,
					'text' => $item->name,
				);
			}
		} else {
			$result = array();
		}

		echo CJSON::encode($result);
		Yii::app()->end();
	}

	public function actionGetRegion() {
		$id = (int)Yii::app()->request->getQuery('id');
		$result = array('id' => 0);

		if ($id) {
			/** @var City $item */
			$item = City::model()->findByPk($id);
			if ($item) {
				$result['id'] = $item->region_id;
			}
		}

		$this->renderJSON($result);
	}
}