<?php

/**
 * Class ArticleController
 */
class ArticleController extends TController
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			array('auth.filters.AuthFilter + Create'),
		);
	}

	public function actionIndex()
	{
		$this->render('index', array(
			'provider' => Article::model()->dataProvider()
		));

	}

	/**
	 * @param $translit
	 * @throws CHttpException
	 */
	public function actionView($translit)
	{
		$model = Article::model()->findByAttributes(array('translit' => $translit));

		if (!$model) {
			throw new CHttpException(404, 'Page not found');
		}

		$this->render('view', array('model' => $model));
	}

	public function actionCreate()
	{
		$model = new Article();

		if (Yii::app()->request->getPost('Article')) {
			$model->attributes = Yii::app()->request->getPost('Article');
			if (!empty($model->create_at)) {
				$model->create_at = Yii::app()->format->formatDbDate($model->create_at);
			} else {
				$model->create_at = Yii::app()->format->formatDbDateTime('now');
			}
			$model->user_id = Yii::app()->user->id;

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Статья успешно добавлена');
				$this->redirect('/article');
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}
}