<?php
/**
 * Class AuthenticateController
 */
class AuthenticateController extends TController {

	public function actionLogin() {
		if (!Yii::app()->user->isGuest) {
			$this->redirect('/');
		}

		$LoginForm = new LoginForm;
		$service = Yii::app()->request->getQuery('service');
		$loginForm = Yii::app()->request->getPost('LoginForm');

		if (!empty($service)) {
			$this->_loginByService($service);
		} else if (!empty($loginForm)) {
			$this->_loginByEmail($LoginForm, $loginForm);
		}

		$this->render('login', array('model' => $LoginForm));
	}

	/**
	 * @param string $service service name
	 */
	protected function _loginByService($service) {
		$authIdentity = Yii::app()->eauth->getIdentity($service);
		$authIdentity->redirectUrl = Yii::app()->user->getReturnUrl(
			Yii::app()->getRequest()->getUrlReferrer()
		);
		$authIdentity->cancelUrl = $this->createAbsoluteUrl(Yii::app()->user->getLoginUrl());

		if ($authIdentity->authenticate()) {
			$identity = new TServiceIdentity($authIdentity);

			// successful authentication
			if ($identity->authenticate()) {
				Yii::app()->user->login($identity);
				// special redirect with closing popup window
				$authIdentity->redirect();
			} else {
				// close popup window and redirect to cancelUrl
				$authIdentity->cancel();
			}
		}

		// Something went wrong, redirect to login page
		$this->redirect(array(Yii::app()->user->getLoginUrl()));
	}

	/**
	 * @param LoginForm $LoginForm
	 * @param array $loginForm
	 */
	protected function _loginByEmail(LoginForm &$LoginForm, array $loginForm) {
		$LoginForm->attributes = $loginForm;
		$isLogin = Yii::app()->request->getPost('login');

		if ($isLogin) {
			if ($LoginForm->validate() && $LoginForm->login()) {
				$this->redirect(Yii::app()->user->returnUrl);
			}
		} else {
			$User = new User();
			$User->scenario = $isLogin ? 'login' : 'register';
			$User->username = $LoginForm->email;
			$User->email = $LoginForm->email;
			$User->password = $LoginForm->password;
			if ($User->save()) {
				// Добавляем запись в user_service
				$UserService = new UserService();
				$UserService->user_id = $User->id;
				$UserService->service = 'email';
				$UserService->identity = $User->id;
				$UserService->save();

				Yii::app()->authManager->assign(TWebApplication::DEFAULT_ROLE, $User->id);

				if ($LoginForm->login()) {
					$message = 'Рекоммендация: мы рекомендуем привязать к своему аккаунту один из профилей социальных сетей. '
						. 'В этом случае, даже если вы забудете свой пароль, вы всегда сможете авторизоваться через социальную сеть и восстановить пароль.';
					Yii::app()->user->setFlash('info', $message);
					$this->redirect(Yii::app()->user->returnUrl);
				}
			} else {
				$LoginForm->addErrors($User->errors);
			}
		}
	}

	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
