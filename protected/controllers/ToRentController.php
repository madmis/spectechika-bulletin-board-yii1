<?php

/**
 * Class ToRentController
 * Сдают в аренду
 */
class ToRentController extends TController {

	public function actionIndex() {
		$provider = ToRent::model()->getDataProvider();

		$this->render('index', array(
			'provider' => $provider,
		));
	}

	/**
	 * @param string $category
	 * @throws CHttpException
	 */
	public function actionCategory($category) {
		$Category = Category::model()
			->findByAttributes(array('translit' => $category));
		if ($Category === null) {
			throw new CHttpException(404, Yii::t('error', 'The requested page does not exist.'));
		}
		$this->searchCategory = $Category;
		$provider = ToRent::model()->byCategoryProvider($Category);

		$this->render('category', array(
			'provider' => $provider,
			'category' => $Category,
		));
	}

	/**
	 * @param string $region
	 * @throws CHttpException
	 */
	public function actionRegion($region) {
		$Region = Region::model()
			->findByAttributes(array('translit' => $region));
		if ($Region === null) {
			throw new CHttpException(404, Yii::t('error', 'The requested page does not exist.'));
		}
		$this->searchRegion = $Region;
		$provider = ToRent::model()->byRegionProvider($Region);

		$this->render('region', array(
			'provider' => $provider,
			'region' => $Region,
		));
	}

	/**
	 * @param int $city
	 * @throws CHttpException
	 */
	public function actionCity($city) {
		$City = City::model()->findByPk($city);
		if ($City === null) {
			throw new CHttpException(404, Yii::t('error', 'The requested page does not exist.'));
		}
		$provider = ToRent::model()->byCityProvider($City);

		$this->render('city', array(
			'provider' => $provider,
			'city' => $City,
		));
	}

	/**
	 * @throws CHttpException
	 */
	public function actionSearch() {
		$Category = $Region = null;
		$category = (int)Yii::app()->request->getQuery('category');
		$region = (int)Yii::app()->request->getQuery('region');

		if ($category) {
			$Category = Category::model()->findByPk($category);
			if ($Category === null) {
				throw new CHttpException(404, Yii::t('error', 'The requested page does not exist.'));
			}
			$this->searchCategory = $Category;
		}

		if ($region) {
			$Region = Region::model()->findByPk($region);
			if ($Region === null) {
				throw new CHttpException(404, Yii::t('error', 'The requested page does not exist.'));
			}
			$this->searchRegion = $Region;
		}

		$provider = ToRent::model()->baseDataProvider($Category, $Region);

		$this->render('search', array(
			'provider' => $provider,
			'category' => $Category,
			'region' => $Region,
		));

	}

	/**
	 * @param int $id
	 */
	public function actionView($id) {
		$model = ToRent::model()->findByPk($id);

		$this->render('view', array(
			'model' => $model,
		));
	}

	public function actionAdd() {
		$User = Yii::app()->user;
		$model = new ToRent();

		$buseId = Yii::app()->request->getQuery('b');
		$Buse = null;
		if (!empty($buseId) && !$User->getIsGuest()) {
			/** @var Buses $Buse */
			$Buse = Buses::model()->findByPk($buseId);
		}

		if (Yii::app()->request->getPost('ToRent')) {
			$post = Yii::app()->request->getPost('ToRent');
			if (!empty($post['city_id'])) {
				$post['city_id'] = City::model()->getIdByName($post['city_id']);
				if (!$post['city_id']) {
					$model->addError('city_id', 'Город указан неверно! Выберите город из списка.');
				}
			}

			if (!$model->hasErrors()) {
				$model->attributes = $post;
				// check is select file
				$photo = CUploadedFile::getInstance($model, 'photo');
				if ($photo) {
					$model->uploadPhoto();
				} else if (!empty($Buse) && $Buse->photo) {
					// check buses selected photo
					$model->uploadBusesPhoto($Buse);
				}
				$model->user_id = Yii::app()->user->id;

				if (!empty($model->city_id)) {
					if (!City::model()->getIdByName($model->city_id)) {
						$model->addError('city_id', 'Город указан неверно! Выберите город из списка.');
					}
				}

				if ($model->save()) {
					$this->redirect(array('index'));
				}
			}
		}

		if (!$User->getIsGuest()) {
			/** @var User $user */
			$user = User::model()->findByPk(Yii::app()->user->id);

			if ($user) {
				$model->phone = $user->phone;
				$model->company_name = $user->company_name;
				$model->fio = $user->username;
				$model->region_id = $user->region_id;
				$model->city_id = $user->city_id;
			}

			if (!empty($Buse)) {
				$model->category_id = $Buse->category_id;
				$model->price = $Buse->price;
				$model->features = $Buse->features;
				$model->description = $Buse->description;
			}

			$buses = Buses::model()->findAllByAttributes(array(
				'user_id' => $User->id
			));
		}

		$this->render('create', array(
			'model' => $model,
			'buses' => !empty($buses) ? $buses : null,
			'buse' => !empty($Buse) ? $Buse : null,
		));
	}
}