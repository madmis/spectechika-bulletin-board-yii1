<?php

class SiteController extends TController {

	public function actions() {
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	public function actionIndex() {
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				Yii::setPathOfAlias('sys', Yii::app()->theme->systemViewPath);
				$view = 'sys.error' . $error['code'];
				$error = new CAttributeCollection($error);
				if ($this->getViewFile($view)) {
					$this->render($view, array('error' => $error));
				} else {
					$this->render('sys.error', array('error' => $error));
				}
			}
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact() {
		$model = new ContactForm;
		if (Yii::app()->request->getPost('ContactForm')) {
			$model->attributes = Yii::app()->request->getPost('ContactForm');
			if ($model->validate()) {
				$mailer = Yii::app()->mailer;
				$mailer->setReplyTo(array($model->email => $model->name))
					->setTo(Yii::app()->params->itemAt('adminEmail'))
					->setSubject("Обратная связь - {$model->subject}")
					->setBody($model->body, SwiftMailer::BODY_TYPE_TEXT);
				if ($mailer->send()) {
					Yii::app()->user->setFlash('success', 'Спасибо что написали нам. Мы постараемся ответить вам как можно скорее.');
					$this->refresh();
				} else {
					Yii::app()->user->setFlash('error', 'При отправке сообщения произошла ошибка. Пожалуйста, попробуйте отправить сообщение еще раз.');
				}
			}
		}
		$this->render('contact', array('model' => $model));
	}
}