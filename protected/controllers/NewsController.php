<?php

/**
 * Class NewsController
 */
class NewsController extends TController
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			array('auth.filters.AuthFilter + Create'),
		);
	}

	public function actionIndex()
	{
		$this->render('index', array(
			'provider' => News::model()->dataProvider()
		));
	}

	public function actionCreate()
	{
		$model = new News;

		if (Yii::app()->request->getPost('News')) {
			$model->attributes = Yii::app()->request->getPost('News');
			if (!empty($model->create_at)) {
				$model->create_at = Yii::app()->format->formatDbDate($model->create_at);
			} else {
				$model->create_at = Yii::app()->format->formatDbDateTime('now');
			}
			$model->user_id = Yii::app()->user->id;

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Новость успешно добавлена');
				$this->redirect('/news');
			}
		}
		$this->render('create', array('model' => $model));
	}

	public function actionView($translit)
	{
		$model = News::model()->findByAttributes(array('translit' => $translit));

		if (!$model) {
			throw new CHttpException(404, 'Page not found');
		}

		$this->render('view', array('model' => $model));
	}
}