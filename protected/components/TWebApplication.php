<?php
/**
 * В данном классе не должно быть никаких рабочих методов.
 * Класс предназначен только для удобства работы в IDE PhpStrom
 *
 * @property TWebUser $user The user session information.
 * @property EAuth $eauth eauth module
 * @property SwiftMailer $mailer mail module
 * @property MailMessage $mailMessage extension
 * @property TFormatter $format
 * @property Curl $curl
 */
class TWebApplication extends CWebApplication
{

	const DEFAULT_ROLE = 'Authenticated';

}
