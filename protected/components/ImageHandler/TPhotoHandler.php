<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dimon
 * Date: 11.06.13
 * Time: 22:39
 * To change this template use File | Settings | File Templates.
 */

class TPhotoHandler extends CApplicationComponent {

	/**
	 * @var string
	 */
	protected $photoUrl = '/uploads/photo/';
	/**
	 * @var string
	 */
	protected $photoAlias = 'webroot.uploads.photo';

	protected $photoSmallWidth = 64;
	protected $photoSmallHeight = 64;
	protected $photoSmallPrefix = 'small-';

	protected $photoMiniWidth = 50;
	protected $photoMiniHeight = 50;
	protected $photoMiniPrefix = 'mini-';

	protected $photoMediumWidth = 220;
	protected $photoMediumHeight = 250;
	protected $photoMediumPrefix = 'medium-';

	protected $watermarkAlias = 'application.assets.img.logo';

	protected $watermarkBigName = 'watermark-big.png';

	protected $watermarkMediumName = 'watermark-medium.png';

	protected $watermarkSmallName = 'watermark-small.png';

	/**
	 * @var CUploadedFile
	 */
	protected $_photo = null;

	protected $_name = null;

	public function __construct(TPhotoHandlerInterface $object, CUploadedFile $photo = null) {
		$this->_photo = $photo;
		$this->photoUrl = $object->getBasePhotoUrl();
		$this->photoAlias = $object->getBasePhotoAlias();

		$this->photoSmallWidth = $object->getPhotoSmallWidth();
		$this->photoSmallHeight = $object->getPhotoSmallHeight();
		$this->photoSmallPrefix = $object->getPhotoSmallPrefix();

		$this->photoMediumWidth = $object->getPhotoMediumWidth();
		$this->photoMediumHeight = $object->getPhotoMediumHeight();
		$this->photoMediumPrefix = $object->getPhotoMediumPrefix();

		$this->photoMiniWidth = $object->getPhotoMiniWidth();
		$this->photoMiniHeight = $object->getPhotoMiniHeight();
		$this->photoMiniPrefix = $object->getPhotoMiniPrefix();
	}

	public function __destruct() {

	}

	/**
	 * Create file name
	 * @return string
	 */
	public function createName() {
		if (!$this->_name) {
			$this->_name = uniqid() . '.' . $this->_photo->extensionName;
		}
	}

	public function getName() {
		$this->createName();
		return $this->_name;
	}

	/**
	 * Load image. Create thumbnails with watermark
	 * @param bool $watermark use or not watermark
	 * @return string original filename
	 */
	public function load($watermark = true) {
		$this->createName();
		$ImgHandler = new CImageHandler;

		/** @var CImageHandler $ImgHandler */
		$ImgHandler = $ImgHandler->load($this->_photo->tempName);

		if ($watermark) {
			$ImgHandler = $ImgHandler->watermark(
				$this->getWatermark($this->watermarkBigName), 10, 20
			);
		}

		$ImgHandler->save($this->getPhotoPath())->reload()
			->resize($this->photoMediumWidth, $this->photoMediumHeight);

		if ($watermark) {
			$ImgHandler = $ImgHandler->watermark(
				$this->getWatermark($this->watermarkMediumName), 5, 10
			);
		}

		$ImgHandler->save($this->getPhotoPath($this->_name, $this->photoMediumPrefix))
			->reload()->resize($this->photoSmallWidth, $this->photoSmallHeight);

		if ($watermark) {
			$ImgHandler = $ImgHandler->watermark(
				$this->getWatermark($this->watermarkSmallName), 5, 5
			);
		}

		$ImgHandler->save($this->getPhotoPath($this->_name, $this->photoSmallPrefix))
			->reload()->resize($this->photoMiniWidth, $this->photoMiniHeight)
			->save($this->getPhotoPath($this->_name, $this->photoMiniPrefix));

		return $this->_name;
	}

	/**
	 * @param string $name
	 * @param string $prefix
	 * @return string
	 */
	public function getPhotoPath($name = '', $prefix = '') {
		if (!$name) {
			$name = $this->_name;
		}
		$alias = Yii::getPathOfAlias($this->photoAlias) . DIRECTORY_SEPARATOR;

		return $prefix ? $alias . $prefix . $name : $alias . $name;
	}

	/**
	 * @param string $name
	 * @param string $prefix
	 * @return null|string null if file not exists
	 */
	public function getPhotoUrl($name = '', $prefix = '') {
		if (!$name) {
			$name = $this->_name;
		}

		$path = $this->getPhotoPath($name, $prefix);

		if (!file_exists($path)) {
			return null;
		}

		$url = $prefix ? $this->photoUrl . $prefix . $name : $this->photoUrl . $name;

		return Yii::app()->createAbsoluteUrl($url);
	}

	public function getWatermark($fileName) {
		$alias = Yii::getPathOfAlias($this->watermarkAlias) . DIRECTORY_SEPARATOR;

		return $alias . $fileName;
	}

}