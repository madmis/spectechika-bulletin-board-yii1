<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class TUserIdentity extends CUserIdentity {
	private $__id;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate() {
		/** @var $user User */
		$user = User::model()->find('email=:email', array(':email' => $this->username));
		if ($user === null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} elseif (!$user->validatePassword($this->password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else {
			$this->__id = $user->id;
			$this->username = !$user->username ? $user->email : $user->username;

			$this->setState('id', $this->id);
			$this->setState('name', $this->name);
			$this->setState('serviceId', $this->id);
			$this->setState('service', 'email');

			$this->errorCode = self::ERROR_NONE;
		}

		return $this->errorCode == self::ERROR_NONE;
	}

	/**
	 * Get user id
	 * @return int
	 */
	public function getId() {
		return $this->__id;
	}
}