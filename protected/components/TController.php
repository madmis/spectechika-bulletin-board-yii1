<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 * @property string $pageDescription The page description.
 * @property string $pageKeywords The page keywords.
 * @property string $ogTitle The Open Graph title.
 * @property string $ogDescription The Open Graph description.
 * @property string $ogImage The Open Graph image.
 */
class TController extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();

	private $__assetsBase;

	/**
	 * @var string
	 */
	private $__pageDescription;
	/**
	 * @var string
	 */
	private $__pageKeywords;

	/**
	 * @var string Open Graph title
	 */
	private $__ogTitle;
	/**
	 * @var string Open Graph description
	 */
	private $__ogDescription;
	/**
	 * @var string Open Graph image
	 */
	private $__ogImage;

	/**
	 * @var Category data to search form
	 */
	public $searchCategory = null;

	/**
	 * @var Region data to search form
	 */
	public $searchRegion = null;


	public function init()
	{
		$this->_addPackages();
	}

	protected function _addPackages()
	{
		Yii::app()->clientScript->coreScriptPosition = CClientScript::POS_END;
		$bootstrapPackage = array(
			'baseUrl' => '//netdna.bootstrapcdn.com/',
			'js' => array('twitter-bootstrap/2.3.2/js/bootstrap.min.js'),
			'css' => array(
				'twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css',
				'font-awesome/3.2.1/css/font-awesome.min.css'
			),
			'depends' => array('jquery', 'cookie'),
		);
		Yii::app()->clientScript->addPackage('bootstrapPackage', $bootstrapPackage)->registerPackage('bootstrapPackage');

		$mainPackage = array(
			'basePath' => 'application.assets',
			'js' => array(
				'js/noty/jquery.noty.js',
				'js/noty/layouts/topRight.js',
				'js/noty/layouts/center.js',
				'js/noty/themes/default.js',
				'js/bootstrap.js',
				'js/config.t.js',
				'js/api.t.js',
				'js/noty.t.js',
				'js/search.t.js',
			),
			'css' => array(
				'css/design.css',
				'css/blue.css',
				'css/bootstrap-extends.css',
				'css/main.css',
			),
			'depends' => array('jquery', 'cookie'),
		);
		Yii::app()->clientScript->addPackage('mainPackage', $mainPackage)->registerPackage('mainPackage');

		$rentBidPackage = array(
			'basePath' => 'application.assets',
			'js' => array(
				'js/select2/select2.min.js',
				'js/select2/select2_locale_' . Yii::app()->language . '.js',
				'js/rent.bid.t.js',
			),
			'css' => array('js/select2/select2.css'),
			'depends' => array('mainPackage'),
		);
		Yii::app()->clientScript->addPackage('rentBidPackage', $rentBidPackage);
	}

	/**
	 * http://habrahabr.ru/post/139166/
	 * Сейчас во вьюшках и лейаутах можем писать что-то типа:
	 * <link rel="stylesheet" type="text/css" href="<?=$this->assetsBase?>/css/main.css" />
	 * или
	 * <?Yii::app()->clientScript->registerScriptFile($this->assetsBase.'/js/utils.js')?>
	 * С виджетами дело обстоит немного по-другому, нужен свой подход, поскольку вьюшки виджета выполняются
	 * не в контексте контроллера, а в контексте виждета, поэтому для подключения Javascript файла во вьюшке виджета будем писать:
	 * <?Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetsBase.'/js/widget.js')?>
	 * @return mixed
	 */
	public function getAssetsBase()
	{
		if ($this->__assetsBase === null) {
			$this->__assetsBase = Yii::app()->assetManager->publish(
				Yii::getPathOfAlias('application.assets'),
				FALSE,
				-1,
				YII_DEBUG
			);
		}
		return $this->__assetsBase;
	}

	/**
	 * @return string
	 */
	public function getPageDescription()
	{
		return $this->__pageDescription;
	}

	/**
	 * @param string $value
	 */
	public function setPageDescription($value)
	{
		$this->__pageDescription = $value;
	}

	/**
	 * @return string
	 */
	public function getPageKeywords()
	{
		return $this->__pageKeywords;
	}

	/**
	 * @param string $value
	 */
	public function setPageKeywords($value)
	{
		$this->__pageKeywords = $value;
	}

	/**
	 * @return string
	 */
	public function getOgTitle()
	{
		if (!$this->__ogTitle) {
			$this->__ogTitle = $this->pageTitle;
		}

		return $this->__ogTitle;
	}

	/**
	 * @param $value
	 */
	public function setOgTitle($value)
	{
		$this->__ogTitle = $value;
	}

	/**
	 * @return string
	 */
	public function getOgDescription()
	{
		if (!$this->__ogDescription) {
			$this->__ogDescription = $this->__pageDescription;
		}

		return $this->__ogDescription;
	}

	/**
	 * @param $value
	 */
	public function setOgDescription($value)
	{
		$this->__ogDescription = $value;
	}

	/**
	 * @return string
	 */
	public function getOgImage()
	{
		if (!$this->__ogImage) {
			$this->__ogImage = $this->createAbsoluteUrl($this->getAssetsBase() . '/img/logo/logo.png');
		}

		return $this->__ogImage;
	}

	/**
	 * @param $value
	 */
	public function setOgImage($value)
	{
		$this->__ogImage = $value;
	}

	/**
	 * Return data to browser as JSON
	 * @param mixed $data
	 * @param string $message
	 * @param string $status ok|error
	 */
	protected function renderJSON($data, $message = null, $status = 'ok')
	{
		if (!headers_sent()) {
			header('Content-type: application/json');
		}
		$data = array(
			'message' => $message,
			'result' => $data,
			'status' => $status,
		);
		echo CJSON::encode($data);
		Yii::app()->end();
	}

}