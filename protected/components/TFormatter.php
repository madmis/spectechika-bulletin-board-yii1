<?php
/**
 * TFormatter provides a set of commonly used data formatting methods.
 * The formatting methods provided by CFormatter are all named in the form of <code>formatXyz</code>.
 * The behavior of some of them may be configured via the properties of CFormatter. For example,
 * by configuring {@link dateFormat}, one may control how {@link formatDate} formats the value into a date string.
 *
 * For convenience, CFormatter also implements the mechanism of calling formatting methods with their shortcuts (called types).
 * In particular, if a formatting method is named <code>formatXyz</code>, then its shortcut method is <code>xyz</code>
 * (case-insensitive). For example, calling <code>$formatter->date($value)</code> is equivalent to calling
 * <code>$formatter->formatDate($value)</code>.
 *
 * Currently, the following types are recognizable:
 * <ul>
 * <li>raw: the attribute value will not be changed at all.</li>
 * <li>text: the attribute value will be HTML-encoded when rendering.</li>
 * <li>ntext: the {@link formatNtext} method will be called to format the attribute value as a HTML-encoded plain text with newlines converted as the HTML &lt;br /&gt; or &lt;p&gt;&lt;/p&gt; tags.</li>
 * <li>html: the attribute value will be purified and then returned.</li>
 * <li>date: the {@link formatDate} method will be called to format the attribute value as a date.</li>
 * <li>time: the {@link formatTime} method will be called to format the attribute value as a time.</li>
 * <li>datetime: the {@link formatDatetime} method will be called to format the attribute value as a date with time.</li>
 * <li>boolean: the {@link formatBoolean} method will be called to format the attribute value as a boolean display.</li>
 * <li>number: the {@link formatNumber} method will be called to format the attribute value as a number display.</li>
 * <li>email: the {@link formatEmail} method will be called to format the attribute value as a mailto link.</li>
 * <li>image: the {@link formatImage} method will be called to format the attribute value as an image tag where the attribute value is the image URL.</li>
 * <li>url: the {@link formatUrl} method will be called to format the attribute value as a hyperlink where the attribute value is the URL.</li>
 * <li>size: the {@link formatSize} method will be called to format the attribute value, interpreted as a number of bytes, as a size in human readable form.</li>
 * </ul>
 *
 * By default, {@link CApplication} registers {@link CFormatter} as an application component whose ID is 'format'.
 * Therefore, one may call <code>Yii::app()->format->boolean(1)</code>.
 *
 * @property CHtmlPurifier $htmlPurifier The HTML purifier instance.
 *
 */
class TFormatter extends CFormatter {

	/**
	 * @var string the format string to be used to format a date on the client side. Defaults to 'dd.mm.yyyy'.
	 */
	public $clientDateFormat = 'dd.mm.yyyy';
	/**
	 * @var string the format string to be used to format a time on the client side. Defaults to 'hh:ii:ss'.
	 */
	public $clientTimeFormat = 'hh:ii:ss';
	/**
	 * @var string the format string to be used to format a date and time on the client side. Defaults to 'dd.mm.yyyy hh:ii:ss'.
	 */
	public $clientDateTimeFormat = 'dd.mm.yyyy hh:ii:ss';

	/**
	 * @var string the format string to be used to format a date on the db. Defaults to 'Y-m-d'.
	 */
	public $dbDateFormat = 'Y-m-d';
	/**
	 * @var string the format string to be used to format a date on the db. Defaults to 'Y-m-d H:i:s'.
	 */
	public $dbDateTimeFormat = 'Y-m-d H:i:s';

	/**
	 * Formats the value as a date fo Db (TFormatter::dbDateFormat).
	 * @param mixed $value the value to be formatted
	 * @return string the formatted result
	 */
	public function formatDbDate($value) {
		return date($this->dbDateFormat, $this->normalizeDateValue($value));
	}

	/**
	 * Formats the value as a date fo Db (TFormatter::dbDateTimeFormat).
	 * @param mixed $value the value to be formatted
	 * @return string the formatted result
	 */
	public function formatDbDateTime($value) {
		return date($this->dbDateTimeFormat, $this->normalizeDateValue($value));
	}
}
