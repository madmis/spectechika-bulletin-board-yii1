<?php


/**
 * Class TAdminNotifier
 * Notify administrator
 */
class TAdminNotifier extends TNotifier
{

	/**
	 * Send email when new user
	 * @param CModelEvent $event
	 * @return bool
	 */
	public function newUser(CModelEvent $event)
	{
		$mailer = Yii::app()->mailer;
		$mailer->setSubject(Yii::t('main', 'Новый пользователь'));
		$params = array(
			'title' => $mailer->getSubject(),
			'User' => $event->sender,
		);

//		$mailer->setTo($this->_adminEmail())
//			->setBody($this->_getMessage('newUser', $params))
//			->addPart($this->_getMessage('newUser', $params, false));

		$mailer->setTo($this->_adminEmail())
			->setBody($this->_getMessage('newUser', $params, false), $mailer::BODY_TYPE_TEXT);

		$event->isValid = Yii::app()->mailer->Send();
		return $event->isValid;
	}

	/**
	 * Send email when new rent
	 * @param CModelEvent $event
	 * @return bool
	 */
	public function newRent(CModelEvent $event)
	{
		$mailer = Yii::app()->mailer;
		$mailer->setSubject(Yii::t('main', 'Новое объявление "Возьму в аренду"'));
		$params = array(
			'title' => $mailer->getSubject(),
			'Rent' => $event->sender,
		);

		$mailer->setTo($this->_adminEmail())
			->setBody($this->_getMessage('newRent', $params, false), $mailer::BODY_TYPE_TEXT);

		$event->isValid = Yii::app()->mailer->Send();
		return $event->isValid;

	}

	/**
	 * Send email when new to rent
	 * @param CModelEvent $event
	 * @return bool
	 */
	public function newToRent(CModelEvent $event)
	{
		$mailer = Yii::app()->mailer;
		$mailer->setSubject(Yii::t('main', 'Новое объявление "Сдам в аренду"'));
		$params = array(
			'title' => $mailer->getSubject(),
			'ToRent' => $event->sender,
		);

		$mailer->setTo($this->_adminEmail())
			->setBody($this->_getMessage('newToRent', $params, false), $mailer::BODY_TYPE_TEXT);

		$event->isValid = Yii::app()->mailer->Send();
		return $event->isValid;
	}
}