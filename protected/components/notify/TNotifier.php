<?php

/**
 * Class TNotifier
 * Base notifier class
 */
abstract class TNotifier extends CComponent
{
	/**
	 * Get admin email
	 * @return array
	 */
	protected function _adminEmail()
	{
		return array(Yii::app()->params->itemAt('adminEmail'));
	}

	/**
	 * Get mail message
	 * @param string $view view name
	 * @param array $params
	 * @param bool $isHtml html or text
	 * @return string
	 */
	protected function _getMessage($view, array $params = array(), $isHtml = true)
	{
		return Yii::app()->mailMessage->getMessage($view, $params, $isHtml);
	}
}