<?php
/**
 * Class TServiceIdentity
 */
class TServiceIdentity extends EAuthUserIdentity {

	const ERROR_INSERT_USER = 20;
	const ERROR_INSERT_USER_SERVICE = 21;

	/**
	 * Аутентификация пользователя, основанная на данных сервиса
	 * Здесь же выполняется добавление пользователя в БД
	 * или получение его данных, если он уже есть в базе
	 * @return bool
	 */
	public function authenticate() {
			// если пользователь авторизован через сервис, выполняем дальнейшие действия
		if ($this->service->getIsAuthenticated()) {
			$UserService = UserService::model()->getUserByService($this->service);

			$this->errorCode = self::ERROR_NONE;
			// если пользователь есть, просто авторизуем его
			if ($UserService != null) {
				$UserService->user->last_visit = Yii::app()->format->formatDbDate('now');
				$UserService->user->save();
			} else {
				$UserService = $this->__addNewUser();
			}

			if (!$this->errorCode) {
				$this->id = $UserService->user_id;
				$this->name = $UserService->user->username;
				$this->setState('id', $this->id);
				$this->setState('name', $this->name);
				$this->setState('serviceId', $this->service->id);
				$this->setState('service', $this->service->serviceName);
			}
		} else {
			$this->errorCode = self::ERROR_NOT_AUTHENTICATED;
		}

		return !$this->errorCode;
	}

	/**
	 * @return null|UserService
	 */
	private function __addNewUser() {
		$User = new User();
		$result = null;
		$User->username = $this->service->getAttribute('name');
		if ($User->save()) {
			// Добавляем запись в user_service
			$UserService = new UserService();
			$UserService->user_id = $User->id;
			$UserService->service = $this->service->serviceName;
			$UserService->identity = $this->service->id;
			if (!$UserService->save()) {
				$this->errorCode = self::ERROR_INSERT_USER_SERVICE;
			} else {
				// Если авторизация прошла успешно и данные пользователя добавлены
				// назначем пользователю роль авторизованного пользователя
				$result = $UserService;
				Yii::app()->authManager->assign(TWebApplication::DEFAULT_ROLE, $User->id);
			}
		} else {
			$this->errorCode = self::ERROR_INSERT_USER;
		}

		return $result;
	}
}
