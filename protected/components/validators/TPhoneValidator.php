<?php
/**
 * Phone validator class
 */
class TPhoneValidator extends CValidator {

	public $pattern = '/^(\+380)\s\((\d){2}\)\s(\d){3}\s(\d){2}\s(\d){2}$/';
	public $message = 'Указан неправильный формат телефонного номера';

	/**
	 * @param CModel $object the object being validated
	 * @param string $attribute the attribute being validated
	 */
	public function validateAttribute($object, $attribute) {
		$value = $object->$attribute;
		if (!empty($value)) {
			if (!preg_match($this->pattern, $value)) {
				$this->addError($object, $attribute, $this->message);
			}
		}
	}

	/**
	 * Returns the JavaScript needed for performing client-side validation.
	 * @param CModel $object the data object being validated
	 * @param string $attribute the name of the attribute to be validated.
	 * @return string the client-side validation script.
	 * @see CActiveForm::enableClientValidation
	 */
	public function clientValidateAttribute($object, $attribute) {
		// check the strength parameter used in the validation rule of our model
		$condition = "value && !value.match({$this->pattern})";

		return "if(" . $condition . ") {
    				messages.push(" . CJSON::encode($this->message) . ");
				}";
	}
}
