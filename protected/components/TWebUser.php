<?php

/**
 * @property AuthWebUser $this
 */
class TWebUser extends AuthWebUser {

	/**
	 * Получить loginUrl
	 * @return string
	 */
	public function getLoginUrl() {
		if (is_array($this->loginUrl)) {
			$list = new CList($this->loginUrl, true);
			return $list->itemAt(0);
		} else {
			return $this->loginUrl;
		}
	}

	public function afterLogin($fromCookie) {
		parent::afterLogin($fromCookie);
		$user = User::model()->findByPk($this->id);
		$user->scenario = 'login';
		// update last visit
		$user->last_visit = Yii::app()->format->formatDbDateTime('now');
		$user->save(false);
	}

	/**
	 * Получить путь к аватарке пользователя
	 * @param string $size ('big', 'medium', 'small'). Use User::AVATAR_BIG etc.
	 * @return string
	 */
//	public function getAvatar($size = User::AVATAR_SMALL) {
//		return User::model()->getAvatar($size, Yii::app()->user->id);
//	}
}
