<?php

class FacebookOAuth extends FacebookOAuthService {
	/**
	 * https://developers.facebook.com/docs/authentication/permissions/
	 */
	protected $scope = 'user_birthday,user_hometown,user_location,user_photos';

	/**
	 * http://developers.facebook.com/docs/reference/api/user/
	 * @see FacebookOAuthService::fetchAttributes()
	 */
	public function fetchAttributes() {
		$this->attributes = (array)$this->makeSignedRequest('https://graph.facebook.com/me');
	}

	/**
	 * Load user avatars
	 * @return bool
	 */
	public function loadAvatar() {
		$img = array(
			'big' => 'https://graph.facebook.com/' . $this->getAttribute('id') . '/picture?type=large',
			'medium' => 'https://graph.facebook.com/' . $this->getAttribute('id') . '/picture?type=normal',
			'small' => 'https://graph.facebook.com/' . $this->getAttribute('id') . '/picture?type=square',
		);

//		return ImgHelper::loadUserAvatars($img);
	}

}
