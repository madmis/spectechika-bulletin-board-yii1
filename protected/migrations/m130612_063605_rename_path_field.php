<?php

class m130612_063605_rename_path_field extends CDbMigration {
	public function up() {
		$this->dropColumn('{{photo}}', 'path');
		$this->addColumn('{{photo}}', 'photo', 'VARCHAR(255) NOT NULL AFTER `id`');
	}

	public function down() {
		echo "m130612_063605_rename_path_field does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}