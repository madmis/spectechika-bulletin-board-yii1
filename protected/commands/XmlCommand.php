<?php
/**
 * CLI commands to generate xml
 */
class XmlCommand extends CConsoleCommand
{
	/**
	 * @var DOMDocument
	 */
	private $_dom;

	/**
	 * @var DOMElement
	 */
	private $_root;

	/**
	 * http://interactive-answers.webmaster.yandex.ua/editor/
	 */
	public function actionToRentIsland()
	{
		$this->_dom();
		$this->_root();

		$rootUrl = Yii::app()->createAbsoluteUrl(Yii::app()->baseUrl) . '/toRent';
		$this->_setRootElements(
			$rootUrl, Yii::t('meta', 'island-title'), Yii::t('meta', 'island-torent-description')
		);

		$resourceName = TUrl::extractDomain(Yii::app()->createAbsoluteUrl(Yii::app()->baseUrl)) . '/toRent/search';
		$this->_fixedResource($resourceName);

		$filters = $this->_filters();
		$filters->appendChild($this->_categoryFilter());
		$filters->appendChild($this->_regionFilter());

		$this->_root->appendChild($filters);
		$this->_dom->appendChild($this->_root);
		$this->_save('torent');
	}

	/**
	 * http://interactive-answers.webmaster.yandex.ua/editor/
	 */
	public function actionRentIsland()
	{
		$this->_dom();
		$this->_root();

		$rootUrl = Yii::app()->createAbsoluteUrl(Yii::app()->baseUrl) . '/rent';
		$this->_setRootElements(
			$rootUrl, Yii::t('meta', 'island-title'), Yii::t('meta', 'island-torent-description')
		);

		$resourceName = TUrl::extractDomain(Yii::app()->createAbsoluteUrl(Yii::app()->baseUrl)) . '/rent/search';
		$this->_fixedResource($resourceName);

		$filters = $this->_filters();
		$filters->appendChild($this->_categoryFilter());
		$filters->appendChild($this->_regionFilter());

		$this->_root->appendChild($filters);
		$this->_dom->appendChild($this->_root);
		$this->_save('rent');
	}



	private function _dom()
	{
		$this->_dom = new DOMDocument('1.0', 'utf-8');
	}

	private function _root()
	{
		$this->_root = $this->_dom->createElement('site');
		$this->_root->setAttribute('xmlns', 'http://interactive-answers.webmaster.yandex.ru/schemas/site/0.0.1');
		$this->_root->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
		$this->_root->setAttribute('xsi:schemaLocation', 'http://interactive-answers.webmaster.yandex.ru/schemas/site-0.0.1.xsd');
	}

	/**
	 * @param string $type torent|rent
	 */
	private function _save($type)
	{
		$path = Yii::getPathOfAlias('application.runtime') . '/';
		$file = "yandex.{$type}.island." . Yii::app()->format->formatDbDate('now') . '.xml';
		$path .= $file;
		if ($this->_dom->save($path)) {
			$this->prompt('File saved to ' . $path);
		} else {
			$this->usageError('Error to save file' . $path);
		}
	}

	/**
	 * @param $rootUrl
	 * @param $title
	 * @param $description
	 */
	private function _setRootElements($rootUrl, $title, $description)
	{
		$this->_root->appendChild($this->_dom->createElement('rootUrl', $rootUrl));
		$this->_root->appendChild($this->_dom->createElement('title', $title));
		$this->_root->appendChild($this->_dom->createElement('description', $description));

	}

	private function _fixedResource($name)
	{
		$resource = $this->_dom->createElement('resource');
		$main = $this->_dom->createElement('fixed');
		$main->setAttribute('name', $name);
		$resource->appendChild($main);
		$this->_root->appendChild($resource);
	}

	/**
	 * @return DOMElement
	 */
	private function _filters()
	{
		return $this->_dom->createElement('filters');
	}

	/**
	 * @return DOMElement
	 */
	public function _categoryFilter()
	{
		$dropDown = $this->_dom->createElement('dropDown');
		$description = $this->_dom->createElement('description');
		$description->setAttribute('caption', 'Категория');
		$setParameter = $this->_dom->createElement('setParameter');
		$setParameter->setAttribute('name', 'category');
		$description->appendChild($setParameter);
		$dropDown->appendChild($description);

		$criteria = new CDbCriteria();
		$criteria->order = 'name ASC';
		$categories = Category::model()->findAll($criteria);
		foreach ($categories as $item) {
			/** @var $item Category */
			$dropDownValue = $this->_dom->createElement('dropDownValue');
			$dropDownValue->setAttribute('key', $item->id);
			$dropDownValue->setAttribute('caption', $item->name);
			$dropDown->appendChild($dropDownValue);
		}

		return $dropDown;
	}

	/**
	 * @return DOMElement
	 */
	private function _regionFilter()
	{
		$dropDown = $this->_dom->createElement('dropDown');
		$description = $this->_dom->createElement('description');
		$description->setAttribute('caption', 'Регион');
		$setParameter = $this->_dom->createElement('setParameter');
		$setParameter->setAttribute('name', 'region');
		$description->appendChild($setParameter);
		$dropDown->appendChild($description);

		$criteria = new CDbCriteria();
		$criteria->order = 'name ASC';
		$regions = Region::model()->findAll($criteria);
		foreach ($regions as $item) {
			/** @var $item Region */
			$dropDownValue = $this->_dom->createElement('dropDownValue');
			$dropDownValue->setAttribute('key', $item->id);
			$dropDownValue->setAttribute('caption', $item->name);
			$dropDown->appendChild($dropDownValue);
		}
		return $dropDown;
	}
}