<?php

class MorpherCommand extends CConsoleCommand {

	protected $_url = 'http://morpher.ru/WebService.asmx/GetXml';
	protected $_param = 's';

	public function actionCategory() {
		$criteria = new CDbCriteria();
		$criteria->addCondition("name_r = ''");
		$items = Category::model()->findAll($criteria);

		foreach($items as $item) {
			/** @var Category $item */
			$xml = Yii::app()->curl->get(
				$this->_url,
				array($this->_param => $item->name)
			);

			if (!$xml) {
				echo "no result for: {$item->name}" . PHP_EOL;
				continue;
			}
			$xml = $this->simpleXml($xml);

			$item->name_r = strval($xml->{'Р'}) ? strval($xml->{'Р'}) : '';
			$item->name_d = strval($xml->{'Д'}) ? strval($xml->{'Д'}) : '';
			$item->name_v = strval($xml->{'В'}) ? strval($xml->{'В'}) : '';
			$item->name_t = strval($xml->{'Т'}) ? strval($xml->{'Т'}) : '';
			$item->name_p = strval($xml->{'П'}) ? strval($xml->{'П'}) : '';
			$item->save(false);
			echo "saved {$item->name}" . PHP_EOL;
		}
	}

	public function actionRegion() {
		$criteria = new CDbCriteria();
		$criteria->addCondition("name_r = ''");
		$items = Region::model()->findAll($criteria);

		foreach($items as $item) {
			/** @var Region $item */
			$xml = Yii::app()->curl->get(
				$this->_url,
				array($this->_param => $item->name)
			);

			if (!$xml) {
				echo "no result for: {$item->name}" . PHP_EOL;
				continue;
			}
			$xml = $this->simpleXml($xml);

			$item->name_r = strval($xml->{'Р'}) ? strval($xml->{'Р'}) : '';
			$item->name_d = strval($xml->{'Д'}) ? strval($xml->{'Д'}) : '';
			$item->name_v = strval($xml->{'В'}) ? strval($xml->{'В'}) : '';
			$item->name_t = strval($xml->{'Т'}) ? strval($xml->{'Т'}) : '';
			$item->name_p = strval($xml->{'П'}) ? strval($xml->{'П'}) : '';
			$item->save(false);
			echo "saved {$item->name}" . PHP_EOL;
		}
	}

	public function actionCity() {
		$criteria = new CDbCriteria();
		$criteria->addCondition("name_r = ''");
		$items = City::model()->findAll($criteria);

		foreach($items as $item) {
			/** @var City $item */
			$xml = Yii::app()->curl
				->setOption(CURLOPT_PROXY, '174.129.173.237:8088')
				->get(
					$this->_url,
					array($this->_param => $item->name)
				);

			if (!$xml) {
				echo "no result for: {$item->name}" . PHP_EOL;
				continue;
			}
			$xml = $this->simpleXml($xml);

			$item->name_r = strval($xml->{'Р'}) ? strval($xml->{'Р'}) : '';
			$item->name_d = strval($xml->{'Д'}) ? strval($xml->{'Д'}) : '';
			$item->name_v = strval($xml->{'В'}) ? strval($xml->{'В'}) : '';
			$item->name_t = strval($xml->{'Т'}) ? strval($xml->{'Т'}) : '';
			$item->name_p = strval($xml->{'П'}) ? strval($xml->{'П'}) : '';
			$item->save(false);
			echo "saved {$item->name}" . PHP_EOL;
		}
	}

	/**
	 * @param $xml
	 * @return SimpleXMLElement
	 */
	protected function simpleXml($xml) {
		return new SimpleXMLElement($xml);
	}


}