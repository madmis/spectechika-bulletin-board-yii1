<?php
/**
 * TransliterateCommand - transliterate methods.
 */

class TransliterateCommand extends CConsoleCommand {

	public function actionCategory() {
		$items = Category::model()->findAll();
		foreach ($items as $item) {
			/** @var $item Category */
			$item->translit = TString::str2url($item->name);
			$item->save(false);
		}
	}

	public function actionRegion() {
		$items = Region::model()->findAll();
		foreach ($items as $item) {
			/** @var $item Region */
			$item->translit = TString::str2url($item->name);
			$item->save();
		}
	}
}