<?php
/**
 * TImageInput widget class
 * Present field to load one image with image preview.
 * Image preview use js FileApi
 */

class TImageInput extends CInputWidget {

	/**
	 * @var string preview block selector
	 */
	public $previewSelector = 'ul#img-list';
	/**
	 * @var int img preview size in px
	 */
	public $previewSize = 50;

	/**
	 * Initializes the widget.
	 */
	public function init() {
		parent::init();
	}

	/**
	 * render the view.
	 */
	public function run() {
		parent::run();
		list($name, $id) = $this->resolveNameID();
		$result = CHtml::activeFileField($this->model, $this->attribute, $this->htmlOptions);
		echo $result;

		$this->registerClientScript($id);
	}

	public function registerClientScript($id) {
		ob_start();

		echo '// Стандарный input для файлов
				var $fileInput = jQuery("#' . $id . '");
				// ul-список, содержащий миниатюрки выбранных файлов
				var $imgList = jQuery("' . $this->previewSelector . '");
				// Обработка события выбора файлов в стандартном поле
				$fileInput.bind({
					change: function() {
						displayFiles(this.files);
					}
				});';

		echo 'function displayFiles(files) {
				$.each(files, function(i, file) {
					if (!file.type.match(/image.*/)) {
						// Отсеиваем не картинки
						return true;
					}
					// Создаем элемент li и помещаем в него название, миниатюру и progress bar,
					// а также создаем ему свойство file, куда помещаем объект File (при загрузке понадобится)
					var li = $("<li/>");
					//var li = $imgList.html("<li/>");
					var img = $("<img/>");
					li.append(img);
					$imgList.html(li);
					li.get(0).file = file;

					// Создаем объект FileReader и по завершении чтения файла,
					// отображаем миниатюру и обновляем инфу обо всех файлах
					var reader = new FileReader();
					reader.onload = (function(aImg) {
						return function(e) {
							aImg.attr("src", e.target.result);
							aImg.attr("width", ' . $this->previewSize . ');
							/* ... обновляем инфу о выбранных файлах ... */
						};
					})(img);
					reader.readAsDataURL(file);
				});
			}';

		Yii::app()->clientScript->registerScript(__CLASS__ . '#' . $this->getId(), ob_get_clean() . ';');
	}

}