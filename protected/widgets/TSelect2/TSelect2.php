<?php

class TSelect2 extends CInputWidget {
	/**
	 * @var array @param data for generating the list options (value=>display)
	 */
	public $data = array();

	/**
	 * @var string[] the JavaScript event handlers.
	 */
	public $events = array();

	/**
	 * @var bool whether to display a dropdown select box or use it for tagging
	 */
	public $asDropDownList = true;

	/**
	 * @var string locale. Defaults to null. Possible values: "it"
	 */
	public $language;

	/**
	 * @var array the plugin options
	 */
	public $options;

	/**
	 * Initializes the widget.
	 */
	public function init() {
		if (empty($this->data) && $this->asDropDownList === true) {
			throw new CException(Yii::t('zii', '"data" attribute cannot be blank'));
		}
	}

	/**
	 * Runs the widget.
	 */
	public function run() {
		$this->renderField();
		$this->registerClientScript();
	}

	/**
	 * Sets the default value for an item in the given options.
	 * @param string $name the name of the item.
	 * @param mixed $value the default value.
	 * @param array $options the options.
	 * @return mixed
	 */
	public static function defaultOption($name, $value, $options) {
		if (is_array($options) && !isset($options[$name])) {
			$options[$name] = $value;
		}
		return $options;
	}

	/**
	 * Returns an item from the given options or the default value if it's not set.
	 * @param string $name the name of the item.
	 * @param array $options the options to get from.
	 * @param mixed $defaultValue the default value.
	 * @return mixed the value.
	 */
	public static function getOption($name, $options, $defaultValue = null) {
		return (is_array($options) && isset($options[$name])) ? $options[$name] : $defaultValue;
	}

	/**
	 * Registers a specific Bootstrap plugin using the given selector and options.
	 * @param string $name the plugin name.
	 * @param string $selector the CSS selector.
	 * @param array $options the JavaScript options for the plugin.
	 * @param int $position the position of the JavaScript code.
	 */
	public function registerPlugin($name, $selector, $options = array(), $position = CClientScript::POS_END) {
		$options = !empty($options) ? CJavaScript::encode($options) : '';
		$script = "jQuery('{$selector}').{$name}({$options});";
		$id = __CLASS__ . '#Plugin' . uniqid();
		Yii::app()->clientScript->registerScript($id, $script, $position);
	}

	/**
	 * Registers events using the given selector.
	 * @param string $selector the CSS selector.
	 * @param string[] $events the JavaScript event configuration (name=>handler).
	 * @param int $position the position of the JavaScript code.
	 */
	public function registerEvents($selector, $events, $position = CClientScript::POS_END) {
		if (empty($events)) {
			return;
		}

		$script = '';
		foreach ($events as $name => $handler) {
			$handler = ($handler instanceof CJavaScriptExpression)
				? $handler
				: new CJavaScriptExpression($handler);

			$script .= "jQuery('{$selector}').on('{$name}', {$handler});";
		}
		$id = __CLASS__ . '#Events' . uniqid();
		Yii::app()->clientScript->registerScript($id, $script, $position);
	}

	/**
	 * Renders the select2 field
	 */
	public function renderField() {
		list($name, $id) = $this->resolveNameID();

		$this->htmlOptions = self::defaultOption('id', $id, $this->htmlOptions);
		$this->htmlOptions = self::defaultOption('name', $name, $this->htmlOptions);

		if ($this->hasModel()) {
			echo $this->asDropDownList ?
				CHtml::activeDropDownList($this->model, $this->attribute, $this->data, $this->htmlOptions) :
				CHtml::activeHiddenField($this->model, $this->attribute);

		} else {
			echo $this->asDropDownList ?
				CHtml::dropDownList($this->name, $this->value, $this->data, $this->htmlOptions) :
				CHtml::hiddenField($this->name, $this->value);
		}
	}

	/**
	 * Registers required client script for bootstrap select2. It is not used through bootstrap->registerPlugin
	 * in order to attach events if any
	 */
	public function registerClientScript() {
		/* publish assets dir */
		$path = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
		$assetsUrl = Yii::app()->assetManager->publish($path);

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();

		$cs->registerCssFile($assetsUrl . '/css/select2.css');
		$cs->registerScriptFile($assetsUrl . '/js/select2.js', CClientScript::POS_END);


		if ($this->language) {
			$cs->registerScriptFile(
				$assetsUrl . '/js/locale/select2_locale_' . $this->language . '.js',
				CClientScript::POS_END
			);
		}

		/* initialize plugin */
		$selector = '#' . self::getOption('id', $this->htmlOptions, $this->getId());

		$this->registerPlugin('select2', $selector, $this->options);
		$this->registerEvents($selector, $this->events);
	}
}