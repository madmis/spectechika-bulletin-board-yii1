<?php

class MainCategory extends CWidget
{
	/**
	 * Initializes the widget.
	 */
	public function init() {
	}

	/**
	 * render the view.
	 */
	public function run() {
		$criteria = new CDbCriteria;
		$criteria->order = 'rating DESC';
		$items = Category::model()->parentAndChild();

		$this->render('block', array('items' => $items));
	}

}