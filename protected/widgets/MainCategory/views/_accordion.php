<?php
/** @var $this MainCategory */
/** @var $items CMap */
?>
<div class="accordion" id="accordion2">
	<?php /** @var $Category Category */ ?>
	<?php foreach($items->itemAt('parent') as $Category): ?>
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle" data-toggle="collapse"
				   data-parent="#accordion2" href="#collapse-<?php echo $Category->id; ?>">
					<?php echo $Category->name; ?>
				</a>
			</div>
			<?php
			$child = $items->itemAt('child');
			if (!empty($child)) {
				$child = $child[$Category->id];
			}
			?>
			<div id="collapse-<?php echo $Category->id; ?>" class="accordion-body collapse">
				<div class="accordion-inner">
					<div class="row-fluid">
						<div class="span6">
							<dl>
								<dt>Сдам в аренду</dt>
								<?php /** @var $Item Category */ ?>
								<?php foreach ($child as $Item): ?>
									<dd>
										<?php echo CHtml::link($Item->name, array("/toRent/category/{$Item->translit}"), array('title' => $Item->name)); ?>
									</dd>
								<?php endforeach; ?>
							</dl>
						</div>
						<div class="span6">
							<dl>
								<dt>Возьму в аренду</dt>
								<?php /** @var $Item Category */ ?>
								<?php foreach ($child as $Item): ?>
									<dd>
										<?php echo CHtml::link($Item->name, array("/rent/category/{$Item->translit}"), array('title' => $Item->name)); ?>
									</dd>
								<?php endforeach; ?>
							</dl>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>