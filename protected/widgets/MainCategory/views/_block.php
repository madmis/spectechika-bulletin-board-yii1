<?php /*
<?php foreach ($items->itemAt('child') as $parent_id => $items): ?>
	<div class="tab-pane" id="main-torent-tab-<?php echo $parent_id; ?>">
		<div class="row-fluid">
			<div class="span12">
				<ul class="main-category-block">
					<?php foreach ($items as $Item): ?>
						<?php // @var $Item Category  ?>
						<li class="item">
							<?php echo CHtml::link($Item->name, array("/{$section}/category/{$Item->translit}"), array('title' => $Item->name)); ?>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
<?php endforeach; ?>
*/?>

<div class="row-fluid">
	<div class="span12 grey-box">
		<div class="hero-block3">
			<div class="hero-content-3">
				<ul class="main-category-block">
					<?php /** @var $Category Category */ ?>
					<?php foreach($items->itemAt('parent') as $Category): ?>
						<li class="item">
							<dl>
								<dt title="Категория: <?php echo $Category->name; ?>"><?php echo $Category->name; ?></dt>
								<?php
								$child = $items->itemAt('child');
								if (!empty($child)) {
									$child = $child[$Category->id];
								}
								?>
								<?php /** @var $Item Category */ ?>
								<?php foreach ($child as $Item): ?>
									<dd>
										<?php echo CHtml::link($Item->name, array("/{$section}/category/{$Item->translit}"), array('title' => $Item->name)); ?>
									</dd>
								<?php endforeach; ?>
							</dl>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
</div>
