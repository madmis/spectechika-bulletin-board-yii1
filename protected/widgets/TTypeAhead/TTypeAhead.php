<?php

class TTypeAhead extends CInputWidget {

	/**
	 * @var array the plugin options
	 * @see https://github.com/twitter/typeahead.js
	 */
	public $options;

	/**
	 * @var bool whether to display minified versions of the files or not
	 */
	public $debugMode = false;

	/**
	 * Initializes the widget.
	 */
	public function init() {
	}

	/**
	 * Runs the widget.
	 */
	public function run() {
		$this->renderField();
		$this->registerClientScript();
	}

	/**
	 * Sets the default value for an item in the given options.
	 * @param string $name the name of the item.
	 * @param mixed $value the default value.
	 * @param array $options the options.
	 * @return mixed
	 */
	public static function defaultOption($name, $value, $options) {
		if (is_array($options) && !isset($options[$name])) {
			$options[$name] = $value;
		}
		return $options;
	}

	/**
	 * Returns an item from the given options or the default value if it's not set.
	 * @param string $name the name of the item.
	 * @param array $options the options to get from.
	 * @param mixed $defaultValue the default value.
	 * @return mixed the value.
	 */
	public static function getOption($name, $options, $defaultValue = null)
	{
		return (is_array($options) && isset($options[$name])) ? $options[$name] : $defaultValue;
	}

	/**
	 * Registers a specific Bootstrap plugin using the given selector and options.
	 * @param string $name the plugin name.
	 * @param string $selector the CSS selector.
	 * @param array $options the JavaScript options for the plugin.
	 * @param int $position the position of the JavaScript code.
	 */
	public function registerPlugin($name, $selector, $options = array(), $position = CClientScript::POS_END)
	{
		$options = !empty($options) ? CJavaScript::encode($options) : '';
		$script = "jQuery('{$selector}').{$name}({$options});";
		$id = __CLASS__ . '#Plugin' . uniqid();
		Yii::app()->clientScript->registerScript($id, $script, $position);
	}


	/**
	 * Renders the typeahead field
	 */
	public function renderField() {
		list($name, $id) = $this->resolveNameID();

		$this->htmlOptions = self::defaultOption('id', $id, $this->htmlOptions);
		$this->htmlOptions = self::defaultOption('name', $name, $this->htmlOptions);

		if ($this->hasModel()) {
			$this->htmlOptions['value'] = $this->value;
			echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
		} else {
			echo CHtml::textField($this->name, $this->value, $this->htmlOptions);
		}
	}

	/**
	 * Registers required client script for bootstrap typeahead. It is not used through bootstrap->registerPlugin
	 * in order to attach events if any
	 */
	public function registerClientScript() {
		/* publish assets dir */
		$path = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
		$assetsUrl = Yii::app()->assetManager->publish($path);

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();

		$min = $this->debugMode ? '.min' : '';

		$cs->registerCssFile($assetsUrl . '/css/typeahead' . $min . '.css');
		$cs->registerScriptFile($assetsUrl . '/js/typeahead' . $min . '.js', CClientScript::POS_END);

		/* initialize plugin */
		$selector = '#' . self::getOption('id', $this->htmlOptions, $this->getId());

		$this->registerPlugin('typeahead', $selector, $this->options);
	}
}