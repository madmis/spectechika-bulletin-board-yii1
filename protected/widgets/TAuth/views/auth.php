<div class="t-services">
	Авторизация:
	<ul class="t-auth-services clear">
		<?php
		foreach ($services as $name => $service) {
			echo '<li class="auth-service ' . $service->id . '" title="' . $service->title . '">';
			$html = '<span class="t-auth-icon ' . $service->id . '"><i></i></span>';

			$url = array($action);
			if ($name !== 'email') {
				$url['service'] = $name;
			}

			$html = CHtml::link($html, $url, array('class' => 't-auth-link ' . $service->id));
			echo $html;
			echo '</li>';
		}
		?>
	</ul>
</div>
