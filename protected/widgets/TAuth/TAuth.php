<?php

class TAuth extends EAuthWidget
{
	/**
	 * Executes the widget.
	 * This method is called by {@link CBaseController::endWidget}.
	 */
	public function run()
	{
		ob_start();
		parent::run();
		ob_get_clean();

		$Email = new stdClass();
		$Email->id = 'email';
		$Email->title = 'Email';
		$Email->type = 'Internal';
		$Email->jsArguments = array();
		$this->services = array('email' => $Email) + $this->services;

		$this->registerAssets();
		$this->render('auth', array(
			'id' => $this->getId(),
			'services' => $this->services,
			'action' => $this->action,
		));
	}

	/**
	 * Register CSS and JS files.
	 */
	protected function registerAssets()
	{
		parent::registerAssets();
		$cs = Yii::app()->clientScript;

		$assets_path = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
		$url = Yii::app()->assetManager->publish($assets_path, false, -1, YII_DEBUG);
		if ($this->cssFile) {
			$cs->registerCssFile($url . '/css/auth.css');
		}
	}

}