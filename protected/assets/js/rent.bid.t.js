var T = (function (t) {
	'use strict';

	t.rentbid = {
		/** form **/
		formatSelection: function(item) {
			var originalOption = item.element;
			return item.text + ' (' + $(originalOption).data('price') + ')';
		},
		format: function (item) {
			var originalOption = item.element;
			var img = '<img src="' + $(originalOption).data('img') + '"/>&nbsp;&nbsp;'
			return img + item.text + '&nbsp;(' + $(originalOption).data('price') + ')';
		},
		initSelect: function() {
			$("#RentBid_buse_id").select2({
				placeholder: 'Выберите спецтехнику',
				allowClear: true,
				formatResult: t.rentbid.format,
				formatSelection: t.rentbid.formatSelection,
				escapeMarkup: function(m) { return m; }
			});
		},
		registerEvents: function() {
			$(document).on('click', '#add-bid-link', function(e) {
				e.preventDefault();
				$('#add-bid').toggle();
			});
		}
	};

	t.rentbid.registerEvents();
	t.rentbid.initSelect();

	return t;
}(T || {}));



