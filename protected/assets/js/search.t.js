var T = (function (t) {
    "use strict";

    t.search = {
        clickEvent: function() {
			var s = '#search-category-input, #search-region-input';
            $(document).on('click', s, function() {
				var selector = $(this).data('dialog');
				t.search.showDialog(selector);
            });
        },

		showDialog: function(selector) {
			$('#' + selector).modal('show').css({
				width: '750px',
				'margin-left': function () {
					return -($(this).width() / 2);
				}
			});
		},

		registerEvents: function() {
			var $cat = $('#search-category-dialog');
			$cat.on('shown', function () {
				if ( $cat.find('.modal-body:not(:has(:visible))').length ) {
					$cat.find('.parent-block li:first').trigger('click');
				}
			});

			$cat.find('.parent-block .item').on('click', function () {
				$cat.find('.parent-block .item i').remove();
				$(this).prepend('<i class="icon-ok"></i>');
				var selector = $(this).data('target');
				$cat.find('.modal-body ul').hide();
				$('#' + selector).show();
			});

            $(document).on('click', '#search-category-dialog-btn', function(e) {
                e.preventDefault();
                var $checked = $('#search-category-dialog :checked');
                if (!$checked.length) {
                    T.noty.show('error', 'Выберите категорию');
                } else {
                    $('#search-category-input').text($checked.data('title'));
					$('#category-li > .icon-remove').show();
                    $('#search-category-input-h').val($checked.val());
                    $('#search-category-dialog').modal('hide');
                }
            });

            $(document).on('click', '#search-region-dialog-btn', function(e) {
                e.preventDefault();
                var $checked = $('#search-region-dialog :checked');
                if (!$checked.length) {
                    T.noty.show('error', 'Выберите регион');
                } else {
                    $('#search-region-input').text($checked.data('title'));
					$('#region-li > .icon-remove').show();
					$('#search-region-input-h').val($checked.val());
                    $('#search-region-dialog').modal('hide');
                }
            });

			$(document).on('click', '#search-torent-form i.icon-remove', function(e) {
				e.preventDefault();
				var $parent = $(this).parent();
				$parent.find('.input-imitator').html('<span class="placeholder">укажите категорию</span>')
				if ($parent.attr('id') == 'category-li') {
					$('#search-category-input-h').attr('value', '');
				} else {
					$('#search-region-input-h').attr('value', '');
				}
				$(this).hide();
			});

		}
    };

    t.search.clickEvent();
    t.search.registerEvents();

    return t;
}(T || {}));



