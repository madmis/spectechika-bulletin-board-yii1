var T = (function (t) {
	'use strict';

	t.photo = {
		fileInput: $('#Photo_path'), // Стандарный input для файлов
		imgList: $('#img-list'), // ul-список, содержащий миниатюрки выбранных файлов
		dropBox: $('#img-container'), // Контейнер, куда можно помещать файлы методом drag and drop
		imgCount: 0, // Счетчик всех выбранных файлов и их размера
		imgSize: 0,
		countInfo: $('#info-count'), // Инфа о выбранных файлах
		sizeInfo: $('#info-size'),
		/**
		 * Initialize uploader plugin
		 */
		initUploader: function() {
			t.photo.fileInput.damnUploader({
				url: '/userCard/photo', // куда отправлять
				fieldName:  'photo', // имитация имени поля с файлом (будет ключом в $_FILES, если используется PHP)
				dropBox: t.photo.dropBox, // дополнительно: элемент, на который можно перетащить файлы (либо объект jQuery, либо селектор)
				limit: 5, // максимальное кол-во выбранных файлов (если не указано - без ограничений)
				// когда максимальное кол-во достигнуто (вызывается при каждой попытке добавить еще файлы)
				onLimitExceeded: function() {
					t.noty.show('error', 'Выбрано максимальное количество картинок!');
				},
				// ручная обработка события выбора файла (в случае, если выбрано несколько, будет вызвано для каждого)
				// если обработчик возвращает true, файлы добавляются в очередь автоматически
				onSelect: function(file) {
					t.photo.addFileToQueue(file);
					return false;
				},
				// когда все загружены
				onAllComplete: function() {
//					t.noty.show('success', 'Всме файлы успешно загружены!');
					$('#global-loader').hide();
					location.reload();
				}
			});
		},
		/**
		 * Add img to upload queue
		 */
		addFileToQueue: function(file) {
			$('.img-container-info').remove();
			// Создаем элемент li и помещаем в него название, миниатюру и progress bar
			var li = $('<li/>');
			var title = $('<div/>').text(file.name+' ').appendTo(li);
			var cancelButton = $('<a/>').attr({
				href: '#cancel',
				title: 'отменить'
			}).text('x').appendTo(title);

			// Если браузер поддерживает выбор файлов (иначе передается специальный параметр fake,
			// обозночающий, что переданный параметр на самом деле лишь имитация настоящего File)
			if(!file.fake) {
				if (!t.photo.__prepareFile(file, li)) {
					return null;
				}
			} else {
				console.log('Файл добавлен: ' + file.name);
			}

			li.appendTo(t.photo.imgList);
			t.photo.imgCount++;
//			updateInfo();
			var uploadItem = t.photo.__createUploadObject(file);
			// ... и помещаем его в очередь
			var queueId = t.photo.fileInput.damnUploader('addItem', uploadItem);
			t.photo.__cancelHandler(file, queueId, cancelButton, li);

			return uploadItem;
		},

		/**
		 *
		 */
		__prepareFile: function(file, $element) {
			// Отсеиваем не картинки
			var imageType = /image.*/;
			if (!file.type.match(imageType)) {
				t.noty.show('error', 'Файл ' + file.name + ' был удален, т.к. он не является картинкой!')
//				console.log('Файл отсеян: `'+file.name+'` (тип '+file.type+')');
				return false;
			}

			// Добавляем картинку и прогрессбар в текущий элемент списка
			var img = $('<img/>').appendTo($element);

			// Создаем объект FileReader и по завершении чтения файла, отображаем миниатюру и обновляем
			// инфу обо всех файлах (только в браузерах, подерживающих FileReader)
			if($.support.fileReading) {
				var reader = new FileReader();
				reader.onload = (function(aImg) {
					return function(e) {
						aImg.attr('src', e.target.result);
						aImg.attr('width', 150);
						aImg.attr('id', file.name.substr(0, 20));
					};
				})(img);
				reader.readAsDataURL(file);
			}

//			console.log('Картинка добавлена: `'+file.name + '` (' +Math.round(file.size / 1024) + ' Кб)');
			t.photo.imgSize += file.size;
			return true;
		},

		/**
		 * @param file
		 * @returns {{file: *, onProgress: Function, onComplete: Function}}
		 * @private
		 */
		__createUploadObject: function(file) {
			// Создаем объект загрузки
			return {
				file: file,
				onProgress: function(percents) {
//					updateProgress(pBar, percents);
				},
				onComplete: function(successfully, data, errorCode) {
					data = JSON.parse(data);

					if(successfully && data.result) {
						if (!data.message) {
							data.message = 'Файл ' + this.file.name + ' успешно загружен!';
						}
						t.noty.show('success', data.message);
					} else {
						if(!this.cancelled) {
							if (!data.message) {
								data.message = 'При загрузке файла ' + this.file.name + ' возникла ошибка.';
								if (errorCode) {
									data.message = data.message + ' Код: ' + errorCode;
								}
							}
							t.noty.show('error', data.message);
						}
					}
				}
			};
		},

		/**
		 * Вешаем обработчик на кнопку отмены загрузки картинки
		 * @param file
		 * @param queueId
		 * @param $cancelButton
		 * @param $element
		 * @private
		 */
		__cancelHandler: function(file, queueId, $cancelButton, $element) {
			// обработчик нажатия ссылки "отмена"
			$cancelButton.click(function() {
				t.photo.fileInput.damnUploader('cancel', queueId);
				$element.remove();
				t.photo.imgCount--;
				t.photo.imgSize -= file.fake ? 0 : file.size;
//				updateInfo();
				t.noty.show('information', 'Файл ' + file.name + ' удален из очереди!');
				return false;
			});
		},

		__bindEvents: function() {
			// Обработка событий drag and drop при перетаскивании файлов на элемент dropBox
			t.photo.dropBox.bind({
				dragenter: function() {
					$(this).addClass('highlighted');
					return false;
				},
				dragover: function() {
					return false;
				},
				dragleave: function() {
					$(this).removeClass('highlighted');
					return false;
				}
			});

			// Обаботка события нажатия на кнопку "Загрузить все". Стартуем все загрузки
			$('#upload-all').click(function() {
				if (t.photo.imgCount) {
					t.photo.fileInput.damnUploader('startUpload');
					$('#global-loader').hide().show();
				} else {
					t.noty.show('warning', 'Добавьте фотографии.');
				}
			});

			// Обработка события нажатия на кнопку "Отменить все"
			$('#cancel-all').click( function() {
				t.photo.fileInput.damnUploader('cancelAll');
				t.photo.imgCount = 0;
				t.photo.imgSize = 0;
//				updateInfo();
				t.noty.show('information', '*** Все загрузки отменены ***');
				t.photo.imgList.empty();
			});

		},

		deletePhoto: function() {
			$(document).on('click', '.photo-control', function(e) {
				var $this = $(this);
				t.noty.confirm('Удалить фотографию?', 'center', function() {
					$.ajax({
						url: '/userCard/deletePhoto',
						dataType: 'json',
						type: 'POST',
						data: { photoId: $this.data('id') },
						success: function(data, textStatus, jqXHR) {
							if (data.result) {
								T.noty.show('success', data.message);
								location.reload();
							} else {
								T.noty.show('error', data.message);
							}
						},
						error: function( jqXHR, textStatus, errorThrown ) {
							var data = JSON.parse(jqXHR.responseText);
							T.noty.show('error', data.message);
						}
					});
				});
			});
		},

		init: function () {
			t.photo.initUploader();
			t.photo.__bindEvents();
			t.photo.deletePhoto();
		}
	};

	t.photo.init();

	return t;
}(T || {}));


