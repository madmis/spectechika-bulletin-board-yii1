var T = (function (t) {
	"use strict";

	t.api = {
		/**
		 * Global loading on ajax
		 */
		globalLoader: function() {
		$('#global-loader')
			.hide()  // hide it initially
			.ajaxStart(function() {
				$(this).show();
			})
			.ajaxStop(function() {
				$(this).hide();
			});
		},

		registerEvent: function() {
			$(document).on("change",'#User_city_id', function(e) {
				$.ajax({
					url: '/city/getRegion',
					dataType: 'json',
					type: 'GET',
					data: { id: e.val },
					success: function(data, textStatus, jqXHR) {
						if (data.result && data.result.id) {
							$('#User_region_id [value="'+ data.result.id +'"]')
								.attr("selected", "selected");
						}
					},
					error: function( jqXHR, textStatus, errorThrown ) {
						var data = JSON.parse(jqXHR.responseText);
						T.noty.show('error', data.message);
					}
				});

			});
		},

		selectCity: function(parentSelector) {
			return { // instead of writing the function to execute the request we use Select2's convenient helper
				url: '/city/clientCities/',
				dataType: 'json',
				data: function (term, page) {
					return {
						p: $(parentSelector + ' :selected').val(), // search term
						q: term
					};
				},
				results: function (data, page) { // parse the results into the format expected by Select2.
//					console.log(data);
					// since we are using custom formatting functions we do not need to alter remote JSON data
					return {results: data};
				}
			}
		}
	};


	t.api.globalLoader();
	t.api.registerEvent();

	return t;
}(T || {}));


