$(function () {
	// отображаем подсказки
	$("[rel=tooltip]").tooltip();
	$("[rel=popover]").popover();

	if ($('.masked-phone').length) {
		$('.masked-phone').mask($('.masked-phone').data('mask'), {placeholder: ' '});
	}

	$('.js-tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	})
});