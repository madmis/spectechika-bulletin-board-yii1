var T = (function (t) {
	'use strict';

	t.profile = {
		cityReplace: function(url, uriEncodedQuery) {
			url = url.replace('%QUERY', uriEncodedQuery);
			var parent = $('#User_region_id :selected').val();

			if (parent) {
				url = url + '?parent=' + parent;
			}
			console.log(url);
			return url;
		}
	};

	return t;
}(T || {}));


