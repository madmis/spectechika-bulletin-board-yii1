var T = (function (t) {
	"use strict";

	t.noty = {
		/**
		 * @param type alert|success|error|warning|information|confirmation
		 * @param message
		 */
		show: function(type, message) {
			noty({
				text: message,
				layout: 'topRight',
				type: type,
				closeWith: ['click', 'button'],
                timeout: 10000
			});
		},

		/**
		 * @param {string} message
		 * @param {string} position (topRight|center)
		 * @param {function} okCallback
		 * @param {function} cancelCallback
		 */
		confirm: function(message, position, okCallback, cancelCallback) {
			if (!position) {
				position = 'center';
			}
			noty({
				text: message,
				layout: position,
				closeWith: ['click', 'button'],
				type: 'confirmation',
				modal: true,
				buttons: [
					{ addClass: 'btn btn-primary', text: 'Ok',
						onClick: function($noty) {
							if ( okCallback instanceof Function ) {
								okCallback.call();
							}

							$noty.close();
						}
					},
					{ addClass: 'btn btn-danger', text: 'Cancel',
						onClick: function($noty) {
							if ( cancelCallback instanceof Function ) {
								cancelCallback.call();
							}

							$noty.close();
						}
					}
				]
			});

		},

        /**
         * Preload function
         */
        run: function() {
        }
	};

    t.noty.run();

	return t;
}(T || {}));
