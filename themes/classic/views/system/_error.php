<?php
/**
 * @var $this TController
 * @var $error CAttributeCollection
 */
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="language" content="ru"/>
	<meta name="robots" content="none"/>
	<!--  SEO STUFF END -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<title>Page Not Found</title>
	<?php Yii::app()->clientScript->registerCssFile('//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile($this->getAssetsBase() . '/css/error.css'); ?>

	<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
	<?php Yii::app()->clientScript->registerScriptFile('//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js', CClientScript::POS_END); ?>

	<?php Yii::app()->clientScript->registerLinkTag('icon', 'image/png', $this->getAssetsBase() . '/img/favicon.png'); ?>
	<?php Yii::app()->clientScript->registerLinkTag('shortcut icon', 'image/x-icon', $this->getAssetsBase() . '/img/favicon.ico'); ?>
</head>
<body>
<div class="row-fluid">
	<div class="http-error">
		<div class="span2"></div>
		<div class="span2">
			<div class="error-caption"><p><?php echo $error->code; ?></p></div>
		</div>
		<div class="span6">
			<div class="error-message">
				<p><?php echo $error->message; ?></p>
				<p class="return-home">
					<a href="<?php echo Yii::app()->getBaseUrl(true); ?>">Перейти на главную страницу</a>
				</p>
				<p class="return-home">
					<a href="<?php echo Yii::app()->request->getUrlReferrer(); ?>">Вернуться на	предыдущую страницу</a>
				</p>
			</div>
		</div>
		<div class="span2"></div>
	</div>
</div>
<div class="clearfix"></div>
<div class="error-bottom"></div>
</body>
</html>