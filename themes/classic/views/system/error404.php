<?php
/**
 * @var $this TController
 * @var $error CAttributeCollection
 */
$error->add('message', 'Запрошенная страница не найдена на сервере...');
$this->renderPartial('//system/_error', array('error' => $error));