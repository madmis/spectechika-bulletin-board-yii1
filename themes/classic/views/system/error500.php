<?php
/**
 * @var $this TController
 * @var $error CAttributeCollection
 */
$error->add('message', 'В ходе обработки вашего запроса произошла внутренняя ошибка сервера');
$this->renderPartial('//system/_error', array('error' => $error));