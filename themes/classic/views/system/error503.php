<?php
/**
 * @var $this TController
 * @var $error CAttributeCollection
 */
$error->add('message', 'В настоящий момент производится техническое обслуживание системы. Возвращайтесь позже.<br>Спасибо.');
$this->renderPartial('//system/_error', array('error' => $error));