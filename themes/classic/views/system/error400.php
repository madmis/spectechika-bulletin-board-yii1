<?php
/**
 * @var $this TController
 * @var $error CAttributeCollection
 */
$error->add('message', 'В запросе обнаружена синтаксическая ошибка, по причине которой сервер не смог обработать этот запрос. Следует исправить ошибку, после чего повторить запрос.');
$this->renderPartial('//system/_error', array('error' => $error));