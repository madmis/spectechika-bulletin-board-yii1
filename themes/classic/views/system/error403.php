<?php
/**
 * @var $this TController
 * @var $error CAttributeCollection
 */
$error->add('message', 'У вас недостаточно прав для доступа к запрошенной странице');
$this->renderPartial('//system/_error', array('error' => $error));
