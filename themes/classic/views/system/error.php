<?php
/**
 * @var $this TController
 * @var $error CAttributeCollection
 */
$this->renderPartial('//system/_error', array('error' => $error));