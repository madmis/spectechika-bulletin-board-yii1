<?php
/**
 * @var $this TController
 * @var $sort CSort
 * @var $totalItemCount int
 */
$price = 'price';
if ($this->id == 'rent') {
	$price = 'max_price';
}

?>
<div class="row order-block">
	<div class="span4">
		<p class="founded">
			<?php echo Yii::t('main', 'Найдено'); ?> <?php echo Yii::t('cldr', '{n} объявление|{n} объявления|{n} объявлений', $totalItemCount);?>
		</p>
	</div>
	<div class="span8">
		<div class="pull-right">
			<noindex>
			<?php $order = Yii::app()->request->getQuery('sort', 'create_at.desc'); ?>
			<?php echo Yii::t('main', 'Выводить'); ?>:&nbsp;&nbsp;
			<?php if (strpos($order, 'create_at') !== false): ?>
				<span class="label label-info">
					<?php echo $sort->link('create_at', Yii::t('main', 'по дате'), array(
						'rel' => 'nofollow',
					)); ?>
				</span>
				<?php if ($order == 'create_at'): ?>
					<i class="icon-sort-by-alphabet"></i>
				<?php else: ?>
					<i class="icon-sort-by-alphabet-alt"></i>
				<?php endif; ?>
			<?php else: ?>
				<span><?php echo $sort->link('create_at', Yii::t('main', 'по дате')); ?></span>
			<?php endif; ?>
			&nbsp;|&nbsp;
			<?php if (strpos($order, $price) !== false): ?>
				<span class="label label-info">
					<?php echo $sort->link($price, Yii::t('main', 'по цене'), array(
						'rel' => 'nofollow',
					)); ?>
				</span>
				<?php if ($order == $price): ?>
					<i class="icon-sort-by-order"></i>
				<?php else: ?>
					<i class="icon-sort-by-order-alt"></i>
				<?php endif; ?>
			<?php else: ?>
				<span><?php echo $sort->link($price, Yii::t('main', 'по цене')); ?></span>
			<?php endif; ?>
			&nbsp;|
			</noindex>
		</div>
	</div>
</div>
