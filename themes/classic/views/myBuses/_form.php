<?php
/* @var $this MyBusesController */
/* @var $model Buses */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'buses-form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnChange' => true,
		'validateOnSubmit' => true,
	),
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'class' => 'form-horizontal',
	),
)); ?>

<div class="row-fluid">
	<div class="span5">
		<div class="control-group">
			<?php echo $form->labelEx($model, 'photo', array('class' => 'control-label')); ?>
			<div class="controls">
				<?php $this->widget('application.widgets.TImageInput.TImageInput', array(
					'model' => $model,
					'attribute' => 'photo',
					'previewSelector' => 'ul#img-list',
					'previewSize' => Buses::PHOTO_SMALL_WIDTH,
					'htmlOptions' => array(
						'placeholder' => $model->getAttributeLabel('photo'),
						'class' => 'span12'
					),
				)); ?>
			</div>
			<?php echo $form->error($model, 'photo', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
		</div>
	</div>
	<div class="span4">
		<ul id="img-list">
			<?php if (!$model->isNewRecord && !empty($model->photo)): ?>
				<li><?php //echo CHtml::image($model->getSmallPhoto()); ?></li>
			<?php endif; ?>
		</ul>
	</div>
</div>


<div class="control-group">
	<?php echo $form->labelEx($model, 'category_id', array(
		'class' => 'control-label'
	)); ?>
	<div class="controls">
		<?php echo $form->dropDownList(
			$model,
			'category_id',
			Category::model()->groupedIndexedList(),
			array(
				'empty' => 'Выберите категорию',
				'options'
			)
		); ?>
		<?php echo $form->error($model, 'category_id', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'price', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php echo $form->textField($model, 'price', array(
			'class' => 'span1',
		)); ?>
		<?php echo $form->error($model, 'price', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'features', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php echo $form->textArea($model, 'features', array(
			'class' => 'span5',
			'rows' => 6,
		)); ?>
		<?php echo $form->error($model, 'features', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'description', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php echo $form->textArea($model, 'description', array(
			'class' => 'span5',
			'rows' => 8,
		)); ?>
		<?php echo $form->error($model, 'description', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>
<div class="control-group">
	<div class="controls">
		<?php echo CHtml::submitButton(Yii::t('app', 'Сохранить'), array('class' => 'btn')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
