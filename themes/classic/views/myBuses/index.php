<?php
/* @var $this MyBusesController */
/* @var $items Buses[] */

$this->pageTitle = 'Моя спецтехника';
$this->breadcrumbs = array(
	'Моя спецтехника',
);
?>
	<h3 class="title">
		Моя спецтехника
	<span class="pull-right">
		<?php echo CHtml::link(
			'<i class="icon-plus-sign"></i> добавить',
			array('myBuses/create')
		); ?>
	</span>
	</h3>


<?php foreach ($items as $item): ?>
	<?php /** @var $item Buses */ ?>
	<div class="cta">
		<div class="container">
			<div class="media my-buses-list">
				<a class="pull-left thumbnail" href="#" style="width: <?php echo Buses::PHOTO_SMALL_WIDTH ?>px;">
					<?php echo CHtml::image($item->getPhotoUrl(Buses::PHOTO_SMALL_PREFIX)); ?>
				</a>

				<div class="media-body">
					<h4 class="media-heading"><?php echo $item->category->name; ?></h4>
					<div class="my-buses-content">
						<span class="my-buses-label"><?php echo $item->getAttributeLabel('price'); ?>: </span><?php echo $item->price; ?> грн.
					</div>
					<div class="my-buses-content">
						<span class="my-buses-label"><?php echo $item->getAttributeLabel('features'); ?></span>
						<div class="my-buses-inline"><?php echo nl2br($item->features); ?></div>
					</div>
					<div class="my-buses-content">
						<span class="my-buses-label"><?php echo $item->getAttributeLabel('description'); ?></span>
						<div class="my-buses-inline"><?php echo nl2br($item->description); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php endforeach; ?>