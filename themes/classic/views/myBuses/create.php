<?php
/* @var $this MyBusesController */
/* @var $model Buses */
$this->pageTitle = 'Моя спецтехника - Добавление';
$this->breadcrumbs = array(
	'Моя спецтехника' => array('index'),
	'Добавление',
);
?>

<h4 class="title">Добавление спецтехники</h4>

<?php $this->renderPartial('_form', array(
	'model' => $model,
)); ?>