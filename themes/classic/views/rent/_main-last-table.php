<?php
/**
 * @var $this TController
 * @var $items Rent[]
 *
 */
$model = Rent::model();
?>
<?php if ($items): ?>
	<div class="row-fluid">
		<div class="span12">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><?php echo $model->getAttributeLabel('title'); ?></th>
						<th><?php echo $model->getAttributeLabel('category_id'); ?></th>
						<th><?php echo $model->getAttributeLabel('max_price'); ?></th>
						<th><?php echo $model->getAttributeLabel('region_id'); ?></th>
						<th><?php echo $model->getAttributeLabel('create_at'); ?></th>
						<th>Добавлено заявок</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($items as $item): ?>
					<tr>
						<td>
							<?php echo CHtml::link(
								$item->title,
								"/rent/{$item->id}",
								array('title' => "Аренда {$item->category->name_r}")
							); ?>
						</td>
						<td>
							<?php echo CHtml::link($item->category->name, "/toRent/category/{$item->category->translit}",
								array('title' => "{$item->getAttributeLabel('category_id')}: {$item->category->name}")
							); ?>
						</td>
						<td>
							<?php echo Yii::app()->numberFormatter->formatCurrency(
								$item->max_price, Yii::app()->params->itemAt('currency')
							); ?>
						</td>
						<td>
							<?php echo CHtml::link($item->region->name, "/toRent/region/{$item->region->translit}",
								array('title' => "{$item->getAttributeLabel('region_id')}: {$item->region->name}")
							); ?>
						</td>
						<td>
							<?php echo Yii::app()->format->formatDatetime($item->create_at); ?>
						</td>
						<td>
							<?php echo count($item->rentBs); ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
<?php endif; ?>