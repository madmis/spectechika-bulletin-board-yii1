<?php
/**
 * @var $this RentController
 * @var $model Rent
 */
$this->pageTitle = $model->title;
$this->setPageDescription(TMeta::rentDescription($model));
$this->setPageKeywords(TMeta::rentKeywords($model));

$this->breadcrumbs = array(
	Yii::t('main', 'Возьму в аренду') => array('/rent'),
	$model->category->name,
);
?>

	<div class="margin-bottom-10"></div>
	<div class="rblock rent blog">
		<?php $this->renderPartial(
			'/rent/full-item',
			array('model' => $model)
		); ?>
	</div>

<?php if (Yii::app()->user->isGuest): ?>
	<div class="alert alert-block">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4>Обратите внимание!</h4>
		Добавление заявок к объявлению доступно только зарегистрированным пользователям, у которых в профиле
		представлена как минимум одна единица искомой спецтехники.
		Что является подтверждением того, что вы владеете указанной техникой и готовы предоставить ее в аренду.
		<?php $this->widget('application.widgets.TAuth.TAuth', array('action' => Yii::app()->user->getLoginUrl())); ?>
	</div>
<?php endif; ?>

	<div class="rblock to-rent blog">
		<div class="meta big">
			<?php if ($model->status == 0): ?>
				<span title="Добавить заявку" class="bid-add-link">
					<i class="icon-plus-sign"></i>
					<?php if (Yii::app()->user->getIsGuest()): ?>
						<span title="Для добавления заявки необходимо авторизоваться">Добавить заявку</span>
					<?php else: ?>
						<?php $userId = Yii::app()->user->getId(); ?>
						<?php if (RentBid::model()->countUserBid($userId, $model->id)): ?>
							<span title="Вы уже добавили заявку к этому объявлению">Заявка добавлена</span>
						<?php else: ?>
							<?php echo CHtml::link('Добавить заявку', array('/rent/addBid'), array('id' => 'add-bid-link')); ?>
						<?php endif; ?>
					<?php endif; ?>
				</span>
			<?php endif; ?>
			<span title="Добавлено заявок">
				<i class="icon-file"></i> Добавлено заявок: <?php echo count($model->rentBs); ?>
			</span>
			<span class="pull-right">
				<?php if (Yii::app()->user->id === $model->user_id && $model->status == 0): ?>
					<span title="Заказ будет закрыт без выбора исполнителя" class="bid-close-link">
						<i class="icon-remove-sign"></i>
						<?php echo CHtml::link('Закрыть заказ', array('/rent/setStatus/', 'id'=>$model->id, 'status'=>2)); ?>
					</span>
				<?php endif; ?>

				<span title="Теги">
					<i class="icon-tags"></i>
					<?php echo CHtml::link($model->category->name, "/rent/category/{$model->category->translit}",
						array('title' => "{$model->getAttributeLabel('category_id')}: {$model->category->name}")
					); ?>
					<?php echo CHtml::link($model->region->name, "/rent/region/{$model->region->translit}",
						array('title' => "{$model->getAttributeLabel('region_id')}: {$model->region->name}")
					); ?>
					<?php if ($model->city_id): ?>
						<?php echo CHtml::link($model->city->name, "/rent/city/{$model->city->id}",
							array('title' => "{$model->getAttributeLabel('city_id')}: {$model->city->name}")
						); ?>
					<?php endif; ?>
				</span>
			</span>
		</div>
		<div class="clearfix"></div>
	</div>
<?php if (!Yii::app()->user->isGuest): ?>
	<div class="row hide" id="add-bid">
		<div class="span12">
			<?php $this->renderPartial('//rentBid/_form', array('model' => new RentBid(), 'rent' => $model)); ?>
		</div>
	</div>
<?php endif; ?>
<?php /** @var $model Rent */ ?>
<legend>Заявки пользователей</legend>
<?php $this->renderPartial('//rentBid/view', array('items' => $model->rentBs, 'rent' => $model)); ?>