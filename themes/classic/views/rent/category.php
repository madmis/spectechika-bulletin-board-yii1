<?php
/* @var $this RentController */
/* @var $provider CActiveDataProvider */
/* @var $category Category */
$this->pageTitle = str_replace('%PREFIX%', 'Поиск предложений по аренде', $category->title);
$this->setPageDescription(str_replace('%PREFIX%', 'Поиск предложений по аренде', $category->meta_description));
$this->setPageKeywords($category->meta_keywords);

$this->breadcrumbs = array(
	Yii::t('main', 'Возьму в аренду спецтехнику') => array('/rent'),
	$category->name
);
?>
<div class="row">
	<div class="span12">
		<?php echo $this->renderPartial('//search/search-block'); ?>
	</div>
</div>

<div class="row">
	<div class="span12 special-description">
		<h1 class="small"><?php echo $category->name; ?></h1>
		<?php echo TMeta::prepare(
			$category->description,
			array('%APP_NAME%' => Yii::app()->name)
		); ?>
	</div>
</div>

<?php $this->beginContent('//decorators/order-block', array(
	'totalItemCount' => $provider->totalItemCount,
	'sort' => $provider->getSort(),
))?>
<?php $this->endContent()?>

<div class="text-divider5"><span></span></div>

<div class="rblock rent blog">
	<?php foreach($provider->getData() as $item): ?>
		<?php $this->renderPartial('//rent/short-item', array('model' => $item)); ?>
	<?php endforeach; ?>
</div>

<?php $this->widget('CLinkPager', array(
	'pages' => $provider->getPagination(),
	'maxButtonCount' => 7,
	'cssFile' => false,
	'header' => '<div class="pagination pagination-centered pagination-large">',
	'footer' => '</div>',
	'hiddenPageCssClass' => 'disabled',
	'selectedPageCssClass' => 'active',
	'firstPageLabel' => '&laquo;&laquo;',
	'prevPageLabel' => '&laquo;',
	'nextPageLabel' => '&raquo;',
	'lastPageLabel' => '&raquo;&raquo;',
)) ?>