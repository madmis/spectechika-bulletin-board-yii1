<?php
/**
 * @var $this RentController
 * @var $model Rent
 */
?>
<div class="row rent">
	<div class="span12">
		<div class="rinfo">


			<div class="row">
				<div class="span6">
					<h2 class="media-heading"><?php echo $model->title; ?></h2>
					<p class="item">
						<i class="icon-calendar"></i>
						<span class="name"><?php echo $model->getAttributeLabel('create_at'); ?>:</span>
						<span class="value">
							<?php echo Yii::app()->dateFormatter->formatDateTime($model->create_at, 'full', 'short'); ?>
						</span>
					</p>

					<p class="item">
						<i class="icon-tags"></i>
						<span class="name"><?php echo $model->getAttributeLabel('category_id'); ?>:</span>
						<span class="value">
							<?php echo CHtml::link($model->category->name, "/rent/category/{$model->category->translit}",
								array('title' => "{$model->getAttributeLabel('category_id')}: {$model->category->name}")
							); ?>
						</span>
					</p>

					<p class="item">
						<i class="icon-globe"></i>
						<span class="name"><?php echo $model->getAttributeLabel('region_id'); ?>:</span>
						<span class="value">
							<?php echo CHtml::link($model->region->name, "/rent/region/{$model->region->translit}",
								array('title' => "{$model->getAttributeLabel('region_id')}: {$model->region->name}")
							); ?>
						</span>
						<?php if ($model->city_id): ?>
							<?php /** @var $model Rent */ ?>
							&bull;
							<span class="value">
								<?php echo CHtml::link($model->city->name, "/rent/city/{$model->city->id}",
									array('title' => "{$model->getAttributeLabel('city_id')}: {$model->city->name}")
								); ?>
							</span>
						<?php endif; ?>
					</p>

					<p class="item">
						<i class="icon-money"></i>
						<span class="name"><?php echo $model->getAttributeLabel('max_price'); ?>:</span>
						<span class="value">
							<?php echo Yii::app()->numberFormatter->formatCurrency($model->max_price, Yii::app()->params->itemAt('currency')); ?>
						</span>
					</p>

				</div>
				<div class="span4 sidebar pull-right">
					<div class="widget">
						<h4>Контактная информация</h4>

						<p class="item">
							<i class="icon-user"></i>
							<span class="name"><?php echo $model->getAttributeLabel('fio'); ?>:</span>
							<span class="value"><?php echo $model->fio; ?></span>
						</p>
						<?php if ($model->company_name): ?>
							<p class="item">
								<i class="icon-building"></i>
								<span class="name"><?php echo $model->getAttributeLabel('company_name'); ?>:</span>
								<span class="value"><?php echo $model->company_name; ?></span>
							</p>
						<?php endif; ?>
						<?php /** @var Rent $model */ ?>
						<p class="item status">
							<i class="icon-bell-alt"></i><?php echo $model->getAttributeLabel('status'); ?>:&nbsp;
							<span class="status-<?php echo $model->status; ?>"><?php echo $model->getStatusDescription(); ?></span>
						</p>

<!--						<p class="item">-->
<!--							<i class="icon-phone"></i>-->
<!--							<span class="name">--><?php //echo $model->getAttributeLabel('phone'); ?><!--:</span>-->
<!--							<span class="value">--><?php //echo $model->phone; ?><!--</span>-->
<!--						</p>-->
					</div>

					<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
					<div class="yashare-auto-init" data-yashareL10n="ru"
						 data-yashareType="none"
						 data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug,gplus,surfingbird">
					</div>
				</div>
			</div>

			<p class="item">
				<span class="heading"><?php echo $model->getAttributeLabel('description'); ?></span>
				<span class=""><?php echo nl2br($model->description); ?></span>
			</p>
		</div>
	</div>
</div>