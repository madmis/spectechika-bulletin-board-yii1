<?php
/* @var $this ToRentController */
/* @var $provider CActiveDataProvider */
/* @var $category Category */
/* @var $region Region */
$this->pageTitle = 'Поиск предложений аренды срецтехники';
$this->setPageDescription('Поиск предложений аренды срецтехники');

$this->breadcrumbs = array(
	Yii::t('main', 'Предложения аренды спецтехники') => array('/rent'),
	Yii::t('main', 'Поиск'),
);
?>
<div class="row">
	<div class="span12">
		<?php echo $this->renderPartial('//search/search-block'); ?>
	</div>
</div>

<?php $this->beginContent('//decorators/order-block', array(
	'totalItemCount' => $provider->totalItemCount,
	'sort' => $provider->getSort(),
))?>
<?php $this->endContent()?>

<div class="text-divider5"><span></span></div>

<div class="rblock rent blog">
	<?php foreach($provider->getData() as $item): ?>
		<?php $this->renderPartial('//rent/short-item', array('model' => $item)); ?>
	<?php endforeach; ?>
</div>
<?php $this->widget('CLinkPager', array(
	'pages' => $provider->getPagination(),
	'maxButtonCount' => 7,
	'cssFile' => false,
	'header' => '<div class="pagination pagination-centered pagination-large">',
	'footer' => '</div>',
	'hiddenPageCssClass' => 'disabled',
	'selectedPageCssClass' => 'active',
	'firstPageLabel' => '&laquo;&laquo;',
	'prevPageLabel' => '&laquo;',
	'nextPageLabel' => '&raquo;',
	'lastPageLabel' => '&raquo;&raquo;',
)) ?>