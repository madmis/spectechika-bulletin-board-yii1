<?php
/* @var $this RentController */
/* @var $model Rent */
?>

<div class="row">
	<div class="span9">
		<div class="rinfo">
			<!-- Name -->
			<h4 class="media-heading">
				<?php echo CHtml::link(
					$model->title,
					"/rent/{$model->id}",
					array('title' => "Аренда {$model->category->name_r}")
				); ?>
			</h4>

			<p class="item">
				<i class="icon-tags"></i>
				<span class="name"><?php echo $model->getAttributeLabel('category_id'); ?>:</span>
					<span class="value">
						<?php echo CHtml::link($model->category->name, "/rent/category/{$model->category->translit}",
							array('title' => "{$model->getAttributeLabel('category_id')}: {$model->category->name}")
						); ?>
					</span>
			</p>

			<p class="item">
				<i class="icon-globe"></i>
				<span class="name"><?php echo $model->getAttributeLabel('region_id'); ?>:</span>
					<span class="value">
						<?php echo CHtml::link($model->region->name, "/rent/region/{$model->region->translit}",
							array('title' => "{$model->getAttributeLabel('region_id')}: {$model->region->name}")
						); ?>
					</span>
				<?php if ($model->city_id): ?>
					<?php /** @var $model ToRent */ ?>
					&bull;
					<span class="value">
							<?php echo CHtml::link($model->city->name, "/rent/city/{$model->city->id}",
								array('title' => "{$model->getAttributeLabel('city_id')}: {$model->city->name}")
							); ?>
						</span>
				<?php endif; ?>
			</p>

			<p class="item">
				<i class="icon-money"></i>
				<span class="name"><?php echo $model->getAttributeLabel('max_price'); ?>:</span>
				<span class="value">
					<?php echo Yii::app()->numberFormatter->formatCurrency(
						$model->max_price, Yii::app()->params->itemAt('currency')
					); ?>
				</span>
			</p>

			<p class="item">
				<span class="heading"><?php echo $model->getAttributeLabel('description'); ?></span>
				<span class=""><?php echo nl2br($model->description); ?></span>
			</p>

		</div>
	</div>
	<div class="span3">
		<div class="meta">
			<div class="created">
				<i class="icon-calendar"></i><?php echo $model->getAttributeLabel('create_at'); ?>: <?php echo Yii::app()->format->formatDatetime($model->create_at); ?>
			</div>
			<?php /** @var Rent $model */ ?>
			<div class="status">
				<i class="icon-bell-alt"></i><?php echo $model->getAttributeLabel('status'); ?>:&nbsp;
				<span class="status-<?php echo $model->status; ?>"><?php echo $model->getStatusDescription(); ?></span>
			</div>
		</div>
	</div>
</div>
<div class="meta">
	<span title="Дата добавления">
		<i class="icon-calendar"></i>
		<?php echo Yii::app()->dateFormatter->formatDateTime($model->create_at, 'full', 'short'); ?>
	</span>
	<span title="Автор">
		<i class="icon-user"></i> <?php echo $model->fio; ?>
	</span>
	<span title="Добавлено заявок">
		<i class="icon-file"></i> Добавлено заявок: <?php echo count($model->rentBs); ?>
	</span>

	<!--	<span title="Количество просмотров">-->
	<!--		<i class="icon-eye-open"></i> 32-->
	<!--	</span>-->
	<span class="pull-right">
		<span title="продробная информация">
			<i class="icon-link"></i>
			<?php echo CHtml::link('продробная информация', "/rent/{$model->id}", array('title' => 'продробная информация')); ?>
		</span>
<!--		<span title="Комментарии">-->
<!--			<i class="icon-comment"></i> <a href="#">2 комментария</a>-->
<!--		</span>-->
	</span>
</div>
<div class="member-divider"></div>

