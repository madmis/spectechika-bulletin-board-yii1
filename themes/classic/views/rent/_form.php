<?php
/* @var $this RentController */
/* @var $model Rent */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScriptFile($this->assetsBase . '/js/maskedinput/jquery.maskedinput.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($this->assetsBase . '/js/profile.t.js', CClientScript::POS_END);
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>'rent-form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnChange' => true,
		'validateOnSubmit' => true,
	),
	'errorMessageCssClass' => 'error',
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'class' => 'form-horizontal big-label',
	),
)); ?>

<div class="control-group">
	<?php echo $form->labelEx($model, 'title', array(
		'class' => 'control-label'
	)); ?>
	<div class="controls">
		<?php echo $form->textField($model, 'title', array(
			'maxlength'=>255,
			'class' => 'span4',
			'placeholder' => $model->getAttributeLabel('title'),
		)); ?>
		<?php echo $form->error($model, 'title', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'category_id', array(
		'class' => 'control-label'
	)); ?>
	<div class="controls">
		<?php echo $form->dropDownList($model, 'category_id',
			Category::model()->groupedIndexedList(),
			array('empty' => 'Выберите категорию', 'class' => 'span4')
		); ?>
		<?php echo $form->error($model, 'category_id', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'region_id', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php echo $form->dropDownList($model, 'region_id',
			Region::model()->getList(),
			array('empty' => 'Выберите регион', 'class' => 'span4')
		); ?>
		<?php echo $form->error($model, 'region_id', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'city_id', array('class' => 'control-label')); ?>
	<div class="controls">
		<div class="input-append">
			<?php $this->widget('application.widgets.TTypeAhead.TTypeAhead', array(
				'model' => $model,
				'attribute' => 'city_id',
				'name' => 'Rent[city_id]',
				'value' => (!empty($model->city) ? $model->city->name : ''),
				'htmlOptions' => array(
					/* @var $model Rent */
					'placeholder' => $model->getAttributeLabel('city_id'),
				),
				'options' => array(
					'minLength' => 3,
					'limit' => 10,
					'name' => 'city_id',
					'remote' => array(
						'url' => $this->createUrl('city/clientCitiesOld/%QUERY'),
						'replace' => 'js:function(url, uriEncodedQuery) { return T.profile.cityReplace(url, uriEncodedQuery); } ',
					),
				)
			));?>
			<span id="tp" class="add-on" rel="tooltip"
				  title="При заполненнии города работает автоподстановка.
				  Т.е., когда вы вводите название города, вам предлагаются похожие варианты и нужно лишь выбрать необходимый.
				  Если выбран регон, предлагаются только города выбранного региона.">
				<i class="icon-question"></i>
			</span>
		</div>
		<?php echo $form->error($model, 'city_id', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group',
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'max_price', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php echo $form->textField($model, 'max_price', array(
			'class' => 'span2',
			'maxlength'=>11,
			'placeholder' => $model->getAttributeLabel('max_price'),
		)); ?>
		<?php echo $form->error($model, 'max_price', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group',
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'description', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php echo $form->textArea($model, 'description', array(
			'class' => 'span5',
			'rows' => 8,
			'placeholder' => $model->getAttributeLabel('description'),
		)); ?>
		<?php echo $form->error($model, 'description', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<h4 class="title">Контактные данные</h4>

<div class="control-group">
	<?php echo $form->labelEx($model, 'fio', array('class' => 'control-label')); ?>
	<div class="controls">
		<div class="input-prepend">
			<span class="add-on"><i class="icon-user"></i></span>
			<?php echo $form->textField($model, 'fio', array(
				'class' => 'span4',
				'maxlength'=>255,
				'placeholder' => $model->getAttributeLabel('fio'),
			)); ?>
		</div>
		<?php echo $form->error($model, 'fio', array('class' => 'help-inline', 'inputContainer' => 'div.control-group')); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'company_name', array('class' => 'control-label')); ?>
	<div class="controls">
		<div class="input-prepend">
			<span class="add-on"><i class="icon-building"></i></span>
			<?php echo $form->textField($model, 'company_name', array(
				'class' => 'span4',
				'maxlength'=>255,
				'placeholder' => $model->getAttributeLabel('company_name'),
			)); ?>
		</div>
		<?php echo $form->error($model, 'company_name', array('class' => 'help-inline', 'inputContainer' => 'div.control-group')); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'phone', array('class' => 'control-label')); ?>
	<div class="controls">
		<div class="input-prepend">
			<span class="add-on"><i class="icon-phone-sign"></i></span>
			<?php echo $form->textField($model, 'phone', array(
				'maxlength' => 19,
				'class' => 'span4 masked-phone',
				'data-mask' => Yii::app()->params->itemAt('phoneMask'),
				'placeholder' => $model->getAttributeLabel('phone'),
			));?>
		</div>
		<?php echo $form->error($model, 'phone', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<?php echo CHtml::submitButton(Yii::t('app', 'Сохранить'), array('class' => 'btn')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>