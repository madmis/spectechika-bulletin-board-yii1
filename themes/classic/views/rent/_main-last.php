<?php
/**
 * @var $this TController
 * @var $items Rent[]
 *
 */
?>

<?php if ($items): ?>
	<div class="row-fluid">
		<div class="span12">
			<h4 class="title">Возьму в аренду - последние предложения</h4>
		</div>
	</div>
		<div class="row-fluid">
		<?php foreach($items as $item): ?>
			<div class="span3">
				<h5>
					<?php echo CHtml::link(
						$item->title,
						"/rent/{$item->id}",
						array('title' => "Аренда {$item->category->name_r}")
					); ?>
				</h5>
				<p class="item">
					<i class="icon-calendar"></i>
					<span class="name"><?php echo $item->getAttributeLabel('create_at'); ?>:</span>
					<span class="value">
						<?php echo Yii::app()->format->formatDatetime($item->create_at); ?>
					</span>
				</p>

				<p class="item">
					<i class="icon-tags"></i>
					<span class="name"><?php echo $item->getAttributeLabel('category_id'); ?>:</span>
					<span class="value">
						<?php echo CHtml::link($item->category->name, "/rent/category/{$item->category->translit}",
							array('title' => "{$item->getAttributeLabel('category_id')}: {$item->category->name}")
						); ?>
					</span>
				</p>

				<p class="item">
					<i class="icon-money"></i>
					<span class="name"><?php echo $item->getAttributeLabel('max_price'); ?>:</span>
					<span class="value">
						<?php echo Yii::app()->numberFormatter->formatCurrency(
							$item->max_price, Yii::app()->params->itemAt('currency')
						); ?>
					</span>
				</p>
				<p class="item">
					<i class="icon-globe"></i>
					<span class="name"><?php echo $item->getAttributeLabel('region_id'); ?>:</span>
					<span class="value">
						<?php echo CHtml::link($item->region->name, "/toRent/region/{$item->region->translit}",
							array('title' => "{$item->getAttributeLabel('region_id')}: {$item->region->name}")
						); ?>
					</span>
				</p>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>