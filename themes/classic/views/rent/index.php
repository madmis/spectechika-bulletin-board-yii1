<?php
/* @var $this RentController */
/* @var $provider CActiveDataProvider */
$this->pageTitle = "Возьму в аренду спецтехнику в Киеве, Хорькове, Донецке. Возьму в аренду экскаватор, самосвал, погрузчик лил другую спецтехнику.";
$this->setPageDescription(
	"Возьму в аренду спецтехнику в Киеве, Хорькове, Донецке. Возьму в аренду экскаватор, самосвал, погрузчик лил другую спецтехнику."
);
$this->setPageKeywords(
	'аренда спецтехники, возьму в аренду, возьму в аренду автомобиль, возьму в аренду экскаватор, возьму в аренду самосвалы,
	возьму кран в аренду, возьму самосвал в аренду, возьму в аренду полуприцеп'
);

$this->breadcrumbs = array(
	Yii::t('main', 'Возьму в аренду'),
);
?>

<div class="row">
	<div class="span12">
		<?php echo $this->renderPartial('//search/search-block'); ?>
	</div>
</div>

<div class="row">
	<div class="span12 special-description">
		<h1 class="small">Возьму в аренду спецтехнику</h1>
		<p>
			<strong>Аренда техники и спецтехники</strong> — одна из наиболее выгодных форм использования техники.
			Но поиск и подбор спецтехники, это не самое легкое занятие.
			<strong>Арендуя спецтехнику</strong> у неизветсного вам владельца, вы рискуете столкнуться с техническими неисправностями в процессе выполнения работ.
		</p>
		<p>
			На портале <strong>"<?php echo Yii::app()->name ?>"</strong> вы можете подать объявление о необходимости взять в аренду спецтехнику.
			После этого, владельцы спецтехники сами будут искать вас, а вы, в свою очередь, будете иметь возможность выбрать наиболее подходящего кандидата.
			После сотрудничества с владельцем спецтехники, вы можете оставить о нем отзыв и повлиять на его рейтинг.
		</p>
	</div>
</div>

<?php $this->beginContent('//decorators/order-block', array(
	'totalItemCount' => $provider->totalItemCount,
	'sort' => $provider->getSort(),
))?>
<?php $this->endContent()?>

<div class="text-divider5"><span></span></div>

<div class="rblock rent blog">
	<?php foreach($provider->getData() as $item): ?>
		<?php $this->renderPartial('//rent/short-item', array('model' => $item)); ?>
	<?php endforeach; ?>
</div>

<?php $this->widget('CLinkPager', array(
	'pages' => $provider->getPagination(),
	'maxButtonCount' => 7,
	'cssFile' => false,
	'header' => '<div class="pagination pagination-centered pagination-large">',
	'footer' => '</div>',
	'hiddenPageCssClass' => 'disabled',
	'selectedPageCssClass' => 'active',
	'firstPageLabel' => '&laquo;&laquo;',
	'prevPageLabel' => '&laquo;',
	'nextPageLabel' => '&raquo;',
	'lastPageLabel' => '&raquo;&raquo;',
)) ?>