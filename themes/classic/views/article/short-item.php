<?php
/**
 * @var ArticleController $this
 * @var $model Article
 */
?>
<div class="entry">
	<h2><?php echo CHtml::link($model->title, array("/article/$model->translit")); ?></h2>

	<div class="meta">
		<i class="icon-calendar"></i>&nbsp;
		<?php echo Yii::app()->dateFormatter->formatDateTime($model->create_at, 'full', null); ?>
		<span class="pull-right"><i class="icon-user"></i>&nbsp;<?php echo $model->user->username; ?></span>
	</div>

	<div class="content"><?php echo $model->snippet; ?></div>
</div>
<div class="row">
	<div class="span4 pull-right">
		<div class="button pull-right"><?php echo CHtml::link('читать дальше...', array("/article/$model->translit")); ?></div>
	</div>
</div>
