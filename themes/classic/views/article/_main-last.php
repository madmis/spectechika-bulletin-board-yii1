<?php
/**
 * @var $this TController
 * @var $items Article[]
 */
?>
<div class="row-fluid">
	<div class="span12">
		<h4 class="title">Статьи</h4>
	</div>
</div>
<?php foreach ($items as $item): ?>
	<div class="row-fluid">
		<div class="span12">
			<h3><?php echo CHtml::link($item->title, array("/article/$item->translit")); ?></h3>

			<p class="item">
				<i class="icon-calendar"></i>
				<span class="name"><?php echo $item->getAttributeLabel('create_at'); ?>:</span>
				<span class="value">
					<?php echo Yii::app()->dateFormatter->formatDateTime($item->create_at, 'full', 'short'); ?>
				</span>
			</p>

			<div class="content"><?php echo $item->snippet; ?></div>
		</div>
	</div>
<?php endforeach; ?>
