<?php
/**
 * @var $this ArticleController
 * @var $provider CActiveDataProvider
 */
$this->pageTitle = "Статьи о спецтехнике, аренда спецтехники в Киеве и регионах";
$this->setPageDescription('Статьи о спецтехнике, аренда спецтехники в Киеве и регионах');
$this->setPageKeywords("статьи о спецтехнике, аренда спецтехники статьи, взять в аренду спецтехнику");

$this->breadcrumbs = array(Yii::t('main', 'Статьи о спецтехнике'));
?>

	<div class="row">
		<div class="span12 special-description">
			<h1 class="medium">Статьи о спецтехнике</h1>
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="span12">
			<div class="article blog posts">
				<?php foreach ($provider->getData() as $item): ?>
					<?php $this->renderPartial('//article/short-item', array('model' => $item)); ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

<?php $this->widget('CLinkPager', array(
	'pages' => $provider->getPagination(),
	'maxButtonCount' => 7,
	'cssFile' => false,
	'header' => '<div class="pagination pagination-centered pagination-large">',
	'footer' => '</div>',
	'hiddenPageCssClass' => 'disabled',
	'selectedPageCssClass' => 'active',
	'firstPageLabel' => '&laquo;&laquo;',
	'prevPageLabel' => '&laquo;',
	'nextPageLabel' => '&raquo;',
	'lastPageLabel' => '&raquo;&raquo;',
)) ?>