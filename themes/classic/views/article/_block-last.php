<?php
/**
 * @var $this ArticleController
 * @var $items Article[]
 */
?>
<?php if ($items): ?>
<div class="widget">
	<h4>Последние статьи</h4>
	<ul>
		<?php foreach ($items as $item): ?>
			<li><?php echo CHtml::link(
					$item->title,
					array("/article/{$item->translit}"),
					array('title' => $item->title)
				); ?></li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>