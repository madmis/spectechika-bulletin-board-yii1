<?php
/**
 * @var $this ArticleController
 * @var $model Article
 */
?>

<?php $this->renderPartial('_form', array('model' => $model)); ?>