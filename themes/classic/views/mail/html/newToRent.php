<?php
/**
 * @var $this TController
 * @var $ToRent ToRent
 */
?>
<h2>В раздел "Сдам в аренду" добавлено новое объявление</h2>
<br><br>
<?php if (!Yii::app()->user->isGuest): ?>
	User: <b>Registered</b><br>
	<b>User ID:</b> <?php echo Yii::app()->user->id; ?><br>
	<b>User name:</b> <?php echo Yii::app()->user->name; ?><br>
<?php else: ?>
	User: <b>Unregistered</b><br>
<?php endif; ?>
<br><br>
<b>Id:</b> <?php echo $ToRent->id; ?><br>
<b>Category:</b> <?php echo $ToRent->category->name; ?><br>
<b>Region:</b> <?php echo $ToRent->region->name; ?><br>
<b>Price:</b> <?php echo $ToRent->price; ?><br>
<b>FIO:</b> <?php echo $ToRent->fio; ?><br>
<?php if ($ToRent->company_name): ?>
	<b>Company name:</b> <?php echo $ToRent->company_name; ?><br>
<?php endif; ?>
<b>Дата:</b> <?php echo Yii::app()->format->formatDatetime($ToRent->create_at? $ToRent->create_at : 'now'); ?><br>
<br>
<b>Features:</b><br>
<?php echo nl2br($ToRent->features); ?><br>
<br>
<b>Description:</b><br>
<?php echo nl2br($ToRent->description); ?><br>

