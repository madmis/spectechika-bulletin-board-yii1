<?php
/**
 * @var $this TController
 * @var $Rent Rent
 */
?>
<h2>В раздел "Возьму в аренду" добавлено новое объявление</h2>
<br><br>
<?php if (!Yii::app()->user->isGuest): ?>
	User: <b>Registered</b><br>
	<b>User ID:</b> <?php echo Yii::app()->user->id; ?><br>
	<b>User name:</b> <?php echo Yii::app()->user->name; ?><br>
<?php else: ?>
	User: <b>Unregistered</b><br>
<?php endif; ?>
<br><br>
<b>Id:</b> <?php echo $Rent->id; ?><br>
<b>Category:</b> <?php echo $Rent->category->name; ?><br>
<b>Region:</b> <?php echo $Rent->region->name; ?><br>
<b>Max price:</b> <?php echo $Rent->max_price; ?><br>
<b>FIO:</b> <?php echo $Rent->fio; ?><br>
<?php if ($Rent->company_name): ?>
	<b>Company name:</b> <?php echo $Rent->company_name; ?><br>
<?php endif; ?>
<b>Дата:</b> <?php echo Yii::app()->format->formatDatetime($Rent->create_at? $Rent->create_at : 'now'); ?><br>
<br>
<b>Description:</b><br>
<?php echo nl2br($Rent->description); ?><br>

