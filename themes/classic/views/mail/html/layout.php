<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><?php echo $title ?></title>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"
	  style="width: 100% !important;-webkit-text-size-adjust: none;margin: 0;padding: 0;">
<center>
	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"
		   style="margin: 0;padding: 0;background-color: #dee0e2;height: 100% !important;width: 100% !important;">
		<tbody>
		<tr>
			<td align="center" valign="top"
				style="border-collapse: collapse;padding-bottom: 40px;">
				<table border="0" cellpadding="0" cellspacing="0"
					   width="729">
					<tbody>
					<tr>
						<td align="center" valign="top"
							style="border-collapse: collapse;padding-bottom: 0px;">
							<table border="0" cellpadding="0" cellspacing="0" width="729"
								   style="background-color: #f8f8f8;border-top: 5px solid #2773ae;">
								<tbody>
								<tr>
									<td align="center" valign="top" style="border-collapse: collapse;padding-top: 0px;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tbody>
											<tr>
												<td bgcolor="#fff" style="border-collapse: collapse;color: #505050;font-family: Helvetica;font-size: 18px;
													font-weight: bold;line-height: 100%;padding: 20px 40px;text-align: left;vertical-align: middle;border-bottom: 1px solid #ddd;">
													<?php /*
														$path = Yii::getPathOfAlias('application.assets.img.logo');
														$url = Yii::app()->getAssetManager()->getPublishedUrl( $path . '/logo.png');
														$url = Yii::app()->createAbsoluteUrl($url);
														<img src="<?php echo $url; ?>"
															 alt="СпецАвто - доска бесплатных объявлений аренды спецтехники"
															 style="border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;">

 													*/?>

													<table style="float: left; width: 350px;">
														<tr>
															<td style="font-size: 40px; padding-bottom: 20px;">
																<span style="color: #666666;">Спец</span><span style="color: #4f99bc;">Авто</span>
															</td>
														</tr>
														<tr>
															<td style="font-size: 12px; font-weight: normal; font-style: italic; color: #666666;">
																<?php echo Yii::app()->params->itemAt('slogan'); ?>
															</td>
														</tr>
													</table>

													<ul style="list-style: none;display: block;float: right;">
														<li style="float: left;font-size: 14px;font-weight: normal;padding: 0 4px;">
															<?php echo CHtml::link(
																'Главная сайта',
																Yii::app()->createAbsoluteUrl(Yii::app()->urlManager->baseUrl),
																array(
																	'target' => '__blank',
																	'style' => 'color: #2773ae;text-decoration: underline;font-weight: bold;font-family: Georgia;font-size: 12px;',
																)
															); ?>
														</li>
														<li style="float: left;font-size: 14px;font-weight: normal;padding: 0 4px;">|</li>
														<li style="float: left;font-size: 14px;font-weight: normal;padding: 0 4px;">
															<?php echo CHtml::link(
																'Контакты',
																Yii::app()->createAbsoluteUrl('/contact'),
																array(
																	'target' => '__blank',
																	'style' => 'color: #2773ae;text-decoration: underline;font-weight: bold;font-family: Georgia;font-size: 12px;',
																)
															); ?>
														</li>
													</ul>
												</td>
											</tr>
											<tr>
												<td style="border-collapse: collapse;color: #505050;font-family: Georgia;
													font-size: 14px;font-weight: normal;line-height: 100%;padding: 20px 40px;
													text-align: left;vertical-align: middle;">
													<?php echo $content ?>
												</td>
											</tr>
											<tr>
												<td style="border-collapse: collapse;">
													<table border="0" cellpadding="0" cellspacing="0" width="729"
														   style="background-color: #f8f8f8;border-bottom: 10px solid #2773ae;border-top: 1px solid #ddd;">
														<tbody>
														<tr>
															<td align="center" valign="top" style="border-collapse: collapse;padding: 0 40px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tbody>
																	<tr>
																		<td valign="top"
																			style="border-collapse: collapse;color: #3F3A38;font-family: Georgia;
																			font-size: 12px;line-height: 150%;text-align: center;padding-top: 20px;">
																			<div>
																				<div>
<!--																					<div class="street-address">35 Miller Ave #192</div>-->
<!--																					<span class="locality">Mill Valley</span>,-->
<!--																					<span class="region">California</span>-->
<!--																					<span class="postal-code">94941</span>-->
																					<div><?php /* echo Yii::app()->getParam('mainPhone'); */ ?></div>
																				</div>
																				<br>
																			<em>&copy; <?php echo date('Y'); ?> <?php echo CHtml::encode(Yii::app()->name); ?></em>
																		</td>
																	</tr>
																	<?php if (!empty($unsubscribeLink)): ?>
																	<tr>
																		<td valign="top"
																			style="border-collapse: collapse;color: #3F3A38;font-family: Georgia;
																			font-size: 12px;line-height: 150%;text-align: center;padding-top: 10px;">
																			<p>
																				<a href="<?php echo $unsubscribeLink; ?>"
																				  style="color: #eb4102;font-weight: normal;
																				  text-decoration: underline;">Unsubscribe from this list</a><!-- | <a href="/">Update subscription preferences</a> -->
																			</p>
																		</td>
																	</tr>
																	<?php endif; ?>
																	</tbody>
																</table>
															</td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</center>
</body>
</html>