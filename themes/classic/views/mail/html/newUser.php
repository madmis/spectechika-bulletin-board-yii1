<?php
/**
 * @var $this TController
 * @var $User User
 */
?>
<h2>На сайте зарегистрирован новый пользователь</h2>
<p>
	<b>Id:</b> <?php echo $User->id; ?><br>
	<b>Имя:</b> <?php echo $User->username; ?><br>
	<b>Дата:</b> <?php echo Yii::app()->format->formatDatetime($User->create_at); ?>
	<?php foreach ($User->userServices as $service): ?>
		<?php /** @var $service UserService */ ?>
		<b>Service:</b> <?php echo $service->service; ?><br>
		<b>Identity:</b> <?php echo $service->identity; ?><br>
	<?php endforeach; ?>
</p>