<?php
/**
 * @var $this TController
 * @var $Rent Rent
 */
?>
В раздел "Возьму в аренду" добавлено новое объявление<?php echo PHP_EOL; ?>
<?php if (!Yii::app()->user->isGuest): ?>
User: Registered<?php echo PHP_EOL; ?>
User ID: <?php echo Yii::app()->user->id . PHP_EOL; ?>
User name: <?php echo Yii::app()->user->name . PHP_EOL; ?>
<?php else: ?>
User: Unregistered<?php echo PHP_EOL; ?>
<?php endif; ?>

Id: <?php echo $Rent->id . PHP_EOL; ?>
Category: <?php echo $Rent->category->name . PHP_EOL; ?>
Region: <?php echo $Rent->region->name . PHP_EOL; ?>
Max price: <?php echo $Rent->max_price . PHP_EOL; ?>
FIO: <?php echo $Rent->fio . PHP_EOL; ?>
<?php if ($Rent->company_name): ?>
Company name: <?php echo $Rent->company_name . PHP_EOL; ?>
<?php endif; ?>
Дата: <?php echo Yii::app()->format->formatDatetime($Rent->create_at? $Rent->create_at : 'now') . PHP_EOL; ?>
Description:<?php echo PHP_EOL; ?>
<?php echo $Rent->description . PHP_EOL; ?>

