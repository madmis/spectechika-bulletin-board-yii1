<?php
/**
 * @var $this TController
 * @var $ToRent ToRent
 */
?>
В раздел "Сдам в аренду" добавлено новое объявление<?php echo PHP_EOL; ?>
<?php if (!Yii::app()->user->isGuest): ?>
User: Registered<?php echo PHP_EOL; ?>
User ID: <?php echo Yii::app()->user->id . PHP_EOL; ?>
User name: <?php echo Yii::app()->user->name . PHP_EOL; ?>
<?php else: ?>
User: Unregistered<?php echo PHP_EOL; ?>
<?php endif; ?>

Id: <?php echo $ToRent->id . PHP_EOL; ?>
Category: <?php echo $ToRent->category->name . PHP_EOL; ?>
Region: <?php echo $ToRent->region->name . PHP_EOL; ?>
Price: <?php echo $ToRent->price . PHP_EOL; ?>
FIO: <?php echo $ToRent->fio . PHP_EOL; ?>
<?php if ($ToRent->company_name): ?>
Company name: <?php echo $ToRent->company_name . PHP_EOL; ?>
<?php endif; ?>
Дата: <?php echo Yii::app()->format->formatDatetime($ToRent->create_at? $ToRent->create_at : 'now') . PHP_EOL; ?>
<?php echo PHP_EOL; ?>
Features:<?php echo PHP_EOL; ?>
<?php echo $ToRent->features . PHP_EOL; ?>
<?php echo PHP_EOL; ?>
Description:<?php echo PHP_EOL; ?>
<?php echo $ToRent->description . PHP_EOL; ?>

