<?php
/**
 * @var $this TController
 * @var $User User
 */
?>
На сайте зарегистрирован новый пользователь<?php echo PHP_EOL; ?>
Id: <?php echo $User->id . PHP_EOL; ?>
Имя: <?php echo $User->username . PHP_EOL; ?>
Дата: <?php echo Yii::app()->format->formatDatetime($User->create_at) . PHP_EOL; ?>
<?php foreach ($User->userServices as $service): ?>
<?php /** @var $service UserService */ ?>
Service: <?php echo $service->service. PHP_EOL; ?>
Identity: <?php echo $service->identity. PHP_EOL; ?>
<?php endforeach; ?>
