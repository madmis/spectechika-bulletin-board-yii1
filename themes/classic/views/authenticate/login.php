<?php
/* @var $this AuthenticateController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
if ($this->route == 'authenticate/login') {
	$this->pageTitle=Yii::app()->name . ' - ' . Yii::t('main', 'Авторизация');
	$this->breadcrumbs = array(Yii::t('main', 'Авторизация'));
}
Yii::app()->clientScript->registerCssFile($this->getAssetsBase() . '/js/passfield/passfield.min.css');
Yii::app()->clientScript->registerScriptFile($this->getAssetsBase() . '/js/passfield/passfield.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($this->getAssetsBase() . '/js/pass.t.js', CClientScript::POS_END);
?>

<div class="row">
	<div class="span6">
		<h3 class="title"><span class="color"><?php echo Yii::t('main', 'Зарегистрируйтесь сейчас'); ?></span></h3>
		<h4>Особенности регистрации</h4>
		<p>
			Зарегистрироваться можно посредством ввода email-адреса или при помощи своего профиля в социальных сетях.
			<span class="color" style="font-weight: bold;">После регистрации посредством email-адреса мы рекомендуем привязать к своему аккаунту один из профилей социальных сетей.</span>
			В этом случае, даже если вы забудете свой пароль, вы всегда сможете авторизоваться через социальную сеть и восстановить пароль.
		</p>
		<h5>Преимущества регистрации</h5>
		<ul>
			<li>Заполнение данных своего профиля</li>
			<li>Добавление списка собственной техники</li>
			<li>Возиожность автозаполнения данных при добавлении объявлений</li>
			<li>Возможность подписки на оповещения</li>
		</ul>
	</div>

	<div class="span6">
		<div class="formy well">
			<h4 class="title"><?php echo Yii::t('main', 'Авторизуйтесь или зарегистрируйтесь'); ?></h4>
			<div class="form">

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'login-form',
					'action'=>$this->createUrl('authenticate/login'),
					'enableClientValidation'=>true,
					'clientOptions'=>array(
						'validateOnSubmit'=>true,
					),
					'htmlOptions' => array(
						'class' => 'form-horizontal',
					),
				)); ?>
				<div class="control-group">
					<?php echo $form->labelEx($model, 'email', array('class' => 'control-label')); ?>
					<div class="controls">
						<?php echo $form->textField($model, 'email', array(
							'placeholder' => $model->getAttributeLabel('email'),
							'class' => 'span4'
						)); ?>
						<?php echo $form->error($model, 'email', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
					</div>
				</div>
				<div class="control-group">
					<?php echo $form->labelEx($model, 'password', array('class' => 'control-label')); ?>
					<div class="controls">
						<?php echo $form->passwordField($model, 'password', array(
							'placeholder' => $model->getAttributeLabel('password'),
							'class' => 'span4'
						)); ?>
						<?php echo $form->error($model, 'password', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
					</div>
				</div>
				<div class="form-actions">
					<?php echo CHtml::submitButton('Войти', array('class' => 'btn btn-primary', 'name' => 'login')); ?>
					<?php echo CHtml::submitButton('Зарегистрироваться', array('class' => 'btn', 'name' => 'register')); ?>
				</div>
				<?php $this->endWidget(); ?>
			</div>

			<hr>
			<h5>Войти через:</h5>
			<?php $this->widget('ext.eauth.EAuthWidget', array('action' => Yii::app()->user->getLoginUrl())); ?>
		</div>
	</div>
</div>
