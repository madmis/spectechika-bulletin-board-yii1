<?php /* @var $this TController */ ?>
<div class="container">
	<div class="row">

		<div class="span4">
			<div class="logo">
				<?php
				$title = Yii::app()->name . ' - ' . Yii::app()->params->itemAt('slogan');
				$img = CHtml::image(
					"{$this->getAssetsBase()}/img/logo/logo.png",
					$title,
					array('title' => $title)
				);
				?>
				<?php echo CHtml::link($img, Yii::app()->request->hostInfo, array('title' => $title)); ?>
			</div>
		</div>

		<?php if (Yii::app()->user->getIsGuest()): ?>
			<div class="span6">
				<?php $this->renderPartial('//elements/topMenu'); ?>
			</div>
			<div class="span2">
				<?php $this->widget('application.widgets.TAuth.TAuth', array('action' => Yii::app()->user->getLoginUrl())); ?>
			</div>
		<?php else: ?>
			<div class="span8">
				<?php $this->renderPartial('//elements/topMenu'); ?>
			</div>
		<?php endif; ?>
	</div>
</div>
