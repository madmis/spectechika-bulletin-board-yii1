<div class="container">
	<?php if(Yii::app()->user->hasFlash('success')):?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?php echo Yii::app()->user->getFlash('success'); ?>
		</div>
	<?php endif; ?>
	<?php if(Yii::app()->user->hasFlash('error')):?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?php echo Yii::app()->user->getFlash('error'); ?>
		</div>
	<?php endif; ?>
	<?php if(Yii::app()->user->hasFlash('info')):?>
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?php echo Yii::app()->user->getFlash('info'); ?>
		</div>
	<?php endif; ?>
	<?php if(Yii::app()->user->hasFlash('warning')):?>
		<div class="alert alert-warning">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?php echo Yii::app()->user->getFlash('warning'); ?>
		</div>
	<?php endif; ?>
</div>