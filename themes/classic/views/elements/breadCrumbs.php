<?php /* @var $this TController */ ?>
<?php if (!empty($this->breadcrumbs)): ?>
	<div class="page-head">
		<div class="container">
			<div class="row">
				<div class="span12">
					<?php $this->widget('zii.widgets.CBreadcrumbs', array('links' => $this->breadcrumbs)); ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
