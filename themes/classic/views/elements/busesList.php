<?php
/** @var TController $this */
/** @var Buses[] $buses */
?>

<div class="row buses-list">
	<div class="span12">
		<h4 class="title">Добавить на основании "Моей спецтехники"</h4>
	</div>
	<?php foreach ($buses as $item): ?>
		<div class="span3 buses-list-item"
			 onclick="location.href='<?php echo $this->createUrl('/toRent/add', array('b'=>$item->id)); ?>'">
			<div class="thumbnail" style="min-height: <?php echo Buses::PHOTO_MINI_HEIGHT + 30; ?>px;">
					<?php echo CHtml::image(
						$item->getPhotoUrl(Buses::PHOTO_MINI_PREFIX)
					); ?>
				<h6 class="center"><?php echo $item->category->name; ?></h6>
			</div>
		</div>
	<?php endforeach; ?>
</div>
<div class="clear margin-bottom-20"></div>
