<?php /* @var $this TController */ ?>
<div class="container">
	<div class="row">
		<div class="span12">

			<div class="row">

				<div class="span4">
					<div class="widget">
						<h5><?php echo Yii::t('main', 'Контакты'); ?></h5>
						<hr>
						<p>
							<?php echo Yii::app()->name; ?> - это, рейтинговый каталог лучших предложений по аренде спецтехники Украины
						</p>
						<hr>
						<i class="icon-home"></i> &nbsp; Украина, г.Киев, ул. Комсомольская 23
<!--						<hr>-->
<!--						<i class="icon-phone"></i> &nbsp; +380 (68) 376-55-62-->
						<hr>
						<i class="icon-envelope-alt"></i> &nbsp; <?php echo Yii::app()->params->itemAt('contactEmail'); ?>
<?php /*
						<div class="social">
							<a href="#"><i class="icon-facebook facebook"></i></a>
							<a href="#"><i class="icon-twitter twitter"></i></a>
							<a href="#"><i class="icon-linkedin linkedin"></i></a>
							<a href="#"><i class="icon-google-plus google-plus"></i></a>
						</div>
*/ ?>
					</div>
				</div>

				<div class="span4">
					<div class="widget">
						<h5><?php echo Yii::t('main', 'Наши проекты'); ?></h5>
						<hr>
						<div class="two-col">
							<div class="col-left">
								<ul>
									<li>
										<i class="icon-vk"></i>&nbsp;&nbsp;&nbsp;
										<a href="http://vk.com/spec_auto" target="_blank" title="СпецАвто во Вконтакте">СпецАвто во Вконтакте</a>
									</li>
									<li>
										<i class="icon-plus-sign-alt"></i>&nbsp;&nbsp;&nbsp;
										<a href="http://my-doc.com.ua/" target="_blank" title="Мой доктор – сервис по поиску и подбору врачей в Киеве.">Рейтинг врачей Киева</a>
									</li>
								</ul>
							</div>
							<div class="col-right">
								<ul>
									<li>
										<i class="icon-facebook"></i>&nbsp;&nbsp;&nbsp;
										<a href="https://www.facebook.com/specauto.com.ua" target="_blank" title="СпецАвто в Facebook">СпецАвто в Facebook</a>
									</li>
								</ul>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>

				<div class="span4">
					<div class="widget">
						<h5><?php echo Yii::t('main', 'Меню'); ?></h5>
						<hr>
						<div class="tweet">
							<?php echo $this->renderPartial('//elements/footerMenu'); ?>

						</div>
					</div>
				</div>
			</div>

			<hr>
			<p class="copy">
				&copy; <?php echo date('Y'); ?>&nbsp;|&nbsp;
				Использование любых материалов, размещённых на сайте, разрешается только при условии установки обратной ссылки на <?php echo TUrl::extractDomain(Yii::app()->request->hostInfo); ?>
			</p>
		</div>
	</div>
	<div class="clearfix"></div>
</div>