<?php
/* @var $this TController */
?>
<div class="navbar">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<div class="nav-collapse collapse">
				<?php $this->widget('zii.widgets.CMenu',array(
					'encodeLabel'=>false,
					'id' => 'main-menu',
					'items'=>array(
						array(
							'label'=>Yii::t('main', 'Главная'),
							'url'=>Yii::app()->request->hostInfo,
							'linkOptions' => array('title'=>Yii::t('main', 'Главная страница сервиса')),
							'itemOptions' => array('class' => 'single'),
							'active' => $this->route == 'site/index',
						),
						array(
							'label'=>Yii::t('main', 'Сдам в аренду&nbsp;<b class="caret"></b>'),
							'url'=>$this->createUrl('/toRent'),
							'linkOptions' => array(
								'title'=>Yii::t('main', "Предложения по аренде спецтехники"),
								'class' => 'dropdown-toggle',
								'data-toggle' => 'dropdown',
							),
							'itemOptions' => array('class' => 'dropdown'),
							'submenuOptions' => array('class' => 'dropdown-menu'),
							'items' => array(
								array(
									'label'=>Yii::t('main', 'Добавить объявление'),
									'url'=>$this->createUrl('/toRent/add'),
								),
								array(
									'label'=>Yii::t('main', 'Объявления'),
									'url'=>$this->createUrl('/toRent'),
								),
							),
						),
						array(
							'label'=>Yii::t('main', 'Возьму в аренду&nbsp;<b class="caret"></b>'),
							'url'=>$this->createUrl('/rent'),
							'linkOptions' => array(
								'title'=>Yii::t('main', "Поиск спецтехники для аренды"),
								'class' => 'dropdown-toggle',
								'data-toggle' => 'dropdown',
							),
							'itemOptions' => array('class' => 'dropdown'),
							'submenuOptions' => array('class' => 'dropdown-menu'),
							'items' => array(
								array(
									'label'=>Yii::t('main', 'Добавить объявление'),
									'url'=>$this->createUrl('/rent/add'),
								),
								array(
									'label'=>Yii::t('main', 'Объявления'),
									'url'=>$this->createUrl('/rent'),
								),
							),
						),
						array(
							'label'=>Yii::t('main', 'Статьи'),
							'url'=>$this->createUrl('/article'),
							'linkOptions' => array('title'=>Yii::t('main', 'Статьи о спецтехнике')),
							'itemOptions' => array('class' => 'single'),
							'active' => $this->route == 'article/index',
						),
						array(
							'label'=>Yii::t('main', 'Faq'),
							'url'=>$this->createUrl('/faq'),
							'linkOptions' => array('title'=>Yii::t('main', 'Часто задаваемые вопросы (FAQ)')),
							'itemOptions' => array('class' => 'single'),
							'active' => !empty($this->actionParams['view']) && $this->actionParams['view'] == 'faq',
						),
					),
					'htmlOptions' => array('class' => 'nav nav-pills main-menu'),
					'submenuHtmlOptions' => array('class' => 'dropdown-menu'),
				)); ?>
			</div>

			<?php if (!Yii::app()->user->isGuest): ?>
				<?php $this->widget('zii.widgets.CMenu', array(
					'encodeLabel' => false,
					'items' => array(
						array(
							'label' => Yii::app()->user->name . '&nbsp;&nbsp;<b class="caret"></b>',
							'url' => array('#'),
							'itemOptions' => array('class' => 'dropdown'),
							'linkOptions' => array(
								'class' => 'dropdown-toggle',
								'data-toggle' => 'dropdown',
							),
							'submenuOptions' => array('class' => 'dropdown-menu'),
							'items' => array(
								array(
									'label' => '<i class="icon-wrench"></i>&nbsp;'  . Yii::t('main', 'Настройки'),
									'url' => array('/profile')
								),
								array(
									'label' => '<i class="icon-truck"></i>&nbsp;'  . Yii::t('main', 'Моя спецтехника'),
									'url' => array('/myBuses')
								),
								array('label' => '', 'itemOptions' => array('class' => 'divider')),
								array(
									'label' => '<i class="icon-signout"></i>&nbsp;' . Yii::t('main', 'Выход'),
									'url' => $this->createUrl('/authenticate/logout'),
								),
							),
						),
					),
					'htmlOptions' => array('class' => 'nav pull-right'),
				)); ?>
			<?php endif; ?>
		</div>
	</div>
</div>

