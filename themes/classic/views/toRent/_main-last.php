<?php
/**
 * @var $this TController
 * @var $items ToRent[]
 *
 */
?>
<?php if ($items): ?>
	<div class="row-fluid">
		<div class="span12">
			<h4 class="title">Сдам в аренду - последние предложения</h4>
		</div>
	</div>
		<div class="row-fluid">
		<?php foreach($items as $item): ?>
			<div class="span3">
				<h5>
					<?php echo CHtml::link(
						$item->category->name,
						"/toRent/{$item->id}",
						array('title' => "Аренда {$item->category->name_r}")
					); ?>
				</h5>
				<div class="row-fluid">
					<div class="span3">
						<?php echo CHtml::link(
							CHtml::image(
								$item->getPhotoUrl(ToRent::PHOTO_MINI_PREFIX),
								$item->category->name,
								array('title' => $item->category->name)
							),
							"/toRent/{$item->id}",
							array(
								'title' => $item->category->name, 'class' => 'thumbnail',
								'style' => 'width:'.ToRent::PHOTO_MINI_WIDTH.'px;'
							)
						); ?>
					</div>
					<div class="span9">
						<p class="item">
							<i class="icon-calendar"></i>
							<span class="name"><?php echo $item->getAttributeLabel('create_at'); ?>:</span>
							<span class="value">
								<?php echo Yii::app()->format->formatDatetime($item->create_at); ?>
							</span>
						</p>
						<p class="item">
							<i class="icon-money"></i>
							<span class="name"><?php echo $item->getAttributeLabel('price'); ?>:</span>
							<span class="value">
								<?php echo Yii::app()->numberFormatter->formatCurrency(
									$item->price, Yii::app()->params->itemAt('currency')
								); ?>
							</span>
						</p>
						<p class="item">
							<i class="icon-globe"></i>
							<span class="name"><?php echo $item->getAttributeLabel('region_id'); ?>:</span>
							<span class="value">
								<?php echo CHtml::link($item->region->name, "/toRent/region/{$item->region->translit}",
									array('title' => "{$item->getAttributeLabel('region_id')}: {$item->region->name}")
								); ?>
							</span>
						</p>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>