<?php
/* @var $this ToRentController */
/* @var $model ToRent */
/* @var $buses Buses[] */
/* @var $buse Buses */
$this->pageTitle = 'Сдам в аренду - Добавление объявления';
$this->setPageDescription(
	"Добавить объявление о аренде спецтехники."
);
$this->setPageKeywords(
	'аренда спецтехники, сдать спецтехнику в аренду, аренда спецтехники киев, аренда спецтехники харьков, аренда спецтехники донецк,
	аренда самосвала, аренда экскаватора, аренда погрузчика, предложение по аренде спецтехники'
);
$this->breadcrumbs = array(
	'Добавление предложения по аренде спецтехники',
);
?>

<?php if (!Yii::app()->user->isGuest && !empty($buses)): ?>
	<?php $this->renderPartial('//elements/busesList', array(
		'buses' => $buses,
	)); ?>
<?php endif; ?>

<h4 class="title">Предложение по аренде спецтехники</h4>

<?php $this->renderPartial('_form', array(
	'model' => $model,
	'buse' => $buse,
)); ?>