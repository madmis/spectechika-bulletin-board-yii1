<?php
/* @var $this ToRentController */
/* @var $provider CActiveDataProvider */
$this->pageTitle = "Аренда спецтехники в Киеве, Хорькове, Донецке. Аренда самосвала, экскаватора, погрузчика и другой техники.";
$this->setPageDescription(
	"Аренда спецтехники в Киеве, Хорькове, Донецке. Аренда самосвала, экскаватора, погрузчика и другой техники."
);
$this->setPageKeywords(
	'аренда спецтехники, аренда спецтехники киев, аренда спецтехники харьков, аренда спецтехники донецк,
	аренда спецтехники днепропетровск, аренда самосвала, аренда экскаватора, аренда погрузчика'
);

$this->breadcrumbs = array(
	Yii::t('main', 'Предложения по аренде спецтехники'),
);
?>

<div class="row">
	<div class="span12">
		<?php echo $this->renderPartial('//search/search-block'); ?>
	</div>
</div>

<div class="row">
	<div class="span12 special-description">
		<h1 class="small">Аренда спецтехники</h1>
		<p>
			<strong>Спецтехника</strong> - это техника предназначенная для выполнения строго определенного круга задач.
			Спецтехника может быть поделена на разные типы и виды, в зависимости от выполняемых работ
			(<i>дорожно-строительная техника, краны, погрузчики, подъемники, экскаваторы</i>).
		</p>
		<p>
			<strong>Аренда техники и спецтехники</strong> — одна из наиболее выгодных форм использования техники для нужд сельского хозяйства,
			лёгкой и тяжёлой промышленности, городского строительства и хозяйства, а также предпринимательства,
			которая предусматривает целевое или сезонное использование техники хозяйствующими субъектами без её покупки.
		</p>
		<p>
			На портале <strong>"<?php echo Yii::app()->name ?>"</strong> вы можете подобрать себе спецтехнику на любой вкус и под любые потребности.
			<strong>Аренда спецтехники</strong> - под любые ваши потребности, это наша работа.
			Определиться выбором спецтехники вам помогут: удобный поиск, рейтинги и отзывы об арендодателях.
		</p>
	</div>
</div>

<?php $this->beginContent('//decorators/order-block', array(
	'totalItemCount' => $provider->totalItemCount,
	'sort' => $provider->getSort(),
))?>
<?php $this->endContent()?>

<div class="text-divider5"><span></span></div>

<div class="rblock to-rent blog">
	<?php foreach($provider->getData() as $item): ?>
		<?php $this->renderPartial('//toRent/short-item', array('model' => $item)); ?>
	<?php endforeach; ?>
</div>
<?php $this->widget('CLinkPager', array(
	'pages' => $provider->getPagination(),
	'maxButtonCount' => 7,
	'cssFile' => false,
	'header' => '<div class="pagination pagination-centered pagination-large">',
	'footer' => '</div>',
	'hiddenPageCssClass' => 'disabled',
	'selectedPageCssClass' => 'active',
	'firstPageLabel' => '&laquo;&laquo;',
	'prevPageLabel' => '&laquo;',
	'nextPageLabel' => '&raquo;',
	'lastPageLabel' => '&raquo;&raquo;',
)) ?>