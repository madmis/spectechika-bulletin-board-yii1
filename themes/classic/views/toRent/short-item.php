<?php
/** @var $model ToRent  */
?>
<div class="row">
	<div class="span2">
		<?php echo CHtml::link(
			CHtml::image(
				$model->getPhotoUrl(ToRent::PHOTO_SMALL_PREFIX),
				$model->category->name,
				array('title' => $model->category->name)
			),
			"/toRent/{$model->id}",
			array(
				'title' => $model->category->name, 'class' => 'thumbnail',
				'style' => 'width:'.ToRent::PHOTO_SMALL_WIDTH.'px;'
			)
		); ?>
		<div class="meta">
			<div class="created">
				<i class="icon-calendar"></i><?php echo $model->getAttributeLabel('create_at'); ?>
				<br><?php echo Yii::app()->format->formatDatetime($model->create_at); ?>
			</div>
		</div>
	</div>
	<div class="span10">
		<div class="rinfo">
			<!-- Name -->
			<h4 class="media-heading">
				<?php echo CHtml::link(
					$model->category->name,
					"/toRent/{$model->id}",
					array('title' => "Аренда {$model->category->name_r}")
				); ?>
			</h4>
			<p class="item">
				<i class="icon-tags"></i>
				<span class="name"><?php echo $model->getAttributeLabel('category_id'); ?>:</span>
					<span class="value">
						<?php echo CHtml::link($model->category->name, "/toRent/category/{$model->category->translit}",
							array('title' => "{$model->getAttributeLabel('category_id')}: {$model->category->name}")
						); ?>
					</span>
			</p>
			<p class="item">
				<i class="icon-globe"></i>
				<span class="name"><?php echo $model->getAttributeLabel('region_id'); ?>:</span>
					<span class="value">
						<?php echo CHtml::link($model->region->name, "/toRent/region/{$model->region->translit}",
							array('title' => "{$model->getAttributeLabel('region_id')}: {$model->region->name}")
						); ?>
					</span>
				<?php if ($model->city_id): ?>
					<?php /** @var $model ToRent */  ?>
					&bull;
					<span class="value">
							<?php echo CHtml::link($model->city->name, "/toRent/city/{$model->city->id}",
								array('title' => "{$model->getAttributeLabel('city_id')}: {$model->city->name}")
							); ?>
						</span>
				<?php endif; ?>
			</p>
			<p class="item">
				<i class="icon-money"></i>
				<span class="name"><?php echo $model->getAttributeLabel('price'); ?>:</span>
				<span class="value">
					<?php echo Yii::app()->numberFormatter->formatCurrency(
						$model->price, Yii::app()->params->itemAt('currency')
					); ?>
				</span>
			</p>

			<p class="item">
				<span class="heading"><?php echo $model->getAttributeLabel('features'); ?></span>
				<span class=""><?php echo nl2br($model->features); ?></span>
			</p>

			<p class="item">
				<span class="heading"><?php echo $model->getAttributeLabel('description'); ?></span>
				<span class=""><?php echo nl2br($model->description); ?></span>
			</p>
		</div>
	</div>
</div>
<div class="meta">
	<span title="Дата добавления">
		<i class="icon-calendar"></i>
		<?php echo Yii::app()->dateFormatter->formatDateTime($model->create_at, 'full', 'short'); ?>
	</span>
	<span title="Автор">
		<i class="icon-user"></i> <?php echo $model->fio; ?>
	</span>
<!--	<span title="Количество просмотров">-->
<!--		<i class="icon-eye-open"></i> 32-->
<!--	</span>-->
	<span class="pull-right">
		<span title="Контактная информация">
			<i class="icon-link"></i>
			<?php echo CHtml::link('контактная информация', "/toRent/{$model->id}", array('title' => 'контактная информация')); ?>
		</span>
<!--		<span title="Комментарии">-->
<!--			<i class="icon-comment"></i> <a href="#">2 комментария</a>-->
<!--		</span>-->
	</span>
</div>
<div class="member-divider"></div>