<?php
/**
 * @var $this TController
 * @var $items ToRent[]
 *
 */
$model = ToRent::model();
?>
<?php if ($items): ?>
	<div class="row-fluid">
		<div class="span12">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><?php echo $model->getAttributeLabel('photo'); ?></th>
						<th><?php echo $model->getAttributeLabel('category_id'); ?></th>
						<th><?php echo $model->getAttributeLabel('price'); ?></th>
						<th><?php echo $model->getAttributeLabel('region_id'); ?></th>
						<th><?php echo $model->getAttributeLabel('create_at'); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($items as $item): ?>
					<tr>
						<td>
							<?php echo CHtml::link(
								CHtml::image(
									$item->getPhotoUrl(ToRent::PHOTO_MINI_PREFIX),
									$item->category->name,
									array('title' => $item->category->name)
								),
								"/toRent/{$item->id}",
								array(
									'title' => $item->category->name, 'class' => 'thumbnail',
									'style' => 'width:'.ToRent::PHOTO_MINI_WIDTH.'px;'
								)
							); ?>
						</td>
						<td>
							<?php echo CHtml::link($item->category->name, "/toRent/category/{$item->category->translit}",
								array('title' => "{$item->getAttributeLabel('category_id')}: {$item->category->name}")
							); ?>
						</td>
						<td>
							<?php echo Yii::app()->numberFormatter->formatCurrency(
								$item->price, Yii::app()->params->itemAt('currency')
							); ?>
						</td>
						<td>
							<?php echo CHtml::link($item->region->name, "/toRent/region/{$item->region->translit}",
								array('title' => "{$item->getAttributeLabel('region_id')}: {$item->region->name}")
							); ?>
						</td>
						<td>
							<?php echo Yii::app()->format->formatDatetime($item->create_at); ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
<?php endif; ?>