<?php
/* @var $this ToRentController */
/* @var $model ToRent */
/* @var $buse Buses */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScriptFile($this->assetsBase . '/js/maskedinput/jquery.maskedinput.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($this->assetsBase . '/js/profile.t.js', CClientScript::POS_END);
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'torent-form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnChange' => true,
		'validateOnSubmit' => true,
	),
	'errorMessageCssClass' => 'error',
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'class' => 'form-horizontal big-label',
	),
)); ?>

<div class="row-fluid">
	<div class="span5">
		<div class="control-group">
			<?php echo $form->labelEx($model, 'photo', array('class' => 'control-label')); ?>
			<div class="controls">
				<?php $this->widget('application.widgets.TImageInput.TImageInput', array(
					'model' => $model,
					'attribute' => 'photo',
					'previewSelector' => 'ul#img-list',
					'previewSize' => ToRent::PHOTO_SMALL_WIDTH,
					'htmlOptions' => array(
						'placeholder' => $model->getAttributeLabel('photo'),
						'class' => 'span12'
					),
				)); ?>
			</div>
			<?php echo $form->error($model, 'photo', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
		</div>
	</div>
	<div class="span4">
		<ul id="img-list">
			<?php if ($model->photo): ?>
				<li><?php echo CHtml::image($model->getPhotoUrl(ToRent::PHOTO_SMALL_PREFIX)); ?></li>
			<?php elseif (!empty($buse->photo)): ?>
				<li><?php echo CHtml::image($buse->getPhotoUrl(ToRent::PHOTO_SMALL_PREFIX)); ?></li>
			<?php endif; ?>
		</ul>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'category_id', array(
		'class' => 'control-label'
	)); ?>
	<div class="controls">
		<?php echo $form->dropDownList($model, 'category_id',
			Category::model()->groupedIndexedList(),
			array('empty' => 'Выберите категорию', 'class' => 'span4')
		); ?>
		<?php echo $form->error($model, 'category_id', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'region_id', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php echo $form->dropDownList($model, 'region_id',
			Region::model()->getList(),
			array('empty' => 'Выберите регион', 'class' => 'span4')
		); ?>
		<?php echo $form->error($model, 'region_id', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'city_id', array('class' => 'control-label')); ?>
	<div class="controls">
		<div class="input-append">
			<?php $this->widget('application.widgets.TTypeAhead.TTypeAhead', array(
				'model' => $model,
				'attribute' => 'city_id',
				'name' => 'ToRent[city_id]',
				'value' => !empty($model->city) ? $model->city->name : '',
				'htmlOptions' => array(
					'placeholder' => $model->getAttributeLabel('city_id'),
				),
				'options' => array(
					'minLength' => 3,
					'limit' => 10,
					'name' => 'city_id',
					'remote' => array(
						'url' => $this->createUrl('city/clientCitiesOld/%QUERY'),
						'replace' => 'js:function(url, uriEncodedQuery) { return T.profile.cityReplace(url, uriEncodedQuery); } ',
					),
				)
			));?>
			<span id="tp" class="add-on" rel="tooltip"
				  title="При заполненнии города работает автоподстановка.
				  Т.е., когда вы вводите название города, вам предлагаются похожие варианты и нужно лишь выбрать необходимый.
				  Если выбран регон, предлагаются только города выбранного региона.">
				<i class="icon-question"></i>
			</span>
		</div>
		<?php echo $form->error($model, 'city_id', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group',
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'price', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php echo $form->textField($model, 'price', array(
			'class' => 'span1',
		)); ?>
		<?php echo $form->error($model, 'price', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'features', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php echo $form->textArea($model, 'features', array(
			'class' => 'span5',
			'rows' => 6,
			'placeholder' => $model->getAttributeLabel('features'),
		)); ?>
		<?php echo $form->error($model, 'features', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'description', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php echo $form->textArea($model, 'description', array(
			'class' => 'span5',
			'rows' => 8,
			'placeholder' => $model->getAttributeLabel('description'),
		)); ?>
		<?php echo $form->error($model, 'description', array(
			'class' => 'help-inline',
			'inputContainer' => 'div.control-group'
		)); ?>
	</div>
</div>

<h4 class="title">Контактные данные</h4>

<div class="control-group">
	<?php echo $form->labelEx($model, 'fio', array('class' => 'control-label')); ?>
	<div class="controls">
		<div class="input-prepend">
			<span class="add-on"><i class="icon-user"></i></span>
			<?php echo $form->textField($model, 'fio', array(
				'class' => 'span4',
				'placeholder' => $model->getAttributeLabel('fio'),
			)); ?>
		</div>
		<?php echo $form->error($model, 'fio', array('class' => 'help-inline', 'inputContainer' => 'div.control-group')); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'company_name', array('class' => 'control-label')); ?>
	<div class="controls">
		<div class="input-prepend">
			<span class="add-on"><i class="icon-building"></i></span>
			<?php echo $form->textField($model, 'company_name', array(
				'class' => 'span4',
				'placeholder' => $model->getAttributeLabel('company_name'),
			)); ?>
		</div>
		<?php echo $form->error($model, 'company_name', array('class' => 'help-inline', 'inputContainer' => 'div.control-group')); ?>
	</div>
</div>

<div class="control-group">
	<?php echo $form->labelEx($model, 'phone', array('class' => 'control-label')); ?>
	<div class="controls">
		<div class="input-prepend">
			<span class="add-on"><i class="icon-phone-sign"></i></span>
			<?php echo $form->textField($model, 'phone', array(
				'maxlength' => 19,
				'class' => 'span4 masked-phone',
				'data-mask' => Yii::app()->params->itemAt('phoneMask'),
				'placeholder' => $model->getAttributeLabel('phone'),
			));?>
		</div>
		<?php echo $form->error($model, 'phone', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<?php echo CHtml::submitButton(Yii::t('app', 'Сохранить'), array('class' => 'btn')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
