<?php
/* @var $this ToRentController */
/* @var $provider CActiveDataProvider */
/* @var $region Region */
$this->pageTitle = TMeta::toRentRegionTitle($region);
$this->setPageDescription(TMeta::toRentRegionDescription($region));
$this->setPageKeywords(TMeta::toRentRegionKeywords($region));

$this->breadcrumbs = array(
	Yii::t('main', 'Предложения по аренде спецтехники') => array('/toRent'),
	$region->name
);
?>

<div class="row">
	<div class="span12">
		<?php echo $this->renderPartial('//search/search-block'); ?>
	</div>
</div>

<div class="row">
	<div class="span12 special-description">
		<h1 class="small"><?php echo TMeta::toRentRegionTitle($region); ?></h1>
	</div>
</div>

<?php $this->beginContent('//decorators/order-block', array(
	'totalItemCount' => $provider->totalItemCount,
	'sort' => $provider->getSort(),
))?>
<?php $this->endContent()?>

<div class="text-divider5"><span></span></div>

<div class="rblock to-rent blog">
	<?php foreach($provider->getData() as $item): ?>
		<?php $this->renderPartial('//toRent/short-item', array('model' => $item)); ?>
	<?php endforeach; ?>
</div>
<?php $this->widget('CLinkPager', array(
	'pages' => $provider->getPagination(),
	'maxButtonCount' => 7,
	'cssFile' => false,
	'header' => '<div class="pagination pagination-centered pagination-large">',
	'footer' => '</div>',
	'hiddenPageCssClass' => 'disabled',
	'selectedPageCssClass' => 'active',
	'firstPageLabel' => '&laquo;&laquo;',
	'prevPageLabel' => '&laquo;',
	'nextPageLabel' => '&raquo;',
	'lastPageLabel' => '&raquo;&raquo;',
)) ?>