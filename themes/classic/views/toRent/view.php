<?php
/**
 * @var $this ToRentController
 * @var $model ToRent
 */
$this->pageTitle = TMeta::toRentTitle($model);
$this->setPageDescription(TMeta::toRentDescription($model));
$this->setPageKeywords(TMeta::toRentKeywords($model));

$this->breadcrumbs = array(
	Yii::t('main', 'Предложения по аренде спецтехники') => array('/toRent'),
	$model->category->name,
);
?>

<div class="margin-bottom-10"></div>
<div class="rblock to-rent blog">
	<?php $this->renderPartial(
		'//toRent/full-item',
		array('model' => $model)
	); ?>
</div>

<div class="well">
	<!-- Social media icons -->
	<div class="pull-left">
		<h5>Поделиться: </h5>
		<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
		<div class="yashare-auto-init" data-yashareL10n="ru"
			 data-yashareType="none"
			 data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug,gplus,surfingbird">
		 </div>
	</div>
	<!-- Tags -->
	<div class="tags pull-right">
		<h5>Теги: </h5>
			<?php echo CHtml::link($model->category->name, "/toRent/category/{$model->category->translit}",
				array('title' => "{$model->getAttributeLabel('category_id')}: {$model->category->name}")
			); ?>
			<?php echo CHtml::link($model->region->name, "/toRent/region/{$model->region->translit}",
				array('title' => "{$model->getAttributeLabel('region_id')}: {$model->region->name}")
			); ?>
			<?php if ($model->city_id): ?>
				<?php echo CHtml::link($model->city->name, "/toRent/city/{$model->city->id}",
					array('title' => "{$model->getAttributeLabel('city_id')}: {$model->city->name}")
				); ?>
			<?php endif; ?>
	</div>
	<div class="clearfix"></div>
</div>