<?php
/**
 * @var $this ToRentController
 * @var $model ToRent
 */
?>
<div class="row">
	<div class="span2">
		<div class="thumbnail" style="width: <?php echo ToRent::PHOTO_SMALL_WIDTH; ?>px;">
			<?php echo CHtml::image(
				$model->getPhotoUrl(ToRent::PHOTO_SMALL_PREFIX),
				$model->category->name,
				array('title' => $model->category->name)
			); ?>
		</div>
	</div>
	<div class="span10">
		<div class="rinfo">
			<h2 class="media-heading"><?php echo $model->category->name; ?></h2>
			<div class="row">
				<div class="span6">
					<p class="item">
						<i class="icon-calendar"></i>
						<span class="name"><?php echo $model->getAttributeLabel('create_at'); ?>:</span>
						<span class="value">
							<?php echo Yii::app()->dateFormatter->formatDateTime($model->create_at, 'full', 'short'); ?>
						</span>
					</p>
					<p class="item">
						<i class="icon-tags"></i>
						<span class="name"><?php echo $model->getAttributeLabel('category_id'); ?>:</span>
						<span class="value">
							<?php echo CHtml::link($model->category->name, "/toRent/category/{$model->category->translit}",
								array('title' => "{$model->getAttributeLabel('category_id')}: {$model->category->name}")
							); ?>
						</span>
					</p>
					<p class="item">
						<i class="icon-globe"></i>
						<span class="name"><?php echo $model->getAttributeLabel('region_id'); ?>:</span>
						<span class="value">
							<?php echo CHtml::link($model->region->name, "/toRent/region/{$model->region->translit}",
								array('title' => "{$model->getAttributeLabel('region_id')}: {$model->region->name}")
							); ?>
						</span>
						<?php if ($model->city_id): ?>
							<?php /** @var $model ToRent */  ?>
							&bull;
							<span class="value">
								<?php echo CHtml::link($model->city->name, "/toRent/city/{$model->city->id}",
									array('title' => "{$model->getAttributeLabel('city_id')}: {$model->city->name}")
								); ?>
							</span>
						<?php endif; ?>
					</p>
					<p class="item">
						<i class="icon-money"></i>
						<span class="name"><?php echo $model->getAttributeLabel('price'); ?>:</span>
						<span class="value">
							<?php echo Yii::app()->numberFormatter->formatCurrency($model->price, Yii::app()->params->itemAt('currency')); ?>
						</span>
					</p>
					<?php /** @var $model ToRent */ ?>
					<p class="item">
						<span class="heading"><?php echo $model->getAttributeLabel('features'); ?></span>
						<span><?php echo nl2br($model->features); ?></span>
					</p>

				</div>
				<div class="span4 sidebar">
					<div class="widget">
						<h4>Контактная информация</h4>
						<p class="item">
							<i class="icon-user"></i>
							<span class="name"><?php echo $model->getAttributeLabel('fio'); ?>:</span>
							<span class="value"><?php echo $model->fio; ?></span>
						</p>
						<?php if ($model->company_name): ?>
							<p class="item">
								<i class="icon-building"></i>
								<span class="name"><?php echo $model->getAttributeLabel('company_name'); ?>:</span>
								<span class="value"><?php echo $model->company_name; ?></span>
							</p>
						<?php endif; ?>
						<p class="item">
							<i class="icon-phone"></i>
							<span class="name"><?php echo $model->getAttributeLabel('phone'); ?>:</span>
							<span class="value"><?php echo $model->phone; ?></span>
						</p>
					</div>
				</div>
			</div>

			<p class="item">
				<span class="heading"><?php echo $model->getAttributeLabel('description'); ?></span>
				<span class=""><?php echo nl2br($model->description); ?></span>
			</p>
		</div>
	</div>
</div>