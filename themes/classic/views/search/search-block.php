<?php
/**
 * @var $this ToRentController
 */
?>

<div class="cta">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="ctas">
					<form id="search-torent-form" class="search-form" action="<?php echo $this->createUrl("/{$this->id}/search"); ?>" method="get">
						<ul class="inline">
							<li id="category-li">
								<span class="input-label">Категория</span>
								<div class="input-imitator category" id="search-category-input" data-dialog="search-category-dialog">
									<?php if ($this->searchCategory): ?>
										<?php echo $this->searchCategory->name; ?>
									<?php else: ?>
										<span class="placeholder">укажите категорию</span>
									<?php endif; ?>
								</div>
								<?php if ($this->searchCategory): ?>
									<input name="category" class="hidden"
										   value="<?php echo $this->searchCategory->id; ?>"
										   id="search-category-input-h">
								<?php else: ?>
									<input name="category" id="search-category-input-h" class="hidden" value="">
								<?php endif; ?>
								<i class="icon-remove pull-right" title="Очистить"
								   style="<?php echo $this->searchCategory ? 'display:inline;' : ''; ?>"></i>
							</li>
							<li id="region-li">
								<span class="input-label">Регион</span>
								<div class="input-imitator region" id="search-region-input" data-dialog="search-region-dialog">
									<?php if ($this->searchRegion): ?>
										<?php echo $this->searchRegion->name; ?>
									<?php else: ?>
										<span class="placeholder">выберите регион</span>
									<?php endif; ?>
								</div>
								<?php if ($this->searchRegion): ?>
									<input name="region" class="hidden"
										   value="<?php echo $this->searchRegion->id; ?>"
										   id="search-region-input-h">
								<?php else: ?>
									<input name="region" id="search-region-input-h" class="hidden" value="">
								<?php endif; ?>
								<i class="icon-remove pull-right" title="Очистить"
								   style="<?php echo $this->searchRegion ? 'display:inline;' : ''; ?>"></i>
							</li>
							<li>
								<button class="btn btn-large" id="search-doctor" title="<?php echo Yii::t('button', 'Найти объявление'); ?>">
									<i class="icon-search"></i>
								</button>
							</li>
						</ul>
					</form>

					<?php echo $this->renderPartial('//search/_category-dialog'); ?>
					<?php echo $this->renderPartial('//search/_region-dialog'); ?>
				</div>
			</div>
		</div>
	</div>
</div>