<?php
/**
 * @var $this TController
 */
$items = Category::model()->parentAndChild();
?>
<div id="search-category-dialog" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4><?php echo Yii::t('main', 'Выберите категорию'); ?></h4>
		<ul class="parent-block">
			<?php foreach ($items->itemAt('parent') as $item): ?>
				<?php /** @var $item Category */ ?>
				<li class="item" data-target="category-parent-<?php echo $item->id; ?>">
					<?php echo $item->name; ?>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
	<div class="modal-body">
		<?php foreach ($items->itemAt('child') as $parent_id => $items): ?>
		<ul class="category-block" id="category-parent-<?php echo $parent_id; ?>">
			<?php foreach ($items as $item): ?>
				<?php /** @var $item Category */ ?>
				<li class="item">
					<div>
						<label style="white-space: nowrap;">
							<input class="search-category-radio" type="radio" name="category-radio"
								   id="search-category-radio-<?php echo $item->id; ?>"
								   data-title="<?php echo $item->name; ?>" value="<?php echo $item->id; ?>">
							<?php echo $item->name; ?>
						</label>
					</div>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php endforeach; ?>
	</div>
	<div class="modal-footer">
		<?php echo CHtml::link('Закрыть', '#', array(
			'class' => 'btn btn-link',
			'data-dismiss' => 'modal',
			'aria-hidden' => 'true'
		)); ?>
		<?php echo CHtml::link(Yii::t('button', 'Выбрать'), '#', array(
			'class' => 'btn btn-success',
			'id' => 'search-category-dialog-btn',
		)); ?>
	</div>
</div>