<?php
/**
 * @var $this TController
 */
$items = Region::model()->findAll();
?>
<div id="search-region-dialog" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4><?php echo Yii::t('main', 'Выберите регион'); ?></h4>
	</div>
	<div class="modal-body">
		<ul class="region-block">
		<?php foreach ($items as $item): ?>
			<?php /** @var $item Region */ ?>
			<li class="item">
				<div>
					<label style="white-space: nowrap;">
						<input class="search-region-radio" type="radio" name="region-radio"
							   id="search-region-radio-<?php echo $item->id; ?>"
							   data-title="<?php echo $item->name; ?>" value="<?php echo $item->id; ?>">
						<?php echo $item->name; ?>
					</label>
				</div>
			</li>
		<?php endforeach; ?>
		</ul>
	</div>
	<div class="modal-footer">
		<?php echo CHtml::link('Закрыть', '#', array(
			'class' => 'btn btn-link',
			'data-dismiss' => 'modal',
			'aria-hidden' => 'true'
		)); ?>
		<?php echo CHtml::link(Yii::t('button', 'Выбрать'), '#', array(
			'class' => 'btn btn-success',
			'id' => 'search-region-dialog-btn',
		)); ?>
	</div>
</div>