<?php
/**
 * @var $this RentController
 * @var $rent Rent
 * @var $items RentBid[]
 */
?>
<?php foreach ($items as $item): ?>
<div class="row">
	<div class="span8">
		<div class="media my-buses-list">
			<a class="pull-left thumbnail" style="width: <?php echo Buses::PHOTO_SMALL_WIDTH ?>px;">
				<?php echo CHtml::image($item->buse->getPhotoUrl(Buses::PHOTO_SMALL_PREFIX)); ?>
			</a>

			<div class="media-body">
				<h4 class="media-heading">
					<?php echo $item->user->username; ?>
					<?php if ($rent->status == 1 && $rent->performer_id == $item->user_id && Yii::app()->user->id == $rent->user_id): ?>
						<span class="performer">(исполнитель)</span>
					<? endif; ?>
				</h4>
				<div class="my-buses-content">
					<i class="icon-calendar"></i>
					<span class="my-buses-label"><?php echo $item->getAttributeLabel('create_at'); ?>: </span>
					<?php echo Yii::app()->format->formatDatetime($item->create_at); ?>
				</div>

				<div class="my-buses-content">
					<i class="icon-money"></i>
					<span class="my-buses-label"><?php echo $item->getAttributeLabel('price'); ?>: </span>
					<?php echo $item->price; ?> грн.
				</div>

				<div class="my-buses-content">
					<i class="icon-tags"></i>
					<span class="my-buses-label"><?php echo $item->buse->getAttributeLabel('category_id'); ?>: </span>
					<?php echo CHtml::link($item->buse->category->name, "/rent/category/{$item->buse->category->translit}",
						array('title' => "{$item->buse->getAttributeLabel('category_id')}: {$item->buse->category->name}")
					); ?>

				</div>
				<div class="my-buses-content">
					<span class="my-buses-label"><?php echo $item->getAttributeLabel('description'); ?></span>
					<div class="my-buses-inline"><?php echo nl2br($item->description); ?></div>
				</div>
			</div>
		</div>
	</div>
	<div class="span4">
		<?php if (Yii::app()->user->id === $rent->user_id): ?>
			<?php if ($rent->status == 0): ?>
				<span title="Заказ будет закрыт и пользователь будет выбран исполнителем" class="bid-select-performer">
					<i class="icon-user"></i>&nbsp;&nbsp;
					<?php echo CHtml::link('Выбрать исполнителем', array('/rent/setStatus/', 'id'=>$rent->id, 'status'=>1, 'performer' => $item->user_id)); ?>
				</span>
			<?php endif; ?>
			<div class="span4 sidebar pull-right">
				<div class="widget">
					<h4>Контактная информация</h4>
					<?php if ($item->user->company_name): ?>
						<p class="item">
							<i class="icon-building"></i>
							<span class="name"><?php echo $item->user->getAttributeLabel('company_name'); ?>:</span>
							<span class="value"><?php echo $item->user->company_name; ?></span>
						</p>
					<?php endif; ?>
					<p class="item">
						<i class="icon-phone"></i>
						<span class="name"><?php echo $item->user->getAttributeLabel('phone'); ?>:</span>
						<span class="value"><?php echo $item->user->phone; ?></span>
					</p>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>

<div class="text-divider5"><span></span></div>
<?php endforeach; ?>