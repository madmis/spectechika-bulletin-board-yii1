<?php
/* @var $this RentBidController */
/* @var $model RentBid */
/* @var $rent Rent */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerPackage('rentBidPackage');

?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'rent-bid-form',
	'action' => $this->createUrl('/rentBid/add'),
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnChange' => true,
		'validateOnSubmit' => true,
	),
	'errorMessageCssClass' => 'error',
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'class' => 'form-horizontal big-label',
	),
)); ?>

	<?php echo $form->hiddenField($model, 'rent_id', array('value' => $rent->id)); ?>
	<div class="control-group">
		<?php echo $form->labelEx($model, 'buse_id', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?php $list = Buses::model()->getDropDownList($rent->category_id); ?>
			<?php echo $form->dropDownList($model, 'buse_id', $list['data'], array(
				'options' => $list['options'], 'class' => 'span4',
				'prompt' => '',
			)); ?>
			<?php echo $form->error($model, 'buse_id', array(
				'class' => 'help-inline',
				'inputContainer' => 'div.control-group'
			)); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'price', array(
			'class' => 'control-label'
		)); ?>
		<div class="controls">
			<?php echo $form->textField($model, 'price', array(
				'maxlength' => 11,
				'class' => 'span4',
				'placeholder' => $model->getAttributeLabel('price'),
				'value' => $rent->max_price,
			)); ?>
			<?php echo $form->error($model, 'price', array(
				'class' => 'help-inline',
				'inputContainer' => 'div.control-group'
			)); ?>
		</div>
	</div>
	<div class="control-group">
		<?php echo $form->labelEx($model, 'description', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo $form->textArea($model, 'description', array(
				'class' => 'span5',
				'rows' => 8,
				'placeholder' => $model->getAttributeLabel('description'),
			)); ?>
			<?php echo $form->error($model, 'description', array(
				'class' => 'help-inline',
				'inputContainer' => 'div.control-group'
			)); ?>
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<?php echo CHtml::submitButton(Yii::t('app', 'Сохранить'), array('class' => 'btn')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>