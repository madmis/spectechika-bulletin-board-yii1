<?php
/**
 * @var NewsController $this
 * @var $model News
 */
?>
<div class="entry">
	<h2><?php echo CHtml::link($model->title, array("/news/$model->translit")); ?></h2>

	<div class="meta">
		<i class="icon-calendar"></i>&nbsp;
		<?php echo Yii::app()->dateFormatter->formatDateTime($model->create_at, 'full', null); ?>
		<span class="pull-right"><i class="icon-user"></i>&nbsp;<?php echo $model->user->username; ?></span>
	</div>

	<div class="content"><?php echo TString::cuteText($model->text, 550,  true, false); ?></div>
</div>
<div class="row">
	<div class="span4 pull-right">
		<div class="button pull-right"><?php echo CHtml::link('читать дальше...', array("/news/$model->translit")); ?></div>
	</div>
</div>
