<?php
/**
 * @var NewsController $this
 * @var News $model
 * @var CActiveForm $form
 */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'news-form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnChange' => true,
		'validateOnSubmit' => true,
	),
	'errorMessageCssClass' => 'error',
)); ?>

	<fieldset>
		<legend>Добавление новости</legend>

		<div class="control-group">
			<?php echo $form->labelEx($model, 'create_at', array('class' => 'control-label')); ?>
			<div class="controls">
				<?php echo $this->widget('application.widgets.TDatePicker.TDatePicker', array(
					'model' => $model,
					'attribute' => 'create_at',
				), true); ?>
			</div>
			<?php echo $form->error($model, 'create_at', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
		</div>

		<div class="control-group">
			<?php echo $form->labelEx($model, 'title', array('class' => 'control-label')); ?>
			<div class="controls">
				<?php echo $form->textField($model, 'title', array(
					'class' => 'span7'
				)); ?>
			</div>
			<?php echo $form->error($model, 'title', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
		</div>

		<div class="control-group">
			<?php echo $form->labelEx($model, 'text', array('class' => 'control-label')); ?>
			<div class="controls">
				<?php $this->widget('ext.markitup.EMarkitupWidget', array(
					'model' => $model,
					'attribute' => 'text',
					'theme' => 'markitup',
					'options' => array(),
					'htmlOptions' => array(
						'rows' => 15,
						'placeholder' => $model->getAttributeLabel('text'),
					),
				)); ?>
			</div>
			<?php echo $form->error($model, 'text', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
		</div>

		<div class="control-group">
			<?php echo $form->labelEx($model, 'meta_keywords', array('class' => 'control-label')); ?>
			<div class="controls">
				<?php echo $form->textArea($model, 'meta_keywords', array(
					'class' => 'span7',
					'rows' => 4,
					'placeholder' => $model->getAttributeLabel('meta_keywords'),
				)); ?>
			</div>
			<?php echo $form->error($model, 'meta_keywords', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
		</div>

		<div class="control-group">
			<?php echo $form->labelEx($model, 'meta_description', array('class' => 'control-label')); ?>
			<div class="controls">
				<?php echo $form->textArea($model, 'meta_description', array(
					'class' => 'span7',
					'rows' => 4,
					'placeholder' => $model->getAttributeLabel('meta_description'),
				)); ?>
			</div>
			<?php echo $form->error($model, 'meta_description', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
		</div>


		<div class="control-group">
			<div class="controls">
				<?php echo CHtml::submitButton(Yii::t('app', 'Сохранить'), array('class' => 'btn')); ?>
			</div>
		</div>
	</fieldset>
<?php $this->endWidget(); ?>