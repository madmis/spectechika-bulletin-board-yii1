<?php
/**
 * @var NewsController $this
 * @var News $model
 */
?>

<?php $this->renderPartial('_form', array('model' => $model)); ?>