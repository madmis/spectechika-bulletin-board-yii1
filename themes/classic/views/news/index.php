<?php
/**
 * @var NewsController $this
 * @var $provider CActiveDataProvider
 */
$this->pageTitle = "Новости сайта";
$this->setPageDescription("Новости сайта " . Yii::app()->name);
$this->setPageKeywords("новости сайта " . Yii::app()->name . ", новости по теме аренда спецтехники");

$this->breadcrumbs = array(Yii::t('main', 'Новости'));
?>

<div class="row">
	<div class="span12 special-description">
		<h1 class="medium">Новости сайта</h1>
	</div>
</div>

<div class="row">
	<div class="span12">
		<div class="news blog posts">
			<?php foreach ($provider->getData() as $item): ?>
				<?php $this->renderPartial('//news/short-item', array('model' => $item)); ?>
			<?php endforeach; ?>
		</div>
	</div>
</div>

<?php $this->widget('CLinkPager', array(
	'pages' => $provider->getPagination(),
	'maxButtonCount' => 7,
	'cssFile' => false,
	'header' => '<div class="pagination pagination-centered pagination-large">',
	'footer' => '</div>',
	'hiddenPageCssClass' => 'disabled',
	'selectedPageCssClass' => 'active',
	'firstPageLabel' => '&laquo;&laquo;',
	'prevPageLabel' => '&laquo;',
	'nextPageLabel' => '&raquo;',
	'lastPageLabel' => '&raquo;&raquo;',
)) ?>