<?php
/**
 * @var $this NewsController
 * @var $items News[]
 */
?>
<?php if ($items): ?>
<div class="widget">
	<h4>Последние новости</h4>
	<ul>
		<?php foreach ($items as $item): ?>
			<li><?php echo CHtml::link(
					$item->title,
					array("/news/{$item->translit}"),
					array('title' => $item->title)
				); ?></li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>