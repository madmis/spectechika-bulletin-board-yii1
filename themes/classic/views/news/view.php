<?php
/**
 * @var NewsController $this
 * @var $model News
 */
$this->pageTitle = $model->title;
$this->setPageDescription($model->meta_description);
$this->setPageKeywords($model->meta_keywords);

$this->breadcrumbs = array(
	Yii::t('main', 'Новости') => array('/news'),
	$model->title
);
?>

<div class="row-fluid">
	<div class="news blog span8">
		<h1 class="small"><?php echo $model->title; ?></h1>

		<div class="meta">
			<i class="icon-calendar"></i>&nbsp;
			<?php echo Yii::app()->dateFormatter->formatDateTime($model->create_at, 'full', null); ?>
			<span class="pull-right"><i class="icon-user"></i>&nbsp;<?php echo $model->user->username; ?></span>
		</div>

		<div class="content"><?php echo $model->text; ?></div>
	</div>
	<div class="span4">
		<div class="sidebar">
			<?php $this->renderPartial('//news/_block-last', array('items' => News::model()->findLast())); ?>
		</div>
	</div>
</div>