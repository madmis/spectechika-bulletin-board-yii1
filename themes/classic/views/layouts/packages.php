<?php
	/* @var $this TController */

	Yii::app()->clientScript->coreScriptPosition = CClientScript::POS_END;
	$bootstrapPackage = array(
		'baseUrl'=> '//netdna.bootstrapcdn.com/',
		'js'     => array('twitter-bootstrap/2.3.2/js/bootstrap.min.js'),
		'css'     => array(
			'twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css',
			'font-awesome/3.2.1/css/font-awesome.min.css'
		),
		'depends' => array('jquery', 'cookie'),
	);
	Yii::app()->clientScript->addPackage('bootstrapPackage', $bootstrapPackage)->registerPackage('bootstrapPackage');

	$mainPackage = array(
		'basePath'=> 'application.assets',
		'js'     => array(
			'js/noty/jquery.noty.js',
			'js/noty/layouts/topRight.js',
			'js/noty/layouts/center.js',
			'js/noty/themes/default.js',
			'js/bootstrap.js',
			'js/config.t.js',
			'js/api.t.js',
			'js/noty.t.js',
			'js/search.t.js',
		),
		'css' => array(
			'css/design.css',
			'css/blue.css',
			'css/bootstrap-extends.css',
			'css/main.css',
		),
		'depends' => array('jquery', 'cookie'),
	);
	Yii::app()->clientScript->addPackage('mainPackage', $mainPackage)->registerPackage('mainPackage');

	$rentBidPackage = array(
		'basePath' => 'application.assets',
		'js' => array(
			'js/select2/select2.min.js',
			'js/select2/select2_locale_' . Yii::app()->language .'.js',
			'js/rent.bid.t.js',
		),
		'css' => array('js/select2/select2.css'),
		'depends' => array('mainPackage'),
	);
	Yii::app()->clientScript->addPackage('rentBidPackage', $rentBidPackage);
?>