<?php /* @var $this TController */ ?>
<?php $this->beginContent('//layouts/main'); ?>
	<div id="content">
		<div class="container">
			<?php echo $content; ?>
		</div>
	</div>
<?php $this->endContent(); ?>