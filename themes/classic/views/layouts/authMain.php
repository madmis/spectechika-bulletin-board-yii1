<?php /* @var $this TController */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="language" content="ru"/>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php Yii::app()->clientScript->registerCssFile('http://fonts.googleapis.com/css?family=Open+Sans:400,600&subset=latin,cyrillic'); ?>
	<!--[if lte IE 8]>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome-ie7.min.css" rel="stylesheet"
		  type="text/css">
	<![endif]-->

	<?php Yii::app()->clientScript->registerLinkTag('icon', 'image/png', $this->getAssetsBase() . '/img/favicon.png'); ?>
	<?php Yii::app()->clientScript->registerLinkTag('shortcut icon', 'image/x-icon', $this->getAssetsBase() . '/img/favicon.ico'); ?>
	<?php Yii::app()->clientScript->registerLinkTag('canonical', null, Yii::app()->request->hostInfo); ?>

	<?php
	Yii::app()->clientScript->coreScriptPosition = CClientScript::POS_END;
	$bootstrapPackage = array(
		'baseUrl' => '//netdna.bootstrapcdn.com/',
		'js' => array('twitter-bootstrap/2.3.2/js/bootstrap.min.js'),
		'css' => array(
			'twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css',
			'font-awesome/3.1.1/css/font-awesome.min.css'
		),
		'depends' => array('jquery'),
	);
	Yii::app()->clientScript->addPackage('bootstrapPackage', $bootstrapPackage)->registerPackage('bootstrapPackage');

	$mainPackage = array(
		'basePath' => 'application.assets',
		'js' => array(
			'js/bootstrap.js',
			'js/config.t.js',
			'js/api.t.js',
		),
		'css' => array(
			'css/design.css',
			'css/blue.css',
			'css/bootstrap-extends.css',
			'css/main.css',
		),
		'depends' => array('jquery'),
	);
	Yii::app()->clientScript->addPackage('mainPackage', $mainPackage)->registerPackage('mainPackage');
	?>

</head>

<body>
<header>
	<?php $this->renderPartial('//elements/header'); ?>
</header>
<div class="sep"></div>
<?php echo $this->renderPartial('//elements/breadCrumbs'); ?>

<?php echo $this->renderPartial('//elements/userFlash'); ?>
<div id="content">
	<div class="container">
		<?php echo $content; ?>
	</div>
</div>
<footer>
	<?php $this->renderPartial('//elements/footer'); ?>
</footer>
</body>
</html>