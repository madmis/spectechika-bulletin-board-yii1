<?php /* @var $this TController */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="robots" content="follow, index" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php Yii::app()->clientScript->registerMetaTag($this->pageDescription, 'description'); ?>
	<?php Yii::app()->clientScript->registerMetaTag($this->pageKeywords, 'keywords'); ?>

	<?php Yii::app()->clientScript->registerCssFile('http://fonts.googleapis.com/css?family=Open+Sans:400,600&subset=latin,cyrillic'); ?>
	<!--[if lte IE 8]>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome-ie7.min.css" rel="stylesheet" type="text/css">
	<![endif]-->

	<?php Yii::app()->clientScript->registerLinkTag('icon', 'image/png', $this->getAssetsBase() . '/img/favicon.png'); ?>
	<?php Yii::app()->clientScript->registerLinkTag('shortcut icon', 'image/x-icon', $this->getAssetsBase() . '/img/favicon.ico'); ?>

	<meta name="verify-reformal" content="fa519f8bb2b1f18886877ca4" />
</head>

<body>
	<header>
		<?php $this->renderPartial('//elements/header'); ?>
	</header>

	<div class="sep"></div>

	<?php echo $this->renderPartial('//elements/breadCrumbs'); ?>

	<div class="clearfix padding-top"></div>

	<?php echo $this->renderPartial('//elements/userFlash'); ?>
	<?php echo $content; ?>

	<div class="clearfix padding-top"></div>
	<footer>
		<?php $this->renderPartial('//elements/footer'); ?>
	</footer>

	<div class="place-text top" id="global-loader"><span class="place-img-content">Loading...</span></div>
	<?php if (!YII_DEBUG): ?>
		<?php echo $this->renderPartial('//elements/externalCode'); ?>
	<?php endif; ?>
</body>
</html>