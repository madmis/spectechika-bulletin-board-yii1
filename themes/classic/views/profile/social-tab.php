<?php
/**
 * @var $this ProfileController
 * @var $userServices array
 *
 */

$this->pageTitle = 'Социальные сети';
$this->breadcrumbs = array(
	Yii::t('main', 'Профиль') => array('/profile/social'),
	Yii::t('main', 'Социальные сети'),
);
?>

<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>Внимание!</strong> Настоятельно рекомендуем вам подключить к аккаунту все социальные сети, в которых вы зарегистрированы.
	&nbsp;В этом случае, выполнив вход на сайт посредством любой из подключенных социальных сетей вы всегда будете авторизованы в текущем аккаунте.
</div>

<?php echo $this->renderPartial('//profile/tabs'); ?>

<div class="tab-content profile-tabs-content">
	<div id="profile-social" class="tab-pane active">
		<dl>
			<dt>Подключенные социальные сети</dt>
			<dd><span class="label label-info">Info</span>
				<small>
					социальные сети, которые уже подключены к текущему аккаунту.&nbsp;
					При авторизации на сайте через какую-либо из данных социальных сетей, вы будете авторизованы в текущем аккаунте.
				</small>
			</dd>
		</dl>
		<?php if (UserService::model()->isUserSocial(Yii::app()->user->id)): ?>
			<ul class="profile-social-networks">
				<?php foreach ($userServices as $name => $service): ?>
					<?php if (isset($service['user'])): ?>
						<li class="social-icon-32 <?php echo $name ?>" title="<?php echo $name ?>"></li>
						<li class="social-icon-manage">
							<p>
								<?php echo CHtml::link(
									'<i class="icon-refresh"></i>',
									CHtml::normalizeUrl(array('profile/updateService', 'service' => $name)),
									array('title' => 'Обновить')
								); ?>
							</p>

							<p>
								<?php echo CHtml::link(
									'<i class="icon-remove"></i>',
									CHtml::normalizeUrl(array('profile/removeService', 'service' => $name)),
									array('title' => 'Удалить')
								); ?>
							</p>
						</li>
					<?php endif ?>
				<?php endforeach ?>
			</ul>
		<?php else: ?>
			<div class="not-profile-social">Нет подключенных социальных сетей</div>
		<?php endif; ?>
		<br><br><br>
		<hr>
		<br>
		<dl>
			<dt>Доступные социальные сети</dt>
			<dd><span class="label label-info">Info</span>
				<small>социальные сети, которые вы можете подключить к своему аккаунту.
					При подключении к аккаунту одной из социальных сетей, в дальнейшем, при авторизации на сайте через эту социальную сеть,
					вы будете авторизованы в текущем аккаунте.
					В противном случае, для каждой социальной сети будет создан новый аккаунт.
				</small>
			</dd>
		</dl>
		<ul class="profile-social-networks excluded">
			<?php foreach ($userServices as $name => $service): ?>
				<?php if (!isset($service['user'])): ?>
					<li class="social-icon-32 <?php echo $name ?>">
						<?php echo CHtml::link(
							'',
							CHtml::normalizeUrl(array('profile/addservice', 'service' => $name)),
							array('title' => $name)
						); ?>
					</li>
				<?php endif ?>
			<?php endforeach ?>
		</ul>
	</div>
</div>