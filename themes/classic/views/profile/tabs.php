<?php
/* @var $this ProfileController */
?>

<ul class="nav nav-tabs profile-tabs" id="profile-tabs">
	<li <?php echo $this->action->id == 'social' ? 'class="active"' : ''; ?>>
		<?php echo CHtml::link(Yii::t('main', 'Социальные сети'), $this->createUrl('/profile')); ?>
	</li>
	<li <?php echo $this->action->id == 'settings' ? 'class="active"' : ''; ?>>
		<?php echo CHtml::link(Yii::t('main', 'Личные данные'), $this->createUrl('/profile/settings')); ?>
	</li>
</ul>