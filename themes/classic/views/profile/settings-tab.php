<?php
/**
 * @var $this ProfileController
 * @var $form CActiveForm
 * @var $model User
 *
 */
$this->pageTitle = 'Личные данные';
$this->breadcrumbs = array(
	Yii::t('main', 'Профиль') => array('/profile/social'),
	Yii::t('main', 'Личные данные'),
);

Yii::app()->clientScript->registerScriptFile($this->assetsBase . '/js/maskedinput/jquery.maskedinput.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($this->assetsBase . '/js/profile.t.js', CClientScript::POS_END);

?>

<?php echo $this->renderPartial('//profile/tabs'); ?>

<div class="tab-content profile-tabs-content">
	<legend><?php echo Yii::t('app', 'Данные профиля'); ?></legend>
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'profile-settings-form',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'action' => Yii::app()->createUrl('profile/saveProfile'),
		'clientOptions' => array(
			'validateOnChange' => true,
			'validateOnSubmit' => true,
		),
		//'errorMessageCssClass' => 'alert alert-error',
		'htmlOptions' => array(
			'class' => 'form-horizontal',
		),
	)); ?>

	<?php //echo $form->errorSummary($model, null); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'username', array('class' => 'control-label')); ?>
		<div class="controls">
			<div class="input-prepend">
				<span class="add-on"><i class="icon-user"></i></span>
				<?php echo $form->textField($model, 'username'); ?>
			</div>
			<?php echo $form->error($model, 'username', array('class' => 'help-inline', 'inputContainer' => 'div.control-group')); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'email', array('class' => 'control-label')); ?>
		<div class="controls">
			<div class="input-prepend">
				<span class="add-on"><i class="icon-envelope"></i></span>
				<?php echo $form->textField($model, 'email'); ?>
			</div>
			<?php echo $form->error($model, 'email', array('class' => 'help-inline', 'inputContainer' => 'div.control-group')); ?>
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<span class="label label-info">Info</span>
			<small>Вы можете указать свой электронный адрес для получения уведомлений.</small>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'company_name', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo $form->textField($model, 'company_name', array(
				'class' => 'span4'
			)); ?>
			<?php echo $form->error($model, 'company_name', array('class' => 'help-inline', 'inputContainer' => 'div.control-group')); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'phone', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo $form->textField($model, 'phone', array(
				'maxlength' => 19,
				'class' => 'span4 masked-phone',
				'data-mask' => Yii::app()->params->itemAt('phoneMask'),
				'placeholder' => $model->getAttributeLabel('phone'),
			));?>
			<?php echo $form->error($model, 'phone', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'region_id', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo $form->dropDownList($model, 'region_id',
				Region::model()->getList(),
				array('empty' => 'Выберите регион')
			); ?>
			<?php echo $form->error($model, 'region_id', array(
				'class' => 'help-inline',
				'inputContainer' => 'div.control-group'
			)); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'city_id', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php $this->widget('application.widgets.TTypeAhead.TTypeAhead', array(
				'model' => $model,
				'attribute' => 'city_id',
				'name' => 'User[city_id]',
				'value' => !empty($model->city) ? $model->city->name : '',
				'options' => array(
					'minLength' => 3,
					'limit' => 10,
					'name' => 'city_id',
					'remote' => array(
						'url' => $this->createUrl('city/clientCitiesOld/%QUERY'),
						'replace' => 'js:function(url, uriEncodedQuery) { return T.profile.cityReplace(url, uriEncodedQuery); } ',
//						'beforeSend' => 'js: function(jqXhr, settings) { console.log(jqXhr, settings); } ',
//						'filter' => 'js: function(parsedResponse) { console.log(parsedResponse); return parsedResponse; } ',
					),
				)
			));?>
			<?php echo $form->error($model, 'city_id', array('class' => 'help-inline', 'inputContainer' => 'div.control-group')); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'address', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo $form->textArea($model, 'address', array(
				'class' => 'span4',
				'rows' => 2,
			)); ?>
			<?php echo $form->error($model, 'address', array('class' => 'help-inline', 'inputContainer' => 'div.control-group')); ?>
		</div>
	</div>
	<div class="control-group">
		<?php echo $form->labelEx($model, 'description', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo $form->textArea($model, 'description', array(
				'class' => 'span4',
				'rows' => 7,
			)); ?>
			<?php echo $form->error($model, 'description', array('class' => 'help-inline', 'inputContainer' => 'div.control-group')); ?>
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<?php echo CHtml::submitButton(Yii::t('app', 'Сохранить'), array('class' => 'btn')); ?>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div>