<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Контакты';
$this->breadcrumbs = array('Контакты');
?>

<div class="row">
	<div class="span8">
		<h1 class="title medium">Контакты</h1>

		<div class="form">
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id' => 'contact-form',
				'enableClientValidation' => true,
				'clientOptions' => array(
					'validateOnSubmit' => true,
				),
				'htmlOptions' => array(
					'class' => 'form-horizontal',
				),
			)); ?>

			<div class="control-group">
				<?php echo $form->labelEx($model, 'name', array('class' => 'control-label')); ?>
				<div class="controls">
					<?php echo $form->textField($model, 'name', array(
						'placeholder' => $model->getAttributeLabel('name'),
						'class' => 'span4'
					)); ?>
					<?php echo $form->error($model, 'name', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
				</div>
			</div>

			<div class="control-group">
				<?php echo $form->labelEx($model, 'email', array('class' => 'control-label')); ?>
				<div class="controls">
					<?php echo $form->textField($model, 'email', array(
						'placeholder' => $model->getAttributeLabel('email'),
						'class' => 'span4'
					)); ?>
					<?php echo $form->error($model, 'email', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
				</div>
			</div>

			<div class="control-group">
				<?php echo $form->labelEx($model, 'subject', array('class' => 'control-label')); ?>
				<div class="controls">
					<?php echo $form->textField($model, 'subject', array(
						'placeholder' => $model->getAttributeLabel('subject'),
						'class' => 'span4'
					)); ?>
					<?php echo $form->error($model, 'subject', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
				</div>
			</div>

			<div class="control-group">
				<?php echo $form->labelEx($model, 'body', array('class' => 'control-label')); ?>
				<div class="controls">
					<?php echo $form->textArea($model, 'body', array(
						'placeholder' => $model->getAttributeLabel('body'),
						'class' => 'span4',
						'rows' => 4,
					)); ?>
					<?php echo $form->error($model, 'body', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
				</div>
			</div>

			<?php if (CCaptcha::checkRequirements()): ?>
				<div class="control-group">
					<?php echo $form->labelEx($model, 'verifyCode', array('class' => 'control-label')); ?>
					<div class="controls">
						<?php $this->widget('CCaptcha', array(
							'showRefreshButton' => false,
							'clickableImage' => true,
							'imageOptions' => array(
								'alt' => 'Обновить код',
								'title' => 'Обновить код',
								'style' => 'cursor: pointer;',
							),
						)); ?>
						<?php echo $form->textField($model, 'verifyCode'); ?>
						<?php echo $form->error($model, 'verifyCode', array('class' => 'help-block error', 'inputContainer' => 'div.control-group')); ?>
					</div>
				</div>
			<?php endif; ?>

			<div class="form-actions">
				<?php echo CHtml::submitButton('Отправить', array('class' => 'btn btn-primary')); ?>
				<?php echo CHtml::resetButton('Очистить', array('class' => 'btn')); ?>
			</div>
			<?php $this->endWidget(); ?>
		</div>
	</div>
	<div class="span4">
		<div class="cwell">
			<!-- Address section -->
			<h4 class="title"><?php echo Yii::t('main', 'Контактная информация'); ?></h4>
			<div class="address">
				<p><?php echo Yii::app()->name; ?> - это, рейтинговый каталог лучших предложений по аренде спецтехники Украины</p>
				<address><p><i class="icon-home"></i> &nbsp; Украина, г.Киев, ул. Комсомольская 23</p></address>
<!--				<p><i class="icon-phone"></i> &nbsp; +380 (68) 376-55-62</p>-->
				<p><i class="icon-envelope-alt"></i> &nbsp; <?php echo Yii::app()->params->itemAt('contactEmail'); ?></p>
			</div>
		</div>
	</div>
</div>