<?php
/**
 * @var $this TController
 */

$this->pageTitle = 'Аренда спецтехники на сайте - ' . Yii::app()->name . '. Часто задаваемые вопросы.';
$this->pageKeywords = 'аренда спецтехники faq,  аренда спецтехники украина, аренда спецтехники киев, аренда спецтехники харьков';
$this->pageDescription = 'Аренда спецтехники на сайте - ' . Yii::app()->name . '. Часто задаваемые вопросы.';
$this->breadcrumbs = array(Yii::t('main', 'Часто задаваемые вопросы (FAQ)'));
?>
<div class="row-fluid">
	<div class="span12">
		<h1 class="title medium">Часто задаваемые вопросы (FAQ)</h1>
	</div>
</div>
<div class="row-fluid faq">
	<div class="span12">
		<div class="accordion" id="accordion2">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
					   href="#faq-1">
						<h4>1. Для чего нужна регистрация?</h4>
					</a>
				</div>
				<div id="faq-1" class="accordion-body collapse" style="height: 0px;">
					<div class="accordion-inner">
						<p>Зарегистрированные пользователи имеют ряд преимуществ:</p>
						<ol>
							<li>Возможность добавления объявлений о поиске спецтехники для аренды
								(раздел <?php echo CHtml::link('"Возьму в аренду"', '/rent'); ?>);
							</li>
							<li>Возможность указать контактные данные в настройках своего профиля. При добавлении
								объявлений, соответствующие поля объявления заполняются автоматически;
							</li>
							<li>Возможность добавить в профиль спецтехнику, которой вы владеете. В дальнейшем, при
								добавлении объявлений, вы можете выбрать необходимую единицу спецтехники и данные
								объявления будут заполнены автоматически;
							</li>
							<li>Возможность подписаться на получение различного рода уведомлений, как по email, так и по
								sms.
							</li>
						</ol>
						<p></p>
					</div>
				</div>
			</div>

			<h2 class="title medium">"Сдам в аренду" (Раздел)</h2>

			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#faq-2">
						<h4>Как добавить объявление?</h4>
					</a>
				</div>
				<div id="faq-2" class="accordion-body collapse" style="height: 0px;">
					<div class="accordion-inner">
						<p>Для добавления объявления необходимо перейти в
							раздел <?php echo CHtml::link('"Сдам в аренду"', '/toRent/add'); ?>.</p>

						<p>Если вы зарегистрированный пользователь, вы можете добавить спецтехнику в своем профиле и
							потом, на основании этих данных очень просто добавлять объявления.</p>
					</div>
				</div>
			</div>

			<h2 class="title medium">"Возьму в аренду" (Раздел)</h2>

			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
					   href="#faq-3">
						<h4>Как добавить объявление?</h4>
					</a>
				</div>
				<div id="faq-3" class="accordion-body collapse" style="height: 0px;">
					<div class="accordion-inner">
						<p>Добавление объявлений этого типа доступно только для зарегистрированных пользователей.</p>

						<p>Для добавления объявления необходимо перейти в
							раздел <?php echo CHtml::link('"Возьму в аренду"', '/rent/add'); ?>.</p>
					</div>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
					   href="#faq-4">
						<h4>Как работает раздел "Возьму в аренду"?</h4>
					</a>
				</div>
				<div id="faq-4" class="accordion-body collapse" style="height: 0px;">
					<div class="accordion-inner">
						<p>Схема достаточно простая. Вы добавляете объявление. Пользователи, которым интересно ваше
							предложение, могут оставить заявку к вашему предложению, в которой указывают свои условия
							сотрудничества.</p>

						<p>Затем, из списка этих заявок вы выбираете исполнителя и закрываете предложение (объявление).
							К закрытому проекту добавить заявку невозможно.</p>

						<p><span class="label label-info">Info!</span> Контактные данные исполнителей видны только
							владельцу предложения. Закрыть предложение может только его владелец.</p>

						<p>В дальнейшем мы планируем добавить возможность оценки как лица представляющего предложение,
							так и исполнителей. Это позволит более качественно оценивать человека, с которым предстоит
							работать, отсеивая заранее недобросовестных пользователей.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>