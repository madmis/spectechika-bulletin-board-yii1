<?php
/* @var $this TController */
$this->pageTitle = "Бесплатная доска объявлений аренды спецтехники в Украине. Аренда самосвала, экскаватора, погрузчика и другой техники.";
$this->setPageDescription(
	"Бесплатная доска объявлений аренды спецтехники в Украине. Взять в аренду самосвал, экскаватор, погрузчик или другую технику."
);
$this->setPageKeywords(
	'аренда спецтехники, аренда спецтехники киев, аренда спецтехники харьков, аренда спецтехники донецк,
	аренда спецтехники днепропетровск, аренда самосвала, аренда экскаватора, аренда погрузчика'
);
$this->breadcrumbs = array();
?>

<?php $this->renderPartial('//toRent/_main-last', array('items' => ToRent::model()->mainBlock())); ?>
<hr>
<?php $this->renderPartial('//toRent/_main-last-table', array('items' => ToRent::model()->mainTable())); ?>
<hr>
<br>
<?php $this->renderPartial('//rent/_main-last', array('items' => Rent::model()->mainBlock())); ?>
<hr>
<?php ///$this->renderPartial('//rent/_main-last-table', array('items' => Rent::model()->mainTable())); ?>
<!--<div class="bor"></div>-->
<br>
<?php //$this->renderPartial('//article/_main-last', array('items' => Article::model()->findLast())); ?>

<div class="row-fluid">
	<div class="span12">
		<h4 class="title">Категории спецтехники</h4>
	</div>
</div>
<?php $this->widget('application.widgets.MainCategory.MainCategory'); ?>


<?php if (Yii::app()->user->isGuest): ?>
	<div class="row">
		<div class="span12">
			<?php $this->renderPartial('//authenticate/login', array('model' => new LoginForm)); ?>
		</div>
	</div>
<?php endif; ?>
