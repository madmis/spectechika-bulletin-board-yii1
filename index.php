<?php

if (@file_exists(dirname(__FILE__) . '/protected/config/dev.php')) {
	define('YII_DEBUG', true);
	define('YII_TRACE_LEVEL', 3);
	$config = dirname(__FILE__) . '/protected/config/dev.php';
} else {
	define('YII_DEBUG', false);
	define('YII_TRACE_LEVEL', 0);
	$config = dirname(__FILE__) . '/protected/config/prod.php';
}

require_once(dirname(__FILE__) . '/../yii/framework/YiiBase.php');
require_once(dirname(__FILE__) . '/protected/components/TWebApplication.php');
class Yii extends YiiBase
{
	/**
	 * @static
	 * @return TWebApplication
	 */
	public static function app()
	{
		return parent::app();
	}

	/**
	 * Creates a Web application instance.
	 * @param mixed $config application configuration.
	 * If a string, it is treated as the path of the file that contains the configuration;
	 * If an array, it is the actual configuration information.
	 * Please make sure you specify the {@link CApplication::basePath basePath} property in the configuration,
	 * which should point to the directory containing all application logic, template and data.
	 * If not, the directory will be defaulted to 'protected'.
	 * @return TWebApplication
	 */
	public static function createWebApplication($config = null)
	{
		return self::createApplication('TWebApplication', $config);
	}
}

Yii::createWebApplication($config)->run();