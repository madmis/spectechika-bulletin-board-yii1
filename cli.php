<?php

if (@file_exists(dirname(__FILE__) . '/protected/config/dev.php')) {
	$config = dirname(__FILE__) . '/protected/config/console_dev.php';
} else {
	$config = dirname(__FILE__) . '/protected/config/console.php';
}
// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

// change the following paths if necessary
require_once(dirname(__FILE__) . '/../yii/framework/yii.php');
Yii::createConsoleApplication($config)->run();
